#include <dasp/Superpixels.hpp>
#include <dasp/Plots.hpp>
#include <Slimage/IO.hpp>
#include <Slimage/Slimage.hpp>
#include <Danvil/LinAlg.h>
#include <eigen3/Eigen/Dense>
#include <boost/math/constants/constants.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>

Eigen::Matrix4f LoadPlanePose(const std::string& fn) {
	std::ifstream ifs(fn);
	float r[9];
	for(unsigned int i=0; i<9; i++) {
		ifs >> r[i];
	}
	float t[3];
	for(unsigned int i=0; i<3; i++) {
		ifs >> t[i];
	}
	Eigen::Matrix4f pose;
	pose << r[0], r[1], r[2], t[0],
		r[3], r[4], r[5], t[1],
		r[6], r[7], r[8], t[2],
		0, 0, 0, 1;
	return pose;
}

float ComputeAngleDegree(const Eigen::Matrix4f& plane_pose) {
	Eigen::Vector4f u4 = plane_pose.inverse().col(3);
	Eigen::Vector3f u(u4[0], u4[1], u4[2]);
	float rad = std::acos(u.normalized().dot(Eigen::Vector3f::Unit(2)));
	float deg = rad / boost::math::constants::pi<float>() * 180.0f;
	return deg;
}

float ComputeDistance(const Eigen::Matrix4f& plane_pose) {
	return plane_pose.inverse().col(3).norm();
}

dasp::Superpixels ComputeSuperpixels(slimage::Image3ub color, slimage::Image1ui16 depth)
{
	dasp::Superpixels clustering;
	clustering.opt.camera = dasp::Camera{320.0f, 240.0f, 540.0f, 0.001f};
	clustering.opt.coverage = 1.7f;
	clustering.opt.weight_spatial = 1.0f;
	clustering.opt.weight_color = 2.0f;
	clustering.opt.weight_normal = 0.0f;
	clustering.opt.iterations = 20;
	clustering.opt.seed_mode = dasp::SeedModes::DepthMipmap;
	clustering.opt.base_radius = 0.012f;

	clustering.CreatePoints(color, depth);

	std::vector<dasp::Seed> seeds = clustering.FindSeeds();

	clustering.ComputeSuperpixels(seeds);

	return clustering;
}

dasp::plots::ClusterSelection SelectRed(const dasp::Superpixels& clustering)
{
	dasp::plots::ClusterSelection selection = dasp::plots::ClusterSelection::Empty(clustering.clusterCount());
	for(unsigned int i=0; i<clustering.clusterCount(); i++) {
		const dasp::Point& center = clustering.cluster[i].center;
		Danvil::ctLinAlg::Vec3f v(center.color[0], center.color[1], center.color[2]);
		// detect red
		float p = v.x / (v.x + v.y + v.z);
		selection[i] = (p > 0.55);
	}
	return selection;
}

slimage::Image3ub PaintSelection(const dasp::Superpixels& clustering, const dasp::plots::ClusterSelection& selection)
{
	slimage::Image3ub img_clusters = dasp::plots::PlotClusters(clustering, dasp::plots::ClusterPoints, dasp::plots::Color, selection);
	dasp::plots::PlotEdges(img_clusters, clustering.ComputeLabels(), slimage::Pixel3ub{{0,0,0}}, 1);
	dasp::plots::PlotClusters(img_clusters, clustering, dasp::plots::ClusterCenter, dasp::plots::UniWhite, selection);
	return img_clusters;
}

float ComputeDensityIntegral(const dasp::Superpixels& clustering, const dasp::plots::ClusterSelection& selection)
{
	slimage::Image1f density = dasp::ComputeDepthDensity(clustering.points, clustering.opt);
	float integral = 0.0f;
	clustering.ForPixelClusters([&selection,&integral,density](unsigned int cid, const dasp::Cluster& c, unsigned int pid, const dasp::Point& p) {
		if(selection[cid]) {
			integral += density(p.spatial_x(), p.spatial_y());
		}
	});
	return integral;
}

int main(int argc, char** argv)
{
	const std::string cBaseDir = "/home/david/Documents/DataSets/2012-03-02 Red Carpet/";
	const std::string cResultFile = "/tmp/red_carpet.txt";
	const unsigned int cSamples = 40;

	std::ofstream fs_result(cResultFile);
	fs_result << "frame" << "\t" << "angle" << "\t" << "distance" << "\t" << "density" << "\t" << "count" << std::endl;

	for(unsigned int i=0; i<cSamples; i++) {
		std::cout << "Frame " << i + 1 << " ..." << std::flush;
		// filenames
		std::string i_str = boost::lexical_cast<std::string>(i + 1);
		std::string fn_col = cBaseDir + "/img" + i_str + ".ppm";
		std::string fn_dep = cBaseDir + "/img" + i_str + "_depth.pgm";
		std::string fn_pse = cBaseDir + "/img" + i_str + "_pose";
		// load files
		slimage::Image3ub img_col = slimage::Load3ub(fn_col);
		slimage::Image1ui16 img_dep = slimage::Load1ui16(fn_dep);
		Eigen::Matrix4f img_pse = LoadPlanePose(fn_pse);
		// compute view angle and distance
		float angle = ComputeAngleDegree(img_pse);
		float distance = ComputeDistance(img_pse);
		// compute superpixels
		dasp::Superpixels super = ComputeSuperpixels(img_col, img_dep);
		dasp::plots::ClusterSelection selection = SelectRed(super);
		unsigned int count = selection.countEnabled();
		slimage::Image3ub img = PaintSelection(super, selection);
		slimage::Save(img, "/tmp/clusters_" + i_str + ".png");
		float density = ComputeDensityIntegral(super, selection);
		// print
		std::cout << " Angle = " << angle << ", Distance = " << distance << ", Density = " << density << ", Count = " << count << std::endl;
		fs_result << i << "\t" << angle << "\t" << distance << "\t" << density << "\t" << count << std::endl;
	}

	return 0;
}
