#include <dasp_aux/Recall.hpp>
#include <dasp_aux/ProgramOptions.hpp>
#include <Slimage/IO.hpp>
#include <Slimage/Slimage.hpp>
#include <boost/format.hpp>
#include <boost/program_options.hpp>
#include <boost/progress.hpp>
#include <iostream>

/**
 * This program evaluates the gPb contour detector on database images
 * required database layout: root -> labels -> %03d -> ucm.bmp
 */

int main(int argc, char** argv)
{
	dasp::ProgramOptions ps;
	ps.parse(argc, argv);

	boost::format fmt_retrieved(ps.dbBasePath() + "/labels/%03d/ucm.png");
	boost::format fmt_result(ps.dbResultPath() + "/gPb.csv");

	std::vector<unsigned int> img_ids = ps.getImageIds();

	std::vector<slimage::Image1ub> imgs_relevant(img_ids.size());
	std::vector<slimage::Image1ub> imgs_retrieved(img_ids.size());
	std::vector<std::ofstream> ofss_result(img_ids.size());

	for(unsigned int i=0; i<img_ids.size(); i++) {
		unsigned int id = img_ids[i];
		std::cout << "Loading database image " << id << std::endl;
		imgs_relevant[i] = ps.dbLoadBorders(id);
		imgs_retrieved[i] = slimage::Pick<unsigned char>(slimage::Load((fmt_retrieved % id).str()), 0);
	}

	std::ofstream ofs_result((fmt_result).str());

	boost::progress_display progress_bar(256);
	for(unsigned int q=0; q<256; q+=1, ++progress_bar) {
		std::vector<float> total(8, 0.0f);
		total[0] = q;
		for(unsigned int i=0; i<img_ids.size(); i++) {
			unsigned int id = img_ids[i];
			slimage::Image1ub bnds = slimage::Threshold(imgs_retrieved[i], static_cast<unsigned char>(q));
			dasp::PrecisionRecallStats stats = dasp::PrecisionRecall(imgs_relevant[i], bnds, 3);
			total[1] += stats.precision();
			total[2] += stats.recall();
			total[3] += stats.f1score();
			total[4] += static_cast<float>(stats.num_retrieved);
			total[5] += static_cast<float>(stats.num_retrieved_and_relevant);
			total[6] += static_cast<float>(stats.num_relevant);
			total[7] += static_cast<float>(stats.num_relevant_and_retrieved);
		}
		for(unsigned int k=1; k<=3; k++) {
			total[k] /= static_cast<float>(img_ids.size());
		}
		dasp::WriteCsvLine(ofs_result, total);
	}

	return 0;
}
