#include <dasp/Superpixels.hpp>
#include <dasp/Neighbourhood.hpp>
#include <dasp/Segmentation.hpp>
#include <dasp/Plots.hpp>
#include <dasp/Metric.hpp>
#include <dasp_aux/Recall.hpp>
#include <dasp_aux/ProgramOptions.hpp>
#include <Slimage/Paint.hpp>
#include <Slimage/Convert.hpp>
#include <boost/format.hpp>
#include <boost/progress.hpp>
#include <boost/timer.hpp>
#include <iostream>

//#define DASP_DEBUG_SHOWGUI

#ifdef DASP_DEBUG_SHOWGUI
	#include <Slimage/Gui.hpp>
#endif

//slimage::Image1ub PlotSegmentBoundaries(const dasp::Superpixels& clustering)
//{
//	// create segmentation graph
//	dasp::Segmentation segs = dasp::MinCutSegmentation(clustering);
//	std::cout << "Segment Count: " << segs.countSegments() << std::endl;
//	segs.createBoundariesFromLabels(clustering);
//	return segs.boundaries_wt;
//}

slimage::Image3ub CombineBoundaries(const slimage::Image1ub& red, const slimage::Image1ub& blue) {
	slimage::Image3ub vis(red.width(), red.height());
	for(unsigned int i=0; i<vis.size(); i++) {
		bool is_red = red[i] != 0;
		bool is_blue = blue[i] != 0;
		slimage::Pixel3ub color{{0,0,0}};
		if(is_red && is_blue) {
			color = {{0,0,255}};
		}
		else if(is_red) {
			color = {{255,0,0}};
		}
		else if(is_blue) {
			color = {{255,255,0}};
		}
		vis[i] = color;
	}
	return vis;
}

struct ProcessSettings
{
	float dasp_w_spatial;
	float dasp_w_color;
	float dasp_w_normal;
	unsigned int segs_num_ev;
	float segs_w_spatial;
	float segs_w_color;
	float segs_w_normal;
	bool dasp;
	bool smooth;
};

std::pair<float,float> process(const dasp::ProgramOptions& po, unsigned int count, bool write_results, bool p_create_images, const ProcessSettings& settings)
{
	const unsigned int cRep = 1;

	dasp::Parameters opt;
	opt.camera = dasp::Camera{320.0f, 240.0f, 540.0f, 0.001f};
	opt.coverage = 1.7f;
	opt.iterations = 3;
	opt.base_radius = 0.012f;
	opt.count = count;
	opt.color_space = dasp::ColorSpaces::RGB;
	opt.use_density_depth = settings.dasp;
	opt.gradient_adaptive_density = settings.dasp;
	opt.is_conquer_enclaves = true;
	opt.is_improve_seeds = true;
	opt.is_repair_depth = true;
	opt.is_smooth_depth = settings.smooth;

	bool is_slic;

	if(po.getAlgoName() == "dasp")
	{
		opt.weight_spatial = settings.dasp_w_spatial;
		opt.weight_image = 0.0f;
		opt.weight_color = settings.dasp_w_color;
		opt.weight_normal = settings.dasp_w_normal;
		opt.seed_mode = dasp::SeedModes::DepthMipmap;
		is_slic = false;
	}
	else if(po.getAlgoName() == "slic") {
		opt.weight_spatial = 0.0f;
		opt.weight_image = settings.dasp_w_spatial;
		opt.weight_color = settings.dasp_w_color;
		opt.weight_normal = 0.0f;
		opt.seed_mode = dasp::SeedModes::EquiDistant;
		is_slic = true;
	}
	else {
		std::cerr << "Unknown algorithm '" << po.getAlgoName() << "'" << std::endl;
		return { 0.0f, 0.0f };
	}

	boost::format fmt_result(po.dbResultPath() + "/" + po.getAlgoName() + "_spectral_%d.csv");

	struct ItemData
	{
		slimage::Image1ub relevant;

		dasp::Superpixels clusters;
		dasp::BorderPixelGraph g_borders;
		dasp::EdgeWeightGraph g_local;
		dasp::EdgeWeightGraph g_global;

		slimage::Image1f integrated_boundary;

		slimage::Image1f original_boundaries;
		slimage::Image1f boundaries;
	};

	std::vector<ItemData> itemdata(po.getImageIds().size());

	boost::progress_display progress(po.getImageIds().size() * cRep);

	double time = 0;

//	std::cout << "Calculating segmentations... " << std::flush;
//	boost::progress_display progress_pre(po.getImageIds().size() * cRep);
	for(unsigned int i=0; i<po.getImageIds().size(); i++) {
		unsigned int id = po.getImageIds()[i];

		slimage::Image3ub img_col = po.dbLoadColor(id);
		slimage::Image1ui16 img_dep = po.dbLoadDepth(id);
		itemdata[i].relevant = po.dbLoadBorders(id);

//		std::cout << "Image=" << id << ", Count=" << count << std::endl;

		std::vector<slimage::Image1f> results(cRep);
		slimage::Image1f result_total;
		for(unsigned int k=0; k<cRep; k++, ++progress) {
			boost::timer timer;
			dasp::Superpixels clustering = dasp::ComputeSuperpixels(img_col, img_dep, opt);
			dasp::BorderPixelGraph g_borders = dasp::CreateNeighborhoodGraph(clustering);
			dasp::EdgeWeightGraph g_local = is_slic
					? dasp::ComputeEdgeWeights(clustering, g_borders,
							dasp::MetricSLIC(opt.weight_spatial, opt.weight_color))
					: dasp::ComputeEdgeWeights(clustering, g_borders,
							dasp::MetricDASP(opt.weight_spatial, opt.weight_color, opt.weight_normal, opt.base_radius));
			dasp::EdgeWeightGraph g_affinity = is_slic
					? dasp::ComputeEdgeWeights(clustering, g_borders,
							dasp::ClassicSpectralAffinitySLIC(clustering.clusterCount(), settings.segs_w_color))
					: dasp::ComputeEdgeWeights(clustering, g_borders,
							dasp::ClassicSpectralAffinity<true>(clustering.clusterCount(), opt.base_radius, settings.segs_w_spatial, settings.segs_w_color, settings.segs_w_normal));
			dasp::EdgeWeightGraph g_global = dasp::SpectralSegmentation(g_affinity, boost::get(boost::edge_weight, g_local), settings.segs_num_ev);
			time += timer.elapsed();
			if(k == 0) {
				itemdata[i].clusters = clustering;
				itemdata[i].g_borders = g_borders;
				itemdata[i].g_local = g_local;
				itemdata[i].g_global = g_global;
				itemdata[i].original_boundaries = dasp::CreateBorderPixelImage(clustering.width(), clustering.height(),
						itemdata[i].g_borders,
						boost::get(boost::edge_weight, itemdata[i].g_local),
						boost::get(dasp::borderpixels_t(), itemdata[i].g_borders));
				itemdata[i].boundaries = dasp::CreateBorderPixelImage(clustering.width(), clustering.height(),
						itemdata[i].g_borders,
						boost::get(boost::edge_weight, itemdata[i].g_global),
						boost::get(dasp::borderpixels_t(), itemdata[i].g_borders));
			}
			// FIXME is this the desired result?
			results[k] = dasp::CreateBorderPixelImage(clustering.width(), clustering.height(),
					g_borders,
					boost::get(boost::edge_weight, g_global),
					boost::get(dasp::borderpixels_t(), g_borders));
			result_total = ((k==0) ? results[k] : (result_total + results[k]));
		}
		result_total *= 1.0f / static_cast<float>(cRep);
		itemdata[i].integrated_boundary = result_total;
//		slimage::Save(slimage::Convert_f_2_ub(result_total, 10.0f / static_cast<float>(cRep)), (fmt_result_vis % id % count).str());
	}

	time /= static_cast<double>(po.getImageIds().size() * cRep);
	std::cout << "Completed in " << time << " seconds" << std::endl;

//	std::cout << "Comparing with ground truth... " << std::flush;
//	boost::progress_display progress_truth(po.getImageIds().size() * 300);

	progress.restart(po.getImageIds().size() * 300);

	std::ofstream ofs_result;
	if(write_results) {
		ofs_result.open((fmt_result % count).str());
	}
	float best_f1 = 0.0f;
	float best_q = 0.0f;
	// convert to binary boundaries
	for(float q=0.2f; q<=40.0f; q+=0.2f) {
		std::vector<float> total(8, 0.0f);
		total[0] = q;
		for(unsigned int i=0; i<po.getImageIds().size(); i++, ++progress) {
			slimage::Image1ub bnds = slimage::Threshold(itemdata[i].integrated_boundary, q);
			dasp::PrecisionRecallStats stats = dasp::PrecisionRecall(itemdata[i].relevant, bnds, 3);
			total[1] += stats.precision();
			total[2] += stats.recall();
			total[3] += stats.f1score();
			total[4] += static_cast<float>(stats.num_retrieved);
			total[5] += static_cast<float>(stats.num_retrieved_and_relevant);
			total[6] += static_cast<float>(stats.num_relevant);
			total[7] += static_cast<float>(stats.num_relevant_and_retrieved);
		}
		for(unsigned int k=1; k<=3; k++) {
			total[k] /= static_cast<float>(po.getImageIds().size());
		}
		total[3] = 2.0f*total[1]*total[2]/(total[1] + total[2]);
		float f1 = total[3];
		if(f1 > best_f1) {
			best_f1 = f1;
			best_q = q;
		}
		if(write_results) {
			dasp::WriteCsvLine(ofs_result, total);
		}
	}

	if(write_results) {
		std::cout << "Wrote results to file." << std::endl;
	}

	if(p_create_images) {

		boost::format fmt_dasp(po.getImagePath() + "/%03d_" + po.getAlgoName() + "_%d_segment_color.png");
		boost::format fmt_connectivity(po.getImagePath() + "/%03d_" + po.getAlgoName() + "_%d_segment_graph.png");
		boost::format fmt_contours_local(po.getImagePath() + "/%03d_" + po.getAlgoName() + "_%d_segment_contours_local.png");
		boost::format fmt_contours_global(po.getImagePath() + "/%03d_" + po.getAlgoName() + "_%d_segment_contours_global.png");
		boost::format fmt_labels(po.getImagePath() + "/%03d_" + po.getAlgoName() + "_%d_segment_labels.png");

		for(unsigned int i=0; i<po.getImageIds().size(); i++) {
			unsigned int id = po.getImageIds()[i];

			{	// create color+superpixels
				slimage::Image3ub vis_img = dasp::plots::PlotPoints(itemdata[i].clusters, dasp::plots::Color);
				dasp::plots::PlotClusters(vis_img, itemdata[i].clusters, dasp::plots::ClusterPoints, dasp::plots::Color);
				dasp::plots::PlotEdges(vis_img, itemdata[i].clusters.ComputeLabels(), slimage::Pixel3ub{{255,255,255}}, 2);
				slimage::Save(vis_img, (fmt_dasp % id % count).str());
#ifdef DASP_DEBUG_SHOWGUI
				slimage::gui::Show("dasp", vis_img);
#endif
			}

			{ // create connectivity graph
				slimage::Image3ub vis_img(itemdata[i].clusters.width(), itemdata[i].clusters.height(), slimage::Pixel3ub{{0,0,0}});
				dasp::plots::PlotEdges(vis_img, itemdata[i].clusters.ComputeLabels(), slimage::Pixel3ub{{255,255,255}}, 2);
				// plot neighbourhood graph
				dasp::plots::PlotWeightedGraphLines(vis_img, itemdata[i].clusters, itemdata[i].g_global, [](float weight) { return dasp::plots::IntensityColor(weight, 0.0f, 1.0f); });
				slimage::Save(vis_img, (fmt_connectivity % id % count).str());
#ifdef DASP_DEBUG_SHOWGUI
				slimage::gui::Show("connectivity", vis_img);
#endif
			}

			{ // create contour image with original superpixel weights
				slimage::Image1ub vis_img = dasp::CreateSmoothedContourImage(itemdata[i].original_boundaries, 1.0f);
				slimage::Save(vis_img, (fmt_contours_local % id % count).str());
#ifdef DASP_DEBUG_SHOWGUI
				slimage::gui::Show("contours_original", vis_img);
#endif
			}

			{ // create contour image after spectral graph segmentation
				slimage::Image1ub vis_img = dasp::CreateSmoothedContourImage(itemdata[i].boundaries, 0.013f);
				slimage::Save(vis_img, (fmt_contours_global % id % count).str());
#ifdef DASP_DEBUG_SHOWGUI
				slimage::gui::Show("contours_spectral", vis_img);
#endif
			}

			{ // labels
				float threshold;
				if(po.has("threshold")) {
					threshold = po.get<float>("threshold");
				}
				else {
					threshold = best_q;
				}
				//segs.createLabelsFromBoundaries(clusters, 5.7f);
				dasp::ClusterLabeling labeling = dasp::ComputeSegmentLabels(itemdata[i].g_global, threshold);
				std::cout << labeling.num_labels  << std::endl;
				slimage::Image3ub vis_img = dasp::CreateLabelImage(itemdata[i].clusters, labeling,
						dasp::ComputeSegmentColors(itemdata[i].clusters, labeling));
				slimage::Save(vis_img, (fmt_labels % id % count).str());
#ifdef DASP_DEBUG_SHOWGUI
				slimage::gui::Show("labels", vis_img);
#endif
			}

#ifdef DASP_DEBUG_SHOWGUI
			slimage::gui::WaitForKeypress();
#endif

		}
	}


//#ifdef DASP_DEBUG_SHOWGUI
//	for(unsigned int i=0; i<img_ids.size(); i++) {
//		slimage::Image1ub bnds = slimage::Threshold(boundary_images[i][j], best_q);
//		slimage::Image3ub vis = dasp::CreateRecallImage(imgs_relevant[i], bnds, 3);
//		slimage::gui::Show((boost::format("img_%03d count=%d") % i % j).str(), vis);
//		slimage::gui::WaitForKeypress();
//	}
//#endif

	return { best_f1, best_q };
}

//#define DASP_OPTIMIZE

int main(int argc, char** argv)
{
	bool p_write_results = true;
	bool p_write_images = false;

	dasp::ProgramOptions po;
	po.desc.add_options()("write_results", boost::program_options::value<bool>(&p_write_results), "whether to write precision/recall results");
	po.desc.add_options()("write_images", boost::program_options::value<bool>(&p_write_images), "whether to create and write images");
	po.desc.add_options()("threshold", boost::program_options::value<float>(), "edge threshold for contours");
	po.parse(argc, argv);


#ifdef DASP_OPTIMIZE
	if(po.getCounts().size() > 1) {
		std::cerr << "Only one count supported in optimizing mode!" << std::endl;
		return 1;
	}
	// optimize parameters
	unsigned int count = po.getCounts().front();
	// which parameter -> dasp_w_color
	ProcessSettings settings;
	settings.dasp_w_spatial = 1.0f;
	settings.dasp_w_color = 2.0f;
	settings.dasp_w_normal = 3.0f;
	settings.segs_num_ev = 24;
	settings.segs_w_spatial = 1.0f;
	settings.segs_w_color = 1.0f;
	settings.segs_w_normal = 1.0f;
	settings.dasp = true;
	settings.smooth = false;
	for(int i=0; i<=1; i++) {
		//float t = 0.5f * static_cast<float>(i);
		bool t = (i == 1);
		settings.smooth = t;
		std::pair<float,float> result = process(po, count, false, false, settings);
		std::cout << "t=" << t << " => f1=" << result.first << " with q=" << result.second << std::endl;
	}
#else
	// just evaluate with some chosen parameters
	for(unsigned int cnt : po.getCounts()) {
		std::cout << "Evaluating segments with n=" << cnt << "..." << std::endl;
		ProcessSettings settings;
		settings.dasp_w_spatial = 1.0f;
		settings.dasp_w_color = 2.0f;
		settings.dasp_w_normal = 3.0f;
		settings.segs_num_ev = 24;
		settings.segs_w_spatial = 1.0f;
		settings.segs_w_color = 1.0f;
		settings.segs_w_normal = 1.0f;
		settings.dasp = true;
		settings.smooth = false;
		std::pair<float,float> result = process(po, cnt, p_write_results, p_write_images, settings);
		std::cout << "n=" << cnt << ": f1=" << result.first << " with q=" << result.second << std::endl;
	}
#endif

	return 0;
}
