/*
 * eval_superpixels.cpp
 *
 * Evaluates superpixels wrt the measures boundary recall and undersegmentation error.
 *
 * Reads files image files with borders located in the results folder of the database
 * main folder. Image names have a specific pattern dependent on 'image_id', the
 * database id of the image, and 'algo_name', the name of the algorithm. Both
 * arguments are given at the command line.
 * Compares to reference image files with a given pattern.
 * Writes results to a result file.
 *
 * === Mode "boundary_recall" ===
 * Input image pattern: "%03d_%s.png" % (image_id, algo_name)
 * Images are 3 channel 0/255 images
 *
 * === Mode "undersegmentation error" ===
 * Input image pattern: "%03d_%s_labels.pgm" % (image_id, algo_name)
 * Images are 1 channel graymap images.
 *
 *  Created on: Mar 13, 2012
 *      Author: david
 */

#include <dasp_aux/Recall.hpp>
#include <dasp_aux/ProgramOptions.hpp>
#include <Slimage/Slimage.hpp>
#include <Slimage/Convert.hpp>
#include <Slimage/IO.hpp>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include <iostream>

struct Item {
	std::string fn;
	unsigned int cnt;
};

void ComputeRecall(const slimage::Image1ub& img_expected, const std::vector<Item>& items, const std::string& fn_results)
{
	std::ofstream results(fn_results);
	std::vector<unsigned int> vr{ 1, 2, 3 };
	std::vector<float> vs{};
//	// write header
//	results << "count";
//	for(unsigned int r : vr) {
//		results << ",d" << r;
//	}
//	for(float s : vs) {
//		results << ",s" << s;
//	}
//	results << std::endl;
	// evaluate and write
	for(Item item : items) {
		std::cout << item.fn << std::endl;
		slimage::Image1ub img_actual = slimage::Pick<unsigned char>(slimage::Load(item.fn), 0);
		results << item.cnt;
		for(unsigned int r : vr) {
			float v = dasp::ComputeRecallBox(img_expected, img_actual, r);
			std::cout << "Recall box r=" << r << ": " << v << std::endl;
			results << "," << v;
		}
		for(float s : vs) {
			float v = dasp::ComputeRecallGaussian(img_expected, img_actual, s);
			std::cout << "Recall gauss s=" << s << ": " << v << std::endl;
			results << "," << v;
		}
		results << std::endl;
	}
}

void ComputeUndersegmentationError(const slimage::Image1i& img_relevant, const std::vector<Item>& items, const std::string& fn_results)
{
	// open file for results
	std::ofstream ofs(fn_results);
	// evaluate all images
	for(Item item : items) {
		std::cout << item.fn << std::endl;
		// load retrieved label image
		slimage::Image1i img_retrieved;
		slimage::conversion::Convert(slimage::Load1ui16(item.fn), img_retrieved);
		// compute undersegmentation error
		std::pair<float,unsigned int> ue = dasp::UndersegmentationErrorTotal(img_relevant, img_retrieved);
		std::cout << ue.first << " " << ue.second << std::endl;
		// write to file
		dasp::WriteCsvLine<float>(ofs, { static_cast<float>(item.cnt), ue.first, static_cast<float>(ue.second) });
	}
}

int main(int argc, char** argv)
{
	std::string p_mode = "";

	dasp::ProgramOptions po;
	po.desc.add_options()("mode", boost::program_options::value<std::string>(&p_mode), "evaluation mode: 'br' or 'use'");
	po.parse(argc, argv);

	boost::format fmt_relevant;
	boost::format fmt_pattern;
	boost::format fmt_result;
	unsigned int cnt_offset;

	if(p_mode == "br") {
		fmt_pattern = boost::format("\\Q%03d_" + po.getAlgoName() + "_\\E\\d+\\Q.png\\E"); // FIXME need to use paths from dasp::ProgramOptions
		cnt_offset = 4;
		fmt_result = boost::format(po.dbResultPath() + "/%03d_" + po.getAlgoName() + "_br.csv");
	}
	else if(p_mode == "use") {
		fmt_pattern = boost::format("\\Q%03d_" + po.getAlgoName() + "_\\E\\d+\\Q_labels.pgm\\E"); // FIXME need to use paths from dasp::ProgramOptions
		cnt_offset = 11;
		fmt_result = boost::format(po.dbResultPath() + "/%03d_" + po.getAlgoName() + "_use.csv");
	}
	else {
		std::cerr << "Unknown mode" << std::endl;
	}

	for(unsigned int id : po.getImageIds()) {
		std::cout << "Processing database image " << id << std::endl;

		std::vector<Item> items;

		// find all files which match pattern
		boost::regex pattern((fmt_pattern % id).str());
		namespace fs = boost::filesystem;
		fs::path someDir(po.getImagePath());
		if(fs::exists(someDir) && fs::is_directory(someDir)) {
			fs::directory_iterator end_iter;
			for(fs::directory_iterator dir_iter(someDir); dir_iter!=end_iter; ++dir_iter) {
				if(fs::is_regular_file(dir_iter->status())) {
					std::string fn = dir_iter->path().filename().string();
					if(boost::regex_match(fn, pattern)) {
						std::size_t pos = 5 + po.getAlgoName().length(); // FIXME HACK
						std::string str_cnt = fn.substr(pos, fn.length() - pos - cnt_offset); // FIXME HACK
						items.push_back({dir_iter->path().string(), boost::lexical_cast<unsigned int>(str_cnt)});
					}
				}
			}
		}
		std::sort(items.begin(), items.end(), [](const Item& a, const Item& b){ return a.cnt < b.cnt; });

		std::string fn_result = (fmt_result % id).str();

		if(p_mode == "br") {
			ComputeRecall(po.dbLoadBorders(id), items, fn_result);
		}
		else if(p_mode == "use") {
			ComputeUndersegmentationError(po.dbLoadLabels(id), items, fn_result);
		}
		else {
			std::cerr << "Unknown mode" << std::endl;
		}

	}

	return 1;
}
