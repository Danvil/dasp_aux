#!/bin/bash

DASP_DB_PATH="/home/david/Documents/DataSets/dasp_rgbd_dataset"
DASP_IMG_PATH="/media/tmp/dasp"
BIN_PATH="/home/david/Projects/dasp/dasp_release"

IMGS="1-11"

rm "${DASP_DB_PATH}/*"
rm "${DASP_IMG_PATH}/*"

echo ""
echo "=====  TURBO  ====="
echo ""
#./turbo_compute_labels.sh
#$BIN_PATH/eval_superpixels/eval_superpixels --db_path="${DASP_DB_PATH}" --image_path="${DASP_IMG_PATH}" --algo=turbo --image_ids=${IMGS} --mode=br
#$BIN_PATH/eval_superpixels/eval_superpixels --db_path="${DASP_DB_PATH}" --image_path="${DASP_IMG_PATH}" --algo=turbo --image_ids=${IMGS} --mode=use

echo ""
echo "=====  DASP  ====="
echo ""
#$BIN_PATH/dasp_cmd/dasp_cmd --db_path="${DASP_DB_PATH}" --image_path="${DASP_IMG_PATH}" --algo=dasp --image_ids=${IMGS}
$BIN_PATH/eval_superpixels/eval_superpixels --db_path="${DASP_DB_PATH}" --image_path="${DASP_IMG_PATH}" --algo=dasp --image_ids=${IMGS} --mode=br
$BIN_PATH/eval_superpixels/eval_superpixels --db_path="${DASP_DB_PATH}" --image_path="${DASP_IMG_PATH}" --algo=dasp --image_ids=${IMGS} --mode=use

echo ""
echo "=====  SLIC  ====="
echo ""
$BIN_PATH/dasp_cmd/dasp_cmd --db_path="${DASP_DB_PATH}" --image_path="${DASP_IMG_PATH}" --algo=slic --image_ids=${IMGS}
$BIN_PATH/eval_superpixels/eval_superpixels --db_path="${DASP_DB_PATH}" --image_path="${DASP_IMG_PATH}" --algo=slic --image_ids=${IMGS} --mode=br
$BIN_PATH/eval_superpixels/eval_superpixels --db_path="${DASP_DB_PATH}" --image_path="${DASP_IMG_PATH}" --algo=slic --image_ids=${IMGS} --mode=use

