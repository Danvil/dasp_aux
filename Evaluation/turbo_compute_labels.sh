#!/bin/bash
DASP_DB_PATH="/media/tmp/dasp"
CMD="/home/david/Projects/dasp/dasp_release/compute_labels/compute_labels"
# pattern 001_turbo_100.png -> 001_turbo_100_labels.pgm

for k in {1..11}
do
	for i in 100 200 300 400 500 750 1000 1500 2000
	do
		ID=`printf %03d ${k}`
		FN_IN="${ID}_turbo_${i}.png"
		FN_OUT=`echo ${FN_IN} | sed 's/.png/_labels.pgm/'`
		echo "${FN_IN} => ${FN_OUT}"
		${CMD} "${DASP_DB_PATH}/${FN_IN}" "${DASP_DB_PATH}/${FN_OUT}"
	done
done
