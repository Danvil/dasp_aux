#!/bin/bash
DASP_DB_PATH="/home/david/Documents/DataSets/dasp_rgbd_dataset/labels"
CMD="/home/david/Projects/dasp/dasp_release/compute_labels/compute_labels"
# pattern 001.png -> 001_labels.pgm

for k in {1..11}
do
	ID=`printf %03d ${k}`
	FN_IN="${ID}.png"
	FN_OUT=`echo ${FN_IN} | sed 's/.png/_labels.pgm/'`
	echo "${FN_IN} => ${FN_OUT}"
	${CMD} "${DASP_DB_PATH}/${FN_IN}" "${DASP_DB_PATH}/${FN_OUT}" 3
done
