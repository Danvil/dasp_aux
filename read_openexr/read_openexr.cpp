#include <OpenEXR/ImfInputFile.h>
#include <OpenEXR/ImfChannelList.h>
#include <OpenEXR/ImfFrameBuffer.h>
#include <OpenEXR/ImfArray.h>
#include <OpenEXR/ImfRgba.h>
#include <Slimage/Slimage.hpp>
#include <Slimage/IO.hpp>
#include <string>
#include <iostream>
#include <boost/program_options.hpp>

int main(int argc, char** argv)
{
	std::string fn_input = "/Amp/blender/0131.exr";
	std::string fn_result = "/tmp/blender/0131";

	namespace po = boost::program_options;

	po::options_description desc("Allowed options");
	desc.add_options()
	    ("help", "produce help message")
	    ("input", po::value<std::string>(&fn_input)->required(), "input filename (must be an *.exr file)")
	    ("output", po::value<std::string>(&fn_result)->required(), "output file string (files written will be $_color.png and $_depth.pgm)")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	std::string fn_result_color = fn_result + "_color.png";
	std::string fn_result_depth = fn_result + "_depth.pgm";

	std::cout << "Opening input file '" << fn_input << "' ..." << std::endl;

	Imf::InputFile file(fn_input.data());
	std::cout << "complete = " << file.isComplete() << std::endl;
	std::cout << "channels = ";
	for(auto it=file.header().channels().begin(); it!= file.header().channels().end(); ++it) {
		std::cout << it.name() << " ";
	}
	std::cout << std::endl;
	Imath::Box2i dw = file.header().dataWindow();
	unsigned int width = dw.max.x - dw.min.x + 1;
	unsigned int height = dw.max.y - dw.min.y + 1;
	std::cout << "size = " << width << "x" << height << std::endl;
	std::cout << "min index = " << dw.min.x << "x" << dw.min.y << std::endl;
	std::cout << "max index = " << dw.max.x << "x" << dw.max.y << std::endl;


	std::cout << "Reading data ..." << std::endl;

	Imf::Array2D<float> rPixels;
	Imf::Array2D<float> gPixels;
	Imf::Array2D<float> bPixels;
	Imf::Array2D<float> zPixels;
	rPixels.resizeErase(height, width);
	gPixels.resizeErase(height, width);
	bPixels.resizeErase(height, width);
	zPixels.resizeErase(height, width);
	Imf::FrameBuffer frameBuffer;
	frameBuffer.insert("R",
			Imf::Slice(Imf::FLOAT,
					(char*)(&rPixels[0][0] - dw.min.x - dw.min.y * width),
					sizeof(rPixels[0][0]) * 1,
					sizeof(rPixels[0][0]) * width,
					1, 1, 0));
	frameBuffer.insert("G",
			Imf::Slice(Imf::FLOAT,
					(char*)(&gPixels[0][0] - dw.min.x - dw.min.y * width),
					sizeof(gPixels[0][0]) * 1,
					sizeof(gPixels[0][0]) * width,
					1, 1, 0));
	frameBuffer.insert("B",
			Imf::Slice(Imf::FLOAT,
					(char*)(&bPixels[0][0] - dw.min.x - dw.min.y * width),
					sizeof(bPixels[0][0]) * 1,
					sizeof(bPixels[0][0]) * width,
					1, 1, 0));
	frameBuffer.insert("Z",
			Imf::Slice(Imf::FLOAT,
					(char*)(&zPixels[0][0] - dw.min.x - dw.min.y * width),
					sizeof(zPixels[0][0]) * 1,
					sizeof(zPixels[0][0]) * width,
					1, 1, FLT_MAX));
	file.setFrameBuffer(frameBuffer);
	file.readPixels(dw.min.y, dw.max.y);


	std::cout << "Converting color data ..." << std::endl;

//	float r_min=FLT_MAX, g_min=FLT_MAX, b_min=FLT_MAX;
//	float r_max=FLT_MIN, g_max=FLT_MIN, b_max=FLT_MIN;
//	for(unsigned int y=0; y<height; y++) {
//		for(unsigned int x=0; x<width; x++) {
//			// read
//			float cr = rPixels[y][x];
//			float cg = gPixels[y][x];
//			float cb = bPixels[y][x];
//			r_min = std::min(r_min, cr);
//			r_max = std::max(r_max, cr);
//			g_min = std::min(g_min, cg);
//			g_max = std::max(g_max, cg);
//			b_min = std::min(b_min, cb);
//			b_max = std::max(b_max, cb);
//		}
//	}
//	std::cout << "r range = " << r_min << "," << r_max << std::endl;
//	std::cout << "g range = " << g_min << "," << g_max << std::endl;
//	std::cout << "b range = " << b_min << "," << b_max << std::endl;

	slimage::Image3ub result_color(width, height); // FIXME get correct size
	for(unsigned int y=0; y<height; y++) {
		for(unsigned int x=0; x<width; x++) {
			// read
//			float cr = 255.0f * (rPixels[y][x] - r_min) / (r_max - r_min);
//			float cg = 255.0f * (gPixels[y][x] - g_min) / (g_max - g_min);
//			float cb = 255.0f * (bPixels[y][x] - b_min) / (b_max - b_min);
			int cr = std::min(255, std::max(0, static_cast<int>(255.0f * rPixels[y][x])));
			int cg = std::min(255, std::max(0, static_cast<int>(255.0f * gPixels[y][x])));
			int cb = std::min(255, std::max(0, static_cast<int>(255.0f * bPixels[y][x])));
			// convert write
			result_color(x, y) = slimage::Pixel3ub{
				static_cast<unsigned char>(cr),
				static_cast<unsigned char>(cg),
				static_cast<unsigned char>(cb)};
		}
	}

	std::cout << "Writing color image to '" << fn_result_color << "' ..." << std::flush;
	slimage::Save(result_color, fn_result_color);
	std::cout << " Finished!" << std::endl;


	std::cout << "Converting depth data ..." << std::endl;

	slimage::Image1ui16 result_depth(width, height); // FIXME get correct size
	for(unsigned int y=0; y<height; y++) {
		for(unsigned int x=0; x<width; x++) {
			// read
			float val_mm = 1000.0 * zPixels[y][x];
			if(val_mm > 20000) val_mm = 20000;
			if(val_mm < -10) val_mm = -10;
			int val_mm_i = std::min(65535, std::max(0, static_cast<int>(val_mm)));
			if(val_mm_i < 400) {
				val_mm_i = 0;
			}
			if(val_mm_i > 16000) {
				val_mm_i = 0;
			}
			// convert write
			uint16_t res = static_cast<uint16_t>(val_mm_i);
			result_depth(x, y) = res;
		}
	}

	std::cout << "Writing depth image to '" << fn_result_depth << "' ..." << std::flush;
	slimage::Save(result_depth, fn_result_depth);
	std::cout << " Finished!" << std::endl;


	return 1;
}
