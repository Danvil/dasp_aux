#include <boost/lexical_cast.hpp>
#include <rgbd/rgbd.hpp>
#include <common/color.hpp>
#include <dasp/Superpixels.hpp>
#include <dasp/Plots.hpp>
#include <dasp/Segmentation.hpp>
#include <dasp/Neighbourhood.hpp>
#include <dasp/Metric.hpp>
#include <dasp/impl/Sampling.hpp>
#include <Slimage/IO.hpp>
#include <Slimage/Slimage.hpp>
#include <Slimage/Gui.hpp>
#include <boost/format.hpp>
#include <boost/array.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <fstream>

void SaveClusterCenters(const std::string& fn, const dasp::Superpixels& dasp)
{
	std::ofstream of(fn);
	for(const auto& c : dasp.cluster) {
		of << c.center.px << "\t" << c.center.py << std::endl;
	}
}

void SaveSeeds(const std::string& fn, const std::vector<dasp::Seed> seeds)
{
	std::ofstream of(fn);
	for(const auto& s : seeds) {
		of << s.x << "\t" << s.y << std::endl;
	}
}

int main(int argc, char** argv)
{
	std::string fn_oni = "/home/david/Documents/DataSets/2013-01_DASV_1/person-3.oni";
	unsigned int frame_a = 32;
	unsigned int frame_b = 40;
	std::string path = "/home/david/git/papers/2013_ICIP_DASV/images";

	std::shared_ptr<RandomAccessRgbdStream> rgbd_stream = FactorOni(fn_oni);
	std::cout << rgbd_stream->numFrames() << std::endl;

	rgbd_stream->seek(frame_a);
	Rgbd rgbd_a = rgbd_stream->grabAndGet();
	std::cout << rgbd_a.color.width() << "x" << rgbd_a.color.height() << std::endl;

	rgbd_stream->seek(frame_b);
	Rgbd rgbd_b = rgbd_stream->grabAndGet();
	std::cout << rgbd_b.color.width() << "x" << rgbd_b.color.height() << std::endl;

	constexpr unsigned int cnt = 1000;

	dasp::Parameters opt;
	opt.camera = dasp::Camera{320.0f, 240.0f, 540.0f, 0.001f};
	opt.coverage = 1.7f;
	opt.weight_spatial = 1.0f;
	opt.weight_color = 6.0f;
	opt.weight_normal = 3.0f;
	opt.iterations = 20;
	opt.seed_mode = dasp::SeedModes::Delta;
	opt.color_space = dasp::ColorSpaces::RGB;
	opt.base_radius = 0.012f;
	opt.count = cnt;
	opt.is_conquer_enclaves = true;
	opt.is_improve_seeds = true;
	opt.is_repair_depth = false;
	opt.is_smooth_depth = false;

	slimage::gui::Show("img_color", rgbd_a.color);
	slimage::Save(rgbd_a.color, path + "/img_color.png");

	slimage::Image3ub img_depth = common::GreyDepth(rgbd_a.depth, 1250, 3250);
	slimage::gui::Show("img_depth", img_depth);
	slimage::Save(img_depth, path + "/img_depth.png");

	dasp::Superpixels clustering = dasp::ComputeSuperpixels(rgbd_a.color, rgbd_a.depth, opt);
	std::cout << "n_given=" << cnt << std::flush;
	std::cout << " R=" << clustering.opt.base_radius << " n_seeds=" << clustering.seeds.size() << " n_final=" << clustering.cluster.size() << std::endl;

	SaveSeeds(path + "/cluster_sampled.tsv", clustering.seeds);
	SaveClusterCenters(path + "/cluster_final.tsv", clustering);

	slimage::Image3ub img_dasp = dasp::plots::PlotClusters(clustering, dasp::plots::ClusterPoints, dasp::plots::Color);
	dasp::plots::PlotEdges(img_dasp, clustering.ComputeLabels(), slimage::Pixel3ub{{0,0,0}}, 2);
	slimage::gui::Show("img_dasp", img_dasp);
	slimage::Save(img_dasp, path + "/img_dasp.png");

	auto Gnb = dasp::CreateNeighborhoodGraph(clustering, dasp::NeighborGraphSettings::NoCut());
	auto Gnb_weighted = dasp::ComputeEdgeWeights(clustering, Gnb,
			dasp::MetricDASP(
				clustering.opt.weight_spatial, clustering.opt.weight_color, clustering.opt.weight_normal,
				clustering.opt.base_radius));
	slimage::Image3ub img_graph = dasp::plots::PlotClusters(clustering, dasp::plots::ClusterPoints, dasp::plots::Color);
	dasp::plots::PlotWeightedGraphLines(img_graph, clustering, Gnb_weighted,
			[](float weight) {
				float s = std::exp(-0.1f*weight);
				return dasp::plots::IntensityColor(s, 0, 1);
			});
	slimage::gui::Show("img_graph", img_graph);
	slimage::Save(img_graph, path + "/img_graph.png");

	for(unsigned int i=0; i<clustering.seeds.size(); ++i) {
		clustering.cluster[i].seed_id = i;
	}

	slimage::Image3ub img_cluster_prev = dasp::plots::PlotClusters(clustering, dasp::plots::ClusterCenter, dasp::plots::UniWhite);
	slimage::gui::Show("img_cluster_prev", img_cluster_prev);
	slimage::Save(img_cluster_prev, path + "/img_cluster_prev.png");
	SaveClusterCenters(path + "/cluster_prev.tsv", clustering);

	Eigen::MatrixXf density = dasp::ComputeDepthDensity(clustering.points, clustering.opt);
	slimage::Image3ub img_density = common::MatrixToImage(density,
		[](float x) {
			return common::IntensityColor(x, 0.0f, 0.01f);
		}
	);
	slimage::gui::Show("img_density", img_density);
	slimage::Save(img_density, path + "/img_density.png");

	ComputeSuperpixelsIncremental(clustering, rgbd_b.color, rgbd_b.depth);
	std::cout << "n_given=" << cnt << std::flush;
	std::cout << " R=" << clustering.opt.base_radius << " n_seeds=" << clustering.seeds.size() << " n_final=" << clustering.cluster.size() << std::endl;

	// write clustering seed change
	std::vector<boost::array<int,4>> cluster_change;
	for(const dasp::Seed& s : clustering.seeds_previous) {
		cluster_change.push_back(boost::array<int,4>{s.x,s.y,0,0});
	}
	for(const auto& t : clustering.getClusterCentersAsSeeds()) {
		if(t.label == -1) {
			cluster_change.push_back(boost::array<int,4>{0,0,t.x,t.y});
		}
		else {
			cluster_change[t.label][2] = t.x;
			cluster_change[t.label][3] = t.y;
		}
	}
	std::ofstream of(path + "/cluster_change.tsv");
	for(const auto& s : cluster_change) {
		of << s[0] << "\t" << s[1] << "\t" << s[2] << "\t" << s[3] << std::endl;
	}

	Eigen::MatrixXf density_curr = dasp::ComputeDepthDensity(clustering.points, clustering.opt);
	slimage::Image3ub img_density_curr = common::MatrixToImage(density_curr,
		[](float x) {
			return common::IntensityColor(x, 0.0f, 0.01f);
		}
	);
	slimage::gui::Show("img_density_curr", img_density_curr);
	slimage::Save(img_density_curr, path + "/img_density_curr.png");

	Eigen::MatrixXf seed_density = dasp::ComputeDepthDensityFromSeeds(clustering.seeds_previous, density);
	slimage::Image3ub img_cluster_density_prev = common::MatrixToImage(seed_density,
		[](float x) {
			return common::IntensityColor(x, 0.0f, 0.01f);
		}
	);
	slimage::gui::Show("img_cluster_density_prev", img_cluster_density_prev);
	slimage::Save(img_cluster_density_prev, path + "/img_cluster_density_prev.png");

	slimage::Image3ub img_density_delta = common::MatrixToImage(seed_density - density_curr,
		[](float x) {
			return common::PlusMinusColor(x, 0.005f);
		}
	);
	slimage::gui::Show("img_density_delta", img_density_delta);
	slimage::Save(img_density_delta, path + "/img_density_delta.png");

	slimage::Image3ub img_cluster_curr = dasp::plots::PlotClusters(clustering, dasp::plots::ClusterCenter, dasp::plots::UniWhite);
	slimage::gui::Show("img_cluster_curr", img_cluster_curr);
	slimage::Save(img_cluster_curr, path + "/img_cluster_curr.png");
	SaveClusterCenters(path + "/cluster_curr.tsv", clustering);

	slimage::gui::WaitForKeypress();

	return 1;
}
