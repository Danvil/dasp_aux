#include <Slimage/IO.hpp>
#include <Slimage/Gui.hpp>
#include <Eigen/Dense>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/progress.hpp>
#include <map>
#include <string>
#include <iostream>
#include <fstream>

typedef int label_t;
typedef slimage::Image<slimage::Traits<label_t,1>> label_image_t;

label_t color_to_id(const slimage::Pixel3ub& color)
{
	unsigned int r = color[0];
	unsigned int g = color[1];
	unsigned int b = color[2];
	if(r == 0 && g == 0 && b == 0) return -1;
	if(r == 255 && g == 255 && b == 255) return -1;
	if(r == 255 && g == 0 && b == 0) return -1;
	return static_cast<label_t>(r + (g << 8) + (b << 16));
}

label_image_t color_to_id(const slimage::Image3ub& img)
{
	label_image_t result(img.width(), img.height());
	for(unsigned int i=0; i<img.size(); i++) {
		result[i] = color_to_id(img[i]);
	}
	return result;
}

unsigned int count_labels(const label_image_t& img)
{
	std::map<label_t, unsigned int> label_count;
	for(unsigned int i=0; i<img.size(); i++) {
		label_count[img[i]] ++;
	}
	return label_count.size();
}

float compute_compactness(unsigned int num_total, unsigned int num_border)
{
	return 4.0f * 3.1415f * static_cast<float>(num_total) / static_cast<float>(num_border*num_border);
}

struct LabelInfo
{
	label_t id;
	unsigned int num_total;
	unsigned int num_border;
	float compactness;
};

std::vector<LabelInfo> compute_label_info(const label_image_t& id_img)
{
	std::map<label_t, LabelInfo> m_labels;
	for(unsigned int y=1; y<id_img.height()-1; y++) {
		for(unsigned int x=1; x<id_img.width()-1; x++) {
			label_t id = id_img(x,y);
			if(id == -1) continue;
			auto it = m_labels.find(id);
			if(it == m_labels.end()) {
				LabelInfo li;
				li.id = id;
				li.num_total = 0;
				li.num_border = 0;
				li.compactness = 0.0f;
				m_labels[id] = li;
			}
			auto& li = m_labels[id];
			li.num_total ++;
			label_t id00 = id_img(x-1,y-1);
			label_t id01 = id_img(x-1,y+1);
			label_t id10 = id_img(x+1,y-1);
			label_t id11 = id_img(x+1,y+1);
			if(id00 != id || id01 != id || id10 != id || id11 != id) {
				li.num_border ++;
			}
		}
	}
	std::vector<LabelInfo> label_info;
	label_info.reserve(m_labels.size());
	for(auto& p : m_labels) {
		p.second.compactness = compute_compactness(p.second.num_total, p.second.num_border);
		label_info.push_back(p.second);
	}
	return label_info;
}

float compute_image_compactness(const std::vector<LabelInfo> v)
{
	float val = 0.0f;
	for(const LabelInfo& info : v) {
		val += info.compactness * static_cast<float>(info.num_total);
	}
	return val / (640.0f * 480.0f);
}

template<typename V, typename T, typename F>
float compute_compression_error_impl(const label_image_t& labels, const slimage::Image<T>& img_values, F f)
{
	const std::size_t num = labels.size();
	std::vector<V> values(num);
	std::map<label_t, std::pair<V,unsigned int>> label_mean;
	for(std::size_t i=0; i<num; i++) {
		label_t label = labels[i];
		if(label == -1) continue;
		auto it = label_mean.find(label);
		if(it == label_mean.end()) {
			label_mean[label] = {V::Zero(), 0};
		}
		auto col = f(img_values[i]);
		values[i] = col;
		label_mean[label].first += col;
		label_mean[label].second ++;
	}
	V image_mean = V::Zero();
	for(auto& p : label_mean) {
		p.second.first /= static_cast<float>(p.second.second);
		image_mean += p.second.first;
	}
	image_mean /= static_cast<float>(label_mean.size());
	float error1 = 0.0f;
	for(auto& p : label_mean) {
	 	float d1 = (p.second.first - image_mean).squaredNorm();
	 	error1 += static_cast<float>(p.second.second) * d1;
	}
	float error2 = 0.0f;
	for(std::size_t i=0; i<num; i++) {
		if(labels[i] == -1) continue;
		float d2 = (values[i] - image_mean).squaredNorm();
		error2 += d2;
	}
	return error1 / error2;
}

Eigen::Vector3f color_to_eigen(const slimage::PixelAccess3ub& color)
{
	unsigned int r = color[0];
	unsigned int g = color[1];
	unsigned int b = color[2];
	return {
		static_cast<float>(r) / static_cast<float>(255.0f),
		static_cast<float>(g) / static_cast<float>(255.0f),
		static_cast<float>(b) / static_cast<float>(255.0f)
	};
}

float compute_compression_error_color(const label_image_t& labels, const slimage::Image3ub& colors)
{
	return compute_compression_error_impl<Eigen::Vector3f>(labels, colors, &color_to_eigen);
}

Eigen::Vector2f depth_to_eigen(const slimage::PixelAccess1ui16& q)
{
	return Eigen::Vector2f{ static_cast<float>(q) / 1000.0f, 0.0f };
}

float compute_compression_error_depth(const label_image_t& labels, const slimage::Image1ui16& depth)
{
	return compute_compression_error_impl<Eigen::Vector2f>(labels, depth, &depth_to_eigen);
}

int main(int argc, char** argv)
{
	std::string p_dir_label = "";
	std::string p_dir_color = "";
	std::string p_dir_depth = "";
	std::string p_result = "/tmp/log.tsv";
	unsigned int p_num = 100;
	bool p_streamgbh_mode = false;

	namespace po = boost::program_options;
	po::options_description desc;
	desc.add_options()
		("help", "produce help message")
		("dl", po::value(&p_dir_label)->default_value(p_dir_label), "label input directory")
		("dc", po::value(&p_dir_color)->default_value(p_dir_color), "color input directory")
		("dd", po::value(&p_dir_depth)->default_value(p_dir_depth), "depth input directory")
		("out", po::value(&p_result)->default_value(p_result), "result output file")
		("num", po::value(&p_num)->default_value(p_num), "number of frames")
		("sgbh", po::value(&p_streamgbh_mode)->default_value(p_streamgbh_mode), "p_streamgbh_mode")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);
	
	if(vm.count("help")) {
		std::cerr << desc << std::endl;
		return 1;
	}

	std::ofstream log(p_result);

	boost::format fmt_label_sgbh(p_dir_label + "/%02d/%05d.ppm");
	boost::format fmt_label[2] = {
		boost::format(p_dir_label + "/%05d_dasv_labels.png"),
		boost::format(p_dir_label + "/%05d_segs_labels.png")
	};
	boost::format fmt_color(p_dir_color + "/%05d.ppm");
	boost::format fmt_depth(p_dir_depth + "/%05d.pgm");

	boost::progress_display prg(p_num);

	for(unsigned int k=1; k<=p_num; k++, ++prg) {
		std::string fn_color = (fmt_color % k).str();
		std::string fn_depth = (fmt_depth % k).str();
		slimage::Image3ub img_color = slimage::Load3ub(fn_color);
		slimage::Image1ui16 img_depth = slimage::Load1ui16(fn_depth);
		if(p_streamgbh_mode) {
			for(unsigned int i=0; i<=20; i++) {
				std::string fn_label = (fmt_label_sgbh % i % k).str();
				slimage::Image3ub img_label = slimage::Load3ub(fn_label);
				label_image_t img_ids = color_to_id(img_label);
				std::vector<LabelInfo> label_info = compute_label_info(img_ids);
				unsigned int num = label_info.size();
				float c = compute_image_compactness(label_info);
				float cec = compute_compression_error_color(img_ids, img_color);
				float ced = compute_compression_error_depth(img_ids, img_depth);
				// std::cout << k << "  \th=" << i << "  \tnum=" << num << "  \tc=" << c << "\te=" << e << std::endl;
				log << k << "\t" << i << "\t" << num << "\t" << c << "\t" << cec << "\t" << ced << "\n";
			}
		}
		else {
			for(unsigned int i=0; i<2; i++) {
				std::string fn_label = (fmt_label[i] % (k-1)).str();
				slimage::Image3ub img_label = slimage::Load3ub(fn_label);
				label_image_t img_ids = color_to_id(img_label);
				std::vector<LabelInfo> label_info = compute_label_info(img_ids);
				unsigned int num = label_info.size();
				float c = compute_image_compactness(label_info);
				float cec = compute_compression_error_color(img_ids, img_color);
				float ced = compute_compression_error_depth(img_ids, img_depth);
				// std::cout << k << "  \th=" << i << "  \tnum=" << num << "  \tc=" << c << "\te=" << e << std::endl;
				log << k << "\t" << i << "\t" << num << "\t" << c << "\t" << cec << "\t" << ced << "\n";
			}
		}
		log << std::flush;
	}

	std::cout << "Finished." << std::endl;

	return 1;
}
