/*
 * simulate.cpp
 *
 *  Created on: May 14, 2012
 *      Author: david
 */

#include <Slimage/Slimage.hpp>
#include <Slimage/IO.hpp>
#include <Slimage/Gui.hpp>
#include <Eigen/Dense>
#include <boost/bind.hpp>
#include <boost/format.hpp>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>


struct Rgbd
{
	slimage::Image3ub color;
	slimage::Image1f depthf;
	slimage::Image1ui16 depth;

	void set(unsigned int x, unsigned int y, const Eigen::Vector3f& px_color, float px_depth) {
		color(x,y) = slimage::Pixel3ub{{
			static_cast<unsigned char>(px_color[0]*255.0f),
			static_cast<unsigned char>(px_color[1]*255.0f),
			static_cast<unsigned char>(px_color[2]*255.0f)}};
		//float d = (px_depth < 0 ? 0 : (px_depth > 12 ? 12 : px_depth));
		float d = px_depth;
		depthf(x,y) = d;
		depth(x,y) = static_cast<int>(d*1000.0f);
	}

	void save(const std::string& fn) {
		slimage::Save(color, fn + "_color.png");
		slimage::Save(depth, fn + "_depth.pgm");
	}

	static Rgbd Create(unsigned int width, unsigned int height) {
		return Rgbd {
			slimage::Image3ub(width, height),
			slimage::Image1f(width, height),
			slimage::Image1ui16(width, height)
		};
	}

};

struct Camera
{
	Eigen::Affine3f T;
	unsigned int width, height;
	float cx, cy;
	float focal_length;

	static Camera DefaultKinect() {
		return Camera{
			Eigen::Affine3f::Identity(),
			640, 480,
			320.0f, 240.0f,
			540.0f
		};
	}
};

struct Ray
{
	Eigen::Vector3f start;
	Eigen::Vector3f direction;

	Eigen::Vector3f point(float t) const {
		return start + t * direction;
	}
};

struct CollisionResult
{
	struct Surfel {
		float t;
		Eigen::Vector3f position;
		Eigen::Vector3f normal;
	};

	CollisionResult() {}

	CollisionResult(const std::vector<Surfel>& v_surfels) {
		surfels = v_surfels;
		std::sort(surfels.begin(), surfels.end(), [](const Surfel& a, const Surfel& b) {
			return (a.t < b.t);
		});
	}

	std::vector<Surfel> surfels;
};

struct Sphere
{
	Eigen::Vector3f center;
	float radius;
};

namespace collision
{
	CollisionResult CollideSphere(const Sphere& shape, const Ray& ray);
}

struct Scene
{
	Rgbd render(const Camera& camera);

	struct Fragment
	{
		float t;
		Eigen::Vector3f position;
		Eigen::Vector3f normal;
		Eigen::Vector3f color;
		bool valid;

		static Fragment Invalid() {
			return Fragment{
				-1e9,
				Eigen::Vector3f::Zero(),
				Eigen::Vector3f::Zero(),
				Eigen::Vector3f::Zero(),
				false
			};
		}
	};

	struct Object
	{
		Eigen::Vector3f color;
		boost::function<CollisionResult(const Ray& ray)> collider;

		Fragment operator()(const Ray& ray) const {
			CollisionResult result = collider(ray);
			for(unsigned int i=0; i<result.surfels.size(); i++) {
				const CollisionResult::Surfel& q = result.surfels[i];
				if(q.t > 0) {
					return Fragment { q.t, q.position, q.normal, color, true };
				}
			}
			return Fragment::Invalid();
		}
	};

	void addSphere(const Eigen::Vector3f& center, float radius, const Eigen::Vector3f& color) {
		objects_.push_back(Scene::Object{color, boost::bind(&collision::CollideSphere, Sphere{center,radius}, _1)});
	}

	Fragment cast(const Ray& ray, unsigned int bumps);

	Eigen::Vector3f background;

	std::vector<Object> objects_;

	Eigen::Vector3f light_dir;

	float light_ambient;
};


CollisionResult collision::CollideSphere(const Sphere& sphere, const Ray& ray)
{
	const Eigen::Vector3f& n = ray.direction;
	Eigen::Vector3f d = ray.start - sphere.center;
	float r = sphere.radius;
	// quadratic equation
	float a = n.dot(n);
	float b = 2*n.dot(d);
	float c = d.dot(d) - r*r;
	// solve
	float D = b*b - 4*a*c;
	if(D < 0) {
		return CollisionResult();
	}
	else if(D == 0) {
		float t = -b / (2 * a);
		Eigen::Vector3f p = ray.point(t);
		return CollisionResult({
			{t, p, (p - sphere.center).normalized()}
		});
	}
	else {
		float q = std::sqrt(D);
		float t1 = (-b + q) / (2 * a);
		float t2 = (-b - q) / (2 * a);
		Eigen::Vector3f p1 = ray.point(t1);
		Eigen::Vector3f p2 = ray.point(t2);
		return CollisionResult({
			{t1, p1, (p1 - sphere.center).normalized()},
			{t2, p2, (p2 - sphere.center).normalized()}
		});
	}
}

Rgbd Scene::render(const Camera& camera)
{
	Rgbd frame = Rgbd::Create(camera.width, camera.height);
	Eigen::Vector3f n0 = camera.T.rotation() * Eigen::Vector3f(0,0,1);
	Ray ray { camera.T.translation(), n0 };
	for(unsigned int y=0; y<camera.height; y++) {
		float dy = (static_cast<float>(y) - camera.cy) / camera.focal_length;
		for(unsigned int x=0; x<camera.width; x++) {
			float dx = (static_cast<float>(x) - camera.cx) / camera.focal_length;
			ray.direction = camera.T.rotation() * (Eigen::Vector3f(dx, dy, 1.0f).normalized());
			Fragment f = cast(ray, 0);
			if(f.valid) {
				float light = - f.normal.dot(light_dir);
				if(light < 0) light = 0;
				light += light_ambient;
				if(light > 1) light = 1;
				float z = n0.dot(f.position - ray.start);
				frame.set(x, y, light * f.color, z);
			}
			else {
				frame.set(x, y, background, 0);
			}
		}
	}
	return frame;
}

Scene::Fragment Scene::cast(const Ray& ray, unsigned int bumps)
{
	// intersect
	Fragment f = Fragment::Invalid();
	for(unsigned int i=0; i<objects_.size(); i++) {
		const Object& obj = objects_[i];
		Fragment q = obj(ray);
		if(!f.valid || (q.valid && q.t < f.t)) {
			f = q;
		}
	}
	return f;
}

typedef std::vector<Eigen::Affine3f> CameraTrajectory;

CameraTrajectory Create(const std::vector<Eigen::Vector3f>& vlookat, const std::vector<Eigen::Vector3f>& vpos, const Eigen::Vector3f& up)
{
	unsigned int n1 = vlookat.size();
	unsigned int n2 = vpos.size();
	unsigned int n = std::max(n1, n2);
	if(n1 != 1 && n1 != n) {
		throw 0;
	}
	if(n2 != 1 && n2 != n) {
		throw 0;
	}
	typedef Eigen::Vector3f Vec3;
	std::vector<Eigen::Affine3f> v;
	for(unsigned int i=0; i<n; i++) {
		Vec3 lookat = vlookat[n1 == 1 ? 0 : i];
		Vec3 p = vpos[n2 == 1 ? 0 : i];
		Vec3 az = (lookat - p).normalized();
		Vec3 ax = az.cross(up).normalized();
		Vec3 ay = az.cross(ax);
		Eigen::Matrix3f R;
		R << ax, ay, az;
		Eigen::Affine3f T;
		T.linear() = R;
		T.translation() = p;
		v.push_back(T);
	}
	return v;
}

int main(int argc, char** argv)
{
	bool show_gui = true;

	typedef Eigen::Vector3f Vec3;
	const float BigR = 500.0f;

	Scene scene;
	scene.background = Vec3(0.2f, 0.2f, 0.2f);
	scene.light_ambient = 0.3f;
	scene.light_dir = Vec3(0,1,0);
	// objects
	scene.addSphere(Vec3(-1,-0.46f,1), 0.46f, Vec3(0.5,0,0.5));
	scene.addSphere(Vec3(+1,-0.46f,1), 0.46f, Vec3(0.5,0,0.5));
	scene.addSphere(Vec3(0.0,-0.25f,0.3), 0.25f, Vec3(1,0,0));
	scene.addSphere(Vec3(-0.5,-0.15f,-0.2), 0.15f, Vec3(0,1,0));
	scene.addSphere(Vec3(0.5,-0.15f,0), 0.10f, Vec3(0,0,1));
	// room
	scene.addSphere(Vec3(0,0,5-BigR), BigR, Vec3(0.85f,0.85f,0.85f)); // back
	scene.addSphere(Vec3(0,BigR,0), BigR, Vec3(0.85f,0.85f,0.85f)); // floor
	scene.addSphere(Vec3(0,-2.7-BigR,0), BigR, Vec3(0.85f,0.85f,0.85f)); // ceiling
	scene.addSphere(Vec3(-2-BigR,0,0), BigR, Vec3(0.85f,0.85f,0.85f)); // left
	scene.addSphere(Vec3(+2+BigR,0,0), BigR, Vec3(0.85f,0.85f,0.85f)); // right

	Camera camera = Camera::DefaultKinect();
//	camera.T.translate(Vec3(0,1,-2));
//	camera.T.rotate(Eigen::AngleAxisf(0.2f, Vec3::UnitX()));

//	CameraTrajectory traj = Create(
//			{Vec3(-1,-0.5, 0), Vec3(-0.5,-0.5, 0), Vec3(0,-0.5, 0), Vec3(+0.5,-0.5, 0), Vec3(+1,-0.5, 0)},
//			{Vec3(-1,-1.5,-2), Vec3(-0.5,-1.5,-2), Vec3(0,-1.5,-2), Vec3(+0.5,-1.5,-2), Vec3(+1,-1.5,-2)},
//			Vec3(0,-1,0)
//	);
//	CameraTrajectory traj = Create(
//			{Vec3(-1,-0.5,0), Vec3(-0.5,-0.5,0), Vec3(0,-0.5,0), Vec3(+0.5,-0.5,0), Vec3(+1,-0.5,0)},
//			{Vec3(0,-0.5,-2)},
//			Vec3(0,-1,0)
//	);
	CameraTrajectory traj = Create(
			{Vec3(0,-0.5,0)},
			{Vec3(-1,-1.5,-2), Vec3(-0.5,-1.5,-2), Vec3(0,-1.5,-2), Vec3(+0.5,-1.5,-2), Vec3(+1,-1.5,-2)},
			Vec3(0,-1,0)
	);

	boost::format fmt("/tmp/sim_%03d");

	for(unsigned int i=0; i<traj.size(); i++) {
		std::cout << "Rendering frame " << i << "..." << std::endl;
		std::string fn = (fmt % i).str();
		// camera
		camera.T = traj[i];
		std::ofstream ofs(fn + ".txt");
		ofs << camera.T.matrix() << std::endl;
		// render image
		Rgbd frame = scene.render(camera);
		frame.save(fn);
		if(show_gui) {
			slimage::gui::Show("dasp simulation (color)", frame.color, 0);
			slimage::gui::Show("dasp simulation (depth)", frame.depthf, 1.0/5.0f, 0);
			slimage::gui::WaitForKeypress();
		}
	}

	return 0;
}
