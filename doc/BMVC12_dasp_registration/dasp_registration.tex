\documentclass{bmvc2k}

\usepackage{amsmath,amssymb}

%% Enter your paper number here for the review copyu
\bmvcreviewcopy{423}

\title{Point Cloud Segmentation using Depth-Adaptive Superpixels}

% Enter the paper's authors in order
% \addauthor{Name}{email/homepage}{INSTITUTION_CODE}
\addauthor{David Weikersdorfer}{http://ias.cs.tum.edu/people/weikersdorfer}{1}
\addauthor{Antonis Argyros}{http://www.ics.forth.gr/~argyros}{1}
\addauthor{Michael Beetz}{http://ias.cs.tum.edu/people/beetz}{2}

% Enter the institutions
% \addinstitution{Name\\Address}
\addinstitution{
 Intelligent Autonomous Systems Group\\
 Technische Universit''at M''unchen\\
 M''unchen, Germany
}
\addinstitution{
 Institute of Computer Science\\
 FORTH\\
 Crete, Greek
}

\runninghead{Weikersdorfer, Argyros, Beetz}{DASP Point Cloud Segmentation}

% Any macro definitions you would like to include
% These are not defined in the style file, because they don't begin
% with \bmva, so they might conflict with the user's own macros.
% The \bmvaOneDot macro adds a full stop unless there is one in the
% text already.
\def\eg{\emph{e.g}\bmvaOneDot}
\def\Eg{\emph{E.g}\bmvaOneDot}
\def\etal{\emph{et al}\bmvaOneDot}

%------------------------------------------------------------------------- 
% Document starts here
\begin{document}

\maketitle

\begin{abstract}
Depth-adaptive superpixels is a superpixel segmentation algorithm for images with color and depth information which exploits depth information for a superior image segmentation.
In this paper we demonstrate that depth-adaptive superpixels are especially advantegeous for segmenting a cloud of three-dimensional points representing the surface of scene geometry.
We propose a novel algorithm which iteratively builds and segments a point cloud constructed from single-view RGBD images.
In the evaluation we show that our approach outperform state-of-the-art point cloud segmentation algorithms in quality and runtime. 
\end{abstract}

%------------------------------------------------------------------------- 
\section{Introduction}
\label{sec:intro}

In this paper we develop a point cloud segmentation algorithm which does not require any special assumptions like a supporting plane.
Our algorithm only has the requirement, that the point cloud is constructed from a set of single-view point clouds which are matched against each other, using e.g. an iterative closest point algorithm.
This is for example the case in a SLAM scenario.

In order to segment the point cloud we compute depth-adaptive superpixels on individual single-view RGBD images and combine them to a ''superpoint'' oversegmentation of the combined point cloud.
Superpoints are in the three-dimensional analogon of superpixels computed by oversegmentation algorithms like Turbopixels and SLIC.
On the neighbourhood graph of the superpoint segmentation we can employ local and global segmentation algorithms.

{\bf Related work:}

\begin{itemize}
\item An Evaluation of the RGB-D SLAM System \cite{Endres}
\item Real-time 3D visual SLAM with a hand-held RGB-D camera \cite{Engelhard}
\item DTAM: Dense tracking and mapping in real-time \cite{Newcombe2011}
\item KinectFusion : Real-Time Dense Surface Mapping and Tracking \cite{Newcombea}
\item Real-Time Visual Odometry from Dense RGB-D Images \cite{Steinbrucker}
\end{itemize}

{\bf Technical references:}

\begin{itemize}
\item Contour detection and hierarchical image segmentation \cite{Arbelaez2011}
\item Robust Euclidean alignment of 3D point sets: the trimmed iterative closest point algorithm \cite{Chetverikov2005}
\item Blue-noise point sampling using kernel density model \cite{Fattal2011}
\item A Database of Human Segmented Natural Images and its Application to Evaluating Segmentation Algorithms and Measuring Ecological Statistics \cite{Martin2001}
\item Efficient Variants of the ICP Algorithm \cite{Rusinkiewicz}
\end{itemize}

%------------------------------------------------------------------------- 
\section{Depth-Adaptive Superpixels}
\label{sec:dasp}
Oversegmentation is an elegant method to reduce image complexity and provide an sparse representation of the dense pixel grid.
Recently depth-adaptive superpixels have been introduced which provide oversegmentation for RGB-D image data.
Depth information is used to compute 3D positions and normals for every pixel.
Using knowledge over depth-gradient and distance from camera, cluster seeds following a poisson disk distribution can be sampled to uniformly cover the scene geometry.
This is in contrast to 2D superpixel algorithms which either do not require uniform covering or can only cover the pixel grid uniformly.
The clustering itself uses a k-means algorithm which operates on pixel 3D position, pixel color and pixel normal using a custom metric.
During the clustering step the pixel grid can be used to efficiently find neighbours of cluster seeds thus resulting in a runtime linear in the number of pixels.


%------------------------------------------------------------------------- 
\section{DASP Registration}
\label{sec:registration}

As superpixels are a sparse representation of all image pixels, the resulting 3D cluster centers, called superpoints in the following, are a sparse representation of the image point cloud.
This fact can be exploited to simplify matching two point clouds resulted from single-view measurements.
Given a set of points $P_{all}$ which shall be matched to a reference set of points $M_{all}$, it is sufficient to consider the corresponding sets of superpoints.
Thus we need to compute a transformation $(R,t)$, $R \in SO(3), t \in \mathbb{R}^3$ which brings a set of superpoints $P = \{ p'_i \,|\, 1 \le i \le n \}$ in alignment with a reference set of superpoints $M = \{ (p_j, n_j) \,|\, 1 \le j \le m \}$.

As superpoints are in general only localizable with respect to orthogonal distance to the geometry surface, we have to choose a distance metric which only considers the "point-to-plane" distance.
The least-square problem we seek to solve is:
\begin{equation}
	E(R,t) := \sum_{i=1}^{n}{ \Big( \big( R \, p'_i + t - p_{\mu(i)} \big) \circ n_{\mu(i)} \Big)^2 } \stackrel{!}{=} \text{min}
\end{equation}

The pairing function $\mu$ assignes each superpoint in $P$ to the nearest superpoint in $M$.

In general this problem can be solved using a non-linear method, for simplification we choose to linearize the problem by assuming that incremental rotations are small.
\begin{equation}
	R \approx \left( \begin{array}{ccc}
	1 & -\gamma & \beta \\
	\gamma & 1 & -\alpha \\
	-\beta & \alpha & 1 \end{array} \right)
\end{equation}
This approximation yields a linear least-squares problem in the variables $\alpha, \beta, \gamma, t_x, t_y, t_z$ which is solved by solving a system of linear equations.
\begin{equation}
	C(P',P) \: (\alpha, \beta, \gamma, t_x, t_y, t_z)^{t} = b(P',P) \,,\:\: C(P',P) \in \mathbb{R}^{6 \times 6},\, b(P',P)\in \mathbb{R}^6
\end{equation}

%------------------------------------------------------------------------- 
\section{Point Cloud Segmentation with DASP}
\label{sec:segmentation}

For single-view images, the superpixel neighbourhood graph, where all superpixels which share a common border are connected, can be used as a starting point for efficient image segmentation.
Edges in the graph represent the superpixel similarity by considering Euclidean, color and normal distance.
The superpixel similarity can be evaluated locally in the spirit of an edge detector, but it can also be used for global segmentation using spectral graph theory.

As this technique does not rely on the fact that the neighbourhood is planar, it can be generalized to the more general neighbourhood graph of superpoints.
In this paper we will use oversegmentations into depth-adaptive superpixels for individual single-view measurements to derive a segmentation for the combined point cloud.
Towards this goal we proceed as follows.

First we demonstrate how the superpoint neighbourhood graphs obtained from a set of individual single-view measurements can be efficiently merged into a global superpoint neighbourhood graph.
Second the exact same clustering method from DASP is used for the general 3D case.
For efficient neighbourhood search we use an octree, as the pixel grid is no longer available.
Third we show how a superpoint neighbourhood graph can be constructed efficiently from the clustered point cloud (\dots How is this actually done in 3D???)
Last we apply the framework of spectral graph theory to get a global segmentation and combine it with a local segmentation results where only individual edges are considered.

\subsection{Merging superpixel clusters and neighbourhood graphs}

\subsection{Point clustering and superpoint neighbourhood graph}

\subsection{Superpoint segmentation}

%------------------------------------------------------------------------- 
\section{Evaluation}
\label{sec:evaluation}

The following statements should be proved or demonstrated.

{\bf Evaluation of registration:}
\begin{itemize}
\item Registration with superpoints has a similar performance than registration with all points
\item Compare e.g. against \cite{Steinbrucker}
\item Varying the number of superpixels has only a minor impact on registration performance
\end{itemize}

{\bf Evaluation of segmentation:}
\begin{itemize}
\item Segmentation with superpoints provide "good" segmentations in the general case (what metric???)
\item Compare DASP point cloud segmentation against the state-of-the-art (table cutting, Euclidean clustering, ???)
\item Demonstrate it in a SLAM scenario using e.g. the dataset from \cite{Endres}
\end{itemize}

%------------------------------------------------------------------------- 
\section{Conclusions}
\label{sec:conclusions}

This paper is awesome!

\bibliography{dasp_registration}
\end{document}
