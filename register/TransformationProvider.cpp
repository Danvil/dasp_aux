/*
 * TransformationProvider.cpp
 *
 *  Created on: May 15, 2012
 *      Author: david
 */

#include "TransformationProvider.hpp"
#include <Candy/OpenGL.h>

TransformationProvider::TransformationProvider(const Batch::Ptr& batch)
{
	batch_ = batch;
	current_frame_ = 0;
}

void TransformationProvider::next()
{
	if(!icp_ || icp_->isReady()) {
		// pick next frame if any
		if(current_frame_ + 1 >= batch_->numFrames()) {
			return;
		}
		current_frame_ ++;
		f_src_ = batch_->at(current_frame_);
		f_dst_ = batch_->at(current_frame_ - 1);
		if(!f_dst_->T_is_ground_truth) {
			std::cerr << "Destination transformation must be known!" << std::endl;
		}

		PointSet pnts_source = f_src_->dasp.createPointSet();
		PointSet pnts_target = f_dst_->dasp.createPointSet();
		std::cout << "TransformationProvider: source frame=" << current_frame_ << ", number of points=" << pnts_source.size() << std::endl;
		std::cout << "TransformationProvider: target frame=" << current_frame_-1 << ", number of points=" << pnts_target.size() << std::endl;

		Transformation Tinital = Transformation::Identity();
		if(f_src_->T_is_ground_truth) {
			// T_G_src * p_src = T_G_dst * p_dst
			// => inv(T_G_dst) * T_G_src * p_src = p_dst
			Tinital = f_dst_->T_global.inverse() * f_src_->T_global;
		}

		impl::NearestPairing c_algo_;
		IterativeClosestPoints::PairingFunctionType pairing_fnc = boost::bind(&impl::NearestPairing::operator(), c_algo_, _1, pnts_target);

		icp_.reset(new IterativeClosestPoints());
		icp_->start(pnts_source, pnts_target, Tinital, pairing_fnc);

		f_src_->T_global = f_dst_->T_global * Tinital;
		f_src_->pairings = icp_->getPairings();
	}
}

void TransformationProvider::update()
{
	if(icp_ && !icp_->isReady()) {
		// icp step
		icp_->step();
		f_src_->T_global = f_dst_->T_global * icp_->getTransformation();
		f_src_->pairings = icp_->getPairings();
	}
}

bool TransformationProvider::isFinished() const
{
	return (!icp_ || icp_->isReady()) && (current_frame_ + 1 >= batch_->numFrames());
}

void RenderCorrespondence(const Pairings& pairings, const PointSet& pnts_source, const PointSet& pnts_target)
{
	glBegin(GL_LINES);
	for(const Pairings::Pair& p : pairings.pairings_) {
		Eigen::Vector3f a = pnts_source[p.source_id].position;
		Eigen::Vector3f n = pnts_source[p.source_id].normal;
		Eigen::Vector3f b = pnts_target[p.target_id].position;
		Eigen::Vector3f c = b - (b - a).dot(n) * n;
		glColor3ub(255, 255, 255);
		glVertex3f(a[0], a[1], a[2]);
		glVertex3f(b[0], b[1], b[2]);
		glColor3ub(0, 0, 0);
		glVertex3f(a[0], a[1], a[2]);
		glVertex3f(c[0], c[1], c[2]);
		glColor3ub(255, 0, 0);
		glVertex3f(c[0], c[1], c[2]);
		glVertex3f(b[0], b[1], b[2]);
	}
	glEnd();
}

void TransformationProvider::render() const
{
	if(!f_src_ || !f_dst_) {
		return;
	}
	PointSet psrc = f_src_->transformPointsGlobal();
	PointSet pdst = f_dst_->transformPointsGlobal();
	// render points
	RenderPoints(psrc, 3.0f);
	RenderPoints(pdst, 3.0f);
	// render point correspondences
	RenderCorrespondence(f_src_->pairings, psrc, pdst);
}
