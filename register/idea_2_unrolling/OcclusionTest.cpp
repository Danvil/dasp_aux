/*
 * OcclusionTest.cpp
 *
 *  Created on: May 21, 2012
 *      Author: david
 */

#include "OcclusionTest.hpp"
#include <Slimage/Gui.hpp>
#include <GL/glew.h>

namespace unroll
{

	std::vector<float> PointRenderOcclusion(const graph_t& graph_, const Transformation& T, const dasp::Camera& camera, unsigned int width, unsigned int height)
	{
		// All points in the model are considered renderable particles.
		// Particles have a position, a normal and a label indication the superpixel.
		// Particles are sorted by distance from camera and rendered nearest to far.
		// The render target is a buffer which stores if filled and particle label.
		// If a particle is rendered into an unfilled location, the location is marked filled and the particle label is stored.
		// If a particle is rendered into a filled location, the particle is discarded.
		// The final occlusion value for each superpixel is #accepted/#total.

		constexpr int cParticleRenderRadius = 0;

		struct OcclusionQuery {
			unsigned int accepted;
			unsigned int total;
		};
		struct Particle {
			Eigen::Vector3f position;
			Eigen::Vector3f normal;
			int label;
		};
		struct BufferPoint {
			bool filled;
			int label;
		};

		// initialize particles
		std::vector<Particle> particles;
		particles.reserve(boost::num_vertices(graph_) * 100);
		for(auto vid : as_range(boost::vertices(graph_))) {
			const Cluster& cluster = graph_[vid];
			for(const Point& p : cluster.points) {
				particles.push_back(Particle{T * p.position, T.rotation() * p.normal, static_cast<int>(vid)});
			}
		}
		std::cout << "#particles=" << particles.size() << std::endl;

		// sort particles by depth (nearest to camera first)
		std::sort(particles.begin(), particles.end(), [](const Particle& x, const Particle& y) {
			return x.position[2] < y.position[2];
		});

		// initialize cluster occlusion queries
		std::vector<OcclusionQuery> cluster_queries(boost::num_vertices(graph_), OcclusionQuery{0,0});
		// prepare renderbuffer
		std::vector<BufferPoint> buffer(width*height, BufferPoint{false,-1});

		// "render" particles (render RxR patches)
		for(const Particle& p : particles) {
			//if(p.normal[2] < 0) {
				Eigen::Vector2f q = camera.project(p.position);
				PixelLocation px_center = Discretize(q);
				bool accepted = true;
				for(int u=-cParticleRenderRadius; u<=+cParticleRenderRadius; u++) {
					for(int v=-cParticleRenderRadius; v<=+cParticleRenderRadius; v++) {
						PixelLocation px = px_center;
						px.x += u;
						px.y += v;
						if(px.inRange(width, height)) {
							BufferPoint& bp = buffer[px.index(width, height)];
							if(!bp.filled || bp.label == p.label) {
								bp.filled = true;
								bp.label = p.label;
							}
							else {
								accepted = false;
							}
						}
					}
				}
				if(accepted) {
					cluster_queries[p.label].accepted ++;
				}
			//}
			// else
			cluster_queries[p.label].total ++;
		}

		// compute occlusion percentage
		std::vector<float> occlusion(cluster_queries.size());
		for(unsigned int i=0; i<occlusion.size(); i++) {
			const OcclusionQuery& q = cluster_queries[i];
			occlusion[i] = 1.0f - static_cast<float>(q.accepted) / static_cast<float>(q.total);
		}

		return occlusion;
	}

	std::vector<float> GraphNodeQuadsOcclusion(graph_t& graph, const Transformation& T, const dasp::Camera& camera, unsigned int width, unsigned int height, float super_radius)
	{
		constexpr float cDepthTolerance = 0.03f;
		constexpr GLenum cDepthInternalFormat = GL_DEPTH_COMPONENT24;
		constexpr double cZNear = 0.5;
		constexpr double cZFar = 30.0;
		constexpr double cDepthMax = 16777215.0;

		std::cout << "GraphNodeQuadsOcclusion running..." << std::endl;

		// create OpenGL render buffers

		// create frame buffer object
		unsigned int fb;
		glGenFramebuffers(1, &fb);
		glBindFramebuffer(GL_FRAMEBUFFER, fb);
		// create color buffer and attach to FBO
		unsigned int color_rb;
		glGenRenderbuffers(1, &color_rb);
		glBindRenderbuffer(GL_RENDERBUFFER, color_rb);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, color_rb);
		// create depth buffer and attach to FBO
		unsigned int depth_rb;
		glGenRenderbuffers(1, &depth_rb);
		glBindRenderbuffer(GL_RENDERBUFFER, depth_rb);
		glRenderbufferStorage(GL_RENDERBUFFER, cDepthInternalFormat, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth_rb);

		// check if everything is allright
		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if(status != GL_FRAMEBUFFER_COMPLETE) {
			std::cerr << "glCheckFramebufferStatus says " << status << std::endl;
		}
		else {
			std::cout << "glCheckFramebufferStatus says GL_FRAMEBUFFER_COMPLETE" << std::endl;
		}
		unsigned int glerr = glGetError();
		if(glerr != GL_NO_ERROR) {
			std::cerr << "OpenGL says " << glerr << std::endl;
		}
		else {
			std::cout << "OpenGL says GL_NO_ERROR" << std::endl;
		}

		// render
		glViewport(0, 0, width, height);
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		gluPerspective(camera.computeOpenGLFOV(height), float(width)/float(height), static_cast<float>(cZNear), static_cast<float>(cZFar));
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		// opengl uses a different coordinate system so we need to rotate around the x-axis by 180 deg
		Eigen::Matrix4f T_ogl = (Eigen::AngleAxisf(boost::math::constants::pi<float>(), Eigen::Vector3f(1,0,0)) * T).matrix();
		glLoadMatrixf(T_ogl.data());
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		for(auto vid : as_range(boost::vertices(graph))) {
			const Superpoint& sp = graph[vid];
			// enlarge hull by absolute the world size of 1px
			Eigen::Vector3f p_cam = T * sp.center.position;
			Eigen::Vector3f n_cam = T.rotation() * sp.center.normal;
			float tau_prj = p_cam[2] / (camera.focal * std::abs(n_cam[2])) * n_cam.norm();
			if(tau_prj > super_radius) {
				tau_prj = super_radius;
			}
			float q = (super_radius + tau_prj * 0.7f) / super_radius;
			sp.renderHullScaled(q);
		}
		glPopMatrix();
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);

		// read result buffer
		slimage::Image4ub buf_color(width, height);
		glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buf_color.begin());
		slimage::Image1f buf_depth(width, height);
		glReadPixels(0, 0, width, height, GL_DEPTH_COMPONENT, GL_FLOAT, buf_depth.begin());
		slimage::gui::Show("render color buffer", buf_color, 0);
		slimage::gui::Show("render depth buffer", buf_depth, 1.0f, 200);

		// clean up OpenGL
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glDeleteRenderbuffers(1, &depth_rb);
		glDeleteRenderbuffers(1, &color_rb);
		glDeleteFramebuffers(1, &fb);

		// check vertex visibility
//		static auto cm = Danvil::ContinuousIntervalColorMapping<float, float>::Factor_Red_Green();
//		cm.setRange(-1, +1);
		std::vector<float> occlusion(boost::num_vertices(graph), 1.0f);
		for(auto vid : as_range(boost::vertices(graph))) {
			Superpoint& sp = graph[vid];
			Eigen::Vector3f p_cam = T * sp.center.position;
			Eigen::Vector3f n_cam = T.rotation() * sp.center.normal;
			// project to image
			Eigen::Vector2f q = camera.project(p_cam);
			PixelLocation px = Discretize(q);
			if(px.inRange(width, height)) {
				// read depth
				double q = buf_depth[px.x + (height - px.y - 1)*width]; // OpenGL image is flipped!
				double d = - cZNear * cZFar / (q * (cZFar - cZNear) - cZFar);
				// compute depth buffer error tolerance
//				double tau_dogl = - cZFar * cZNear * (1.0/(q*(cZFar-cZNear)+(cZFar-cZNear)/cDepthMax-cZFar) - 1.0/(q*(cZFar-cZNear)-cZFar));
				float tau_prj = p_cam[2] / (camera.focal * std::abs(n_cam[2])) * n_cam.norm();
				// error
				float error = p_cam[2] - d;
				// compare depth
				if(error < cDepthTolerance + tau_prj) {
					occlusion[vid] = 0.0f;
				}
				else {
					occlusion[vid] = 0.5f;
					//std::cout << "q=" << q << ", z=" << p_cam[2] << ", depth=" << d << ", err=" << error << ", tau_dogl=" << tau_dogl << ", tau_prj=" << tau_prj << std::endl;
				}
//				Danvil::Colorf color = cm(error / tau_prj);
//				sp.center.color = Eigen::Vector3f(color.r, color.g, color.b);
			}
			// TODO check neighbourhood?
		}

		std::cout << "GraphNodeQuadsOcclusion finished." << std::endl;
		return occlusion;
	}

}
