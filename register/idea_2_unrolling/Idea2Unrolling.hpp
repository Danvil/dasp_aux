/*
 * Idea2Unrolling.hpp
 *
 *  Created on: May 7, 2012
 *      Author: david
 */

#ifndef IDEA2UNROLLING_HPP_
#define IDEA2UNROLLING_HPP_

#include "../IDaspMerger.hpp"

class Idea2Unrolling
: public IDaspMerger
{
public:
	SuperModel merge(const std::vector<Frame>& frames);
};

#endif
