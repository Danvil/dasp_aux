/*
 * UnrollModel.hpp
 *
 *  Created on: May 8, 2012
 *      Author: david
 */

#ifndef UNROLLMODEL_HPP_
#define UNROLLMODEL_HPP_

#include "../Frame.hpp"
#include "../SuperModel.hpp"
#include <boost/graph/adjacency_list.hpp>

struct PixelLocation
{
	int x, y;

	inline int index(int width, int height) {
		return x + y * width;
	}

	inline bool inRange(int width, int height) {
		return (0 <= x && x < width && 0 <= y && y < height);
	}

};

inline PixelLocation Discretize(const Eigen::Vector2f& p) {
	return PixelLocation{ static_cast<int>(p[0] + 0.5f), static_cast<int>(p[1] + 0.5f) };
}

namespace unroll
{

	struct Cluster : public ::Superpoint
	{
		Eigen::Vector3f occlusion_color;
		Eigen::Vector3f role_color;

		void reduceSurfacePoints(unsigned int n);
	};

	typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, Cluster> graph_t;

	struct Model
	{
		static constexpr unsigned int cPointsPerNode = 100;

		typedef graph_t::vertex_descriptor vertex_id_t;
		typedef graph_t::vertex_iterator vertex_it_t;
		typedef graph_t::edge_descriptor edge_id_t;
		typedef graph_t::edge_iterator edge_it_t;

		Model();

		void merge(const dasp::Superpixels& superpixels, const Transformation& T);

		float computeIdealDensity() const;

		SuperModel toModel() const;

		unsigned int numNodes() const {
			return boost::num_vertices(graph_);
		}

	private:
		void setNodeData(const dasp::Cluster& cluster, const dasp::ImagePoints& imagepoints, const Transformation& T, vertex_id_t vid);

		void addNodeData(const dasp::Cluster& cluster, const dasp::ImagePoints& imagepoints, const Transformation& T, vertex_id_t vid);

		/** Just adds superpixels and neighbourhood graph */
		void merge_first(const dasp::Superpixels& superpixels, const Transformation& T);

		/** Merges information from this view into existing model */
		void merge_step(const dasp::Superpixels& superpixels, const Transformation& T);

	private:
		bool is_empty_;
		unsigned int num_merged_;
		float superpoint_radius_;
		graph_t graph_;

	};

}

#endif
