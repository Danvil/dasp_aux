/*
 * Idea2Unrolling.cpp
 *
 *  Created on: May 7, 2012
 *      Author: david
 */

#include "Idea2Unrolling.hpp"
#include "UnrollModel.hpp"
#include <iostream>

std::vector<Eigen::Vector2f> Project(const std::vector<Eigen::Vector3f>& points, const dasp::Camera& camera)
{
	std::vector<Eigen::Vector2f> u(points.size());
	for(unsigned int i=0; i<points.size(); i++) {
		u[i] = camera.project(points[i]);
	}
	return u;
}

std::vector<Eigen::Vector2f> Project(const std::vector<Eigen::Vector3f>& points, const Transformation& T, const dasp::Camera& camera)
{
	std::vector<Eigen::Vector2f> u(points.size());
	for(unsigned int i=0; i<points.size(); i++) {
		u[i] = camera.project(T * points[i]);
	}
	return u;
}

std::vector<Eigen::Vector3f> GetClustersPositions(const dasp::Superpixels& superpixels, const Transformation& T)
{
	std::vector<Eigen::Vector3f> clusters;
	for(const dasp::Cluster& c : superpixels.cluster) {
		clusters.push_back(T * c.center.world);
	}
	return clusters;
}

SuperModel Idea2Unrolling::merge(const std::vector<Frame>& frames)
{
	unroll::Model result;
	for(unsigned int frame_id=0; frame_id<frames.size(); frame_id++) {
		std::cout << "Frame " << frame_id << std::endl;
		result.merge(frames[frame_id].dasp.superpixel, frames[frame_id].T_global);
	}
	std::cout << "Creating model" << std::endl;
	return result.toModel();
}
