/*
 * OcclusionTest.hpp
 *
 *  Created on: May 21, 2012
 *      Author: david
 */

#ifndef OCCLUSIONTEST_HPP_
#define OCCLUSIONTEST_HPP_

#include "UnrollModel.hpp"
#include <vector>

namespace unroll
{

	std::vector<float> PointRenderOcclusion(const graph_t& graph, const Transformation& T, const dasp::Camera& camera, unsigned int width, unsigned int height);

	std::vector<float> GraphNodeQuadsOcclusion(graph_t& graph, const Transformation& T, const dasp::Camera& camera, unsigned int width, unsigned int height, float super_radius);

}

#endif
