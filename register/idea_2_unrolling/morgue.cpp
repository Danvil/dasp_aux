/*
 * morgue.cpp
 *
 *  Created on: May 8, 2012
 *      Author: david
 */

std::vector<Eigen::Vector3f> ComputeVisibleClusters(const dasp::Superpixels& superpixels, const Transformation& T)
{
	std::vector<Eigen::Vector3f> clusters;
	for(const dasp::Cluster& c : superpixels.cluster) {
		Eigen::Vector3f n = T.rotation() * c.center.computeNormal();
		if(n[2] > 0.0f) {
			std::cout << n.transpose() << std::endl;
			continue;
		}
		Eigen::Vector3f p = T * c.center.world;
		clusters.push_back(p);
	}
	return clusters;
}

slimage::Image1f ProjectImagePoints(const dasp::ImagePoints& points, const Transformation& T, const dasp::Camera& camera)
{
	slimage::Image1f result(points.width(), points.height(), slimage::Pixel1f{0.0f});
	for(unsigned int i=0; i<points.size(); i++) {
		Eigen::Vector3f n = T.rotation() * points[i].computeNormal();
		if(n[2] > 0.0f) {
			continue;
		}
		Eigen::Vector2f q = camera.project(T * points[i].world);
		float x = q[0];
		float y = q[1];
		int xi = static_cast<int>(x);
		int yi = static_cast<int>(y);
		if(xi < 0 || static_cast<int>(points.width()) <= xi+1 || yi < 0 || static_cast<int>(points.height()) <= yi+1) {
			// TODO points at border should be assigned partially ...
			continue;
		}
		float px = x - static_cast<float>(xi);
		float py = y - static_cast<float>(yi);
		float qx = 1.0f - px;
		float qy = 1.0f - py;
		result(xi, yi) += qx * qy;
		result(xi+1, yi) += px * qy;
		result(xi, yi+1) += qx * py;
		result(xi+1, yi+1) += px * py;
	}
	return result;
}

std::vector<float> ComputeClusterOcclusion(const dasp::Superpixels& superpixels, const Transformation& T, const dasp::Camera& camera)
{
	struct OcclusionQuery {
		unsigned int accepted;
		unsigned int rejected;
	};
	struct Particle {
		Eigen::Vector3f position;
		Eigen::Vector3f normal;
		int label;
	};
	struct BufferPoint {
		bool filled;
		int label;
	};
	// get point labels
	slimage::Image1i labels = superpixels.ComputeLabels();
	// initialize particles
	std::vector<Particle> particles;
	particles.reserve(superpixels.points.size());
	for(unsigned int i=0; i<superpixels.points.size(); i++) {
		const dasp::Point& p = superpixels.points[i];
		if(p.isInvalid()) {
			continue;
		}
		particles.push_back(Particle{
			T * p.world,
			T.rotation() * p.computeNormal(),
			labels[i] });
	}
	// sort particles by depth (nearest to camera first)
	std::sort(particles.begin(), particles.end(), [](const Particle& x, const Particle& y) {
		return x.position[2] < y.position[2];
	});
	// initialize cluster occlusion queries
	std::vector<OcclusionQuery> cluster_queries(superpixels.clusterCount(), OcclusionQuery{0,0});
	// prepare renderbuffer
	std::vector<BufferPoint> buffer(superpixels.points.size(), BufferPoint{false,-1});
	// "render" particles
	int width = superpixels.width();
	int height = superpixels.height();
	for(const Particle& p : particles) {
		if(p.normal[2] < 0) {
			Eigen::Vector2f q = camera.project(p.position);
			PixelLocation px = Discretize(q);
			if(px.inRange(width, height)) {
				BufferPoint& bp = buffer[px.index(width, height)];
				if(!bp.filled || bp.label == p.label) {
					bp.filled = true;
					bp.label = p.label;
					cluster_queries[p.label].accepted ++;
					continue;
				}
			}
		}
		// else
		cluster_queries[p.label].rejected ++;
	}
	// compute occlusion percentage
	std::vector<float> occlusion(cluster_queries.size());
	for(unsigned int i=0; i<occlusion.size(); i++) {
		const OcclusionQuery& q = cluster_queries[i];
		occlusion[i] = static_cast<float>(q.accepted) / static_cast<float>(q.accepted + q.rejected);
	}
	return occlusion;
}

