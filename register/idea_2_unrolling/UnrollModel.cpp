/*
 * UnrollModel.cpp
 *
 *  Created on: May 8, 2012
 *      Author: david
 */

#include "UnrollModel.hpp"
#include "OcclusionTest.hpp"
#include <dasp/Superpixels.hpp>
#include <dasp/Plots.hpp>
#include <dasp/impl/Sampling.hpp>
#include <Slimage/Gui.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/graph/sequential_vertex_coloring.hpp>
#include <iostream>

namespace unroll
{

	void Cluster::reduceSurfacePoints(unsigned int n)
	{
		if(points.size() > n) {
			// randomly shuffle points and pick first n
			std::random_shuffle(points.begin(), points.end());
			points.resize(n);
		}
	}

	Model::Model()
	: is_empty_(true) {
		num_merged_ = 0;
	}

	void Model::merge(const dasp::Superpixels& superpixels, const Transformation& T)
	{
		if(is_empty_) {
			merge_first(superpixels, T);
			is_empty_ = false;
		}
		else{
			merge_step(superpixels, T);
		}
		num_merged_++;
	}

	float Model::computeIdealDensity() const
	{
		return 1.0f / (superpoint_radius_ * superpoint_radius_ * boost::math::constants::pi<float>());
	}

	SuperModel Model::toModel() const
	{
		const bool cUseDebugColor = false;
		SuperModel model;
		// add points
		for(auto vid : as_range(boost::vertices(graph_))) {
			const Cluster& c = graph_[vid];
			Eigen::Vector3f color = (cUseDebugColor ? c.role_color : c.center.color);
			Superpoint sp = c;
			sp.center.color = color;
//			model.surface_points.insert(model.surface_points.end(), c.points.begin(), c.points.end());
			for(Point p : sp.points) {
				p.color = color;
			}
			model.superpoints.push_back(sp);
		}
		// add edges
		float rho = computeIdealDensity();
		static auto cm = Danvil::ContinuousIntervalColorMapping<unsigned char, float>::Factor_Blue_Red_Yellow();
		cm.setRange(1.0f, 1.5f*rho);
		cm.useCustomBorderColors(Danvil::Color::Black, Danvil::Color::White);
		edge_it_t ei, ei_end;
		for(boost::tie(ei, ei_end)=boost::edges(graph_); ei!=ei_end; ++ei) {
			auto a = boost::source(*ei, graph_);
			auto b = boost::target(*ei, graph_);
			SuperModel::Edge edge;
			edge.a = a;
			edge.b = b;
			edge.color_a = Eigen::Vector3f(1,1,1);
			edge.color_b = Eigen::Vector3f(1,1,1);
			model.edges.push_back(edge);
		}
		return model;
	}

	void Model::setNodeData(const dasp::Cluster& dasp_cluster, const dasp::ImagePoints& imagepoints, const Transformation& T, vertex_id_t vid)
	{
		Cluster& cluster = graph_[vid];
		cluster.points.clear();
		addNodeData(dasp_cluster, imagepoints, T, vid);
		cluster.center = T * Point{dasp_cluster.center.world, dasp_cluster.center.computeNormal(), dasp_cluster.center.color};
	}

	void Model::addNodeData(const dasp::Cluster& dasp_cluster, const dasp::ImagePoints& imagepoints, const Transformation& T, vertex_id_t vid)
	{
		Cluster& cluster = graph_[vid];
		// TODO merge center info?
		cluster.center = T * Point{dasp_cluster.center.world, dasp_cluster.center.computeNormal(), dasp_cluster.center.color};
		cluster.points.reserve(cluster.points.size() + dasp_cluster.pixel_ids.size());
		for(unsigned int pid : dasp_cluster.pixel_ids) {
			const dasp::Point& img_point = imagepoints[pid];
			cluster.points.push_back(T * Point{img_point.world, img_point.computeNormal(), img_point.color});
		}
		//cluster.reduceSurfacePoints(cPointsPerNode);
		cluster.computeConvexHull();
	}

	void Model::merge_first(const dasp::Superpixels& superpixels, const Transformation& T)
	{
		// add nodes
		for(unsigned int i=0; i<superpixels.cluster.size(); i++) {
			vertex_id_t vid = boost::add_vertex(graph_);
			setNodeData(superpixels.cluster[i], superpixels.points, T, vid);
		}
		// add edges
		dasp::BorderPixelGraph Gnb = dasp::CreateNeighborhoodGraph(superpixels);
		for(auto eid : as_range(boost::edges(Gnb))) {
			vertex_id_t va = dasp::source_superpixel_id(eid, Gnb);
			vertex_id_t vb = dasp::target_superpixel_id(eid, Gnb);
			boost::add_edge(va, vb, graph_);
		}
		std::cout << "#nodes=" << boost::num_vertices(graph_) << ", #edges=" << boost::num_edges(graph_) << std::endl;
	}

	void Model::merge_step(const dasp::Superpixels& superpixels_inp, const Transformation& T_cam_to_global)
	{
		Transformation T_global_to_cam = T_cam_to_global.inverse();

		dasp::Superpixels superpixels = superpixels_inp;
		const dasp::Camera& camera = superpixels.opt.camera;
		int width = superpixels.width();
		int height = superpixels.height();
		// TODO assure that internal camera parameters are always the same

		// compute occlusion values for each cluster
		std::vector<float> occlusion = GraphNodeQuadsOcclusion(graph_, T_global_to_cam, camera, width, height, superpixels.opt.base_radius);
		std::vector<bool> is_in_frustrum(occlusion.size(), false);

		// get visible nodes as cluster seeds
		std::vector<Eigen::Vector2f> clusters_visible;
		std::vector<dasp::Seed> old_seeds;
		std::map<unsigned int, vertex_id_t> seedid_to_nodeid;
		std::vector<bool> graph_ref_node_is_visible(numNodes());
		std::vector<bool> graph_ref_node_is_fixed(numNodes());
		for(auto vid : as_range(boost::vertices(graph_))) {
			unsigned int i = static_cast<unsigned int>(vid);
			bool is_visible = false;
			Eigen::Vector3f p_cam = T_global_to_cam * graph_[vid].center.position;
			Eigen::Vector2f p_screen = camera.project(p_cam);
			PixelLocation px = Discretize(p_screen);
			if(px.inRange(width, height)) {
				is_in_frustrum[i] = true;
				if(occlusion[i] < 0.25f) {
					// add to visible nodes
					clusters_visible.push_back(p_screen);
					seedid_to_nodeid[clusters_visible.size() - 1] = vid;
					is_visible = true;
					// create seed point
					float scala = superpixels.opt.base_radius * superpixels.opt.camera.focal / p_cam[2]; // HACK
					old_seeds.push_back(dasp::Seed::Static(px.x, px.y, scala,
							p_cam, graph_[vid].center.color, graph_[vid].center.normal));
					old_seeds.back().is_fixed = false;
				}
			}
			graph_ref_node_is_visible[i] = is_visible;
			graph_ref_node_is_fixed[i] = !is_visible;
		}
		std::cout << "#visible nodes = " << clusters_visible.size() << std::endl;

		// remove all edges which connects two visible nodes
		// for each seed vertex (=visible vertices)
		for(unsigned int i=0; i<old_seeds.size(); i++) {
			// get global vertex id
			vertex_id_t center_vid = seedid_to_nodeid[i];
			assert(graph_ref_node_is_visible[static_cast<unsigned int>(center_vid)]);
			// find all neighbouring nodes
			std::vector<vertex_id_t> connected_vertices;
			for(auto aid : as_range(boost::adjacent_vertices(center_vid, graph_))) {
				connected_vertices.push_back(aid);
			}
			// iterate over neighbouring nodes
			for(vertex_id_t v : connected_vertices) {
				// visible nodes with an invisible neighbour shall be fixed for dasp
				if(!graph_ref_node_is_visible[static_cast<unsigned int>(v)]) {
					// mark vertex as not moving
					old_seeds[i].is_fixed = true;
					graph_ref_node_is_fixed[static_cast<unsigned int>(center_vid)] = true;
				}
				else {
					// remove edge
					boost::remove_edge(center_vid, v, graph_);
				}
			}
		}

//#if 0
		// compute density
		slimage::Image1f target_density = dasp::ComputeDepthDensity(superpixels.points, superpixels.opt);
		slimage::Image1f provided_density = dasp::ComputeDepthDensityFromSeeds(clusters_visible, target_density);
		slimage::Image1f delta_density = slimage::abs(target_density - provided_density);

		// add more seeds to cover density
		// FIXME are we getting bugs when using small scala seeds?
		std::vector<dasp::Seed> seeds = dasp::FindSeedsDelta(superpixels.points, old_seeds, delta_density, false);
		std::cout << "#old_seeds=" << old_seeds.size() << ", #seeds" << seeds.size() << std::endl;

		// computer DASP with these seeds
		superpixels.ComputeSuperpixels(seeds);

		// add new nodes and set data for changed nodes
		for(unsigned int i=0; i<superpixels.cluster.size(); i++) {
			const dasp::Cluster& cluster = superpixels.cluster[i];
			int seed_id = cluster.seed_id;
			// add clusters which are not in the reference model
			if(seed_id >= clusters_visible.size()) {
				// add a new superpixel node
				vertex_id_t vid = boost::add_vertex(graph_);
				seedid_to_nodeid[seed_id] = vid;
				// set node data
				setNodeData(cluster, superpixels.points, T_cam_to_global, seedid_to_nodeid[seed_id]);
			}
			else {
				// TODO merge with old cluster information
				setNodeData(cluster, superpixels.points, T_cam_to_global, seedid_to_nodeid[seed_id]);
			}
		}

		// add edges from the new neighbourhood graph
		dasp::BorderPixelGraph graph_cur = dasp::CreateNeighborhoodGraph(superpixels);
		for(auto eid : as_range(boost::edges(graph_cur))) {
			int sa = superpixels.cluster[dasp::source_superpixel_id(eid, graph_cur)].seed_id;
			int sb = superpixels.cluster[dasp::target_superpixel_id(eid, graph_cur)].seed_id;
			vertex_id_t va = seedid_to_nodeid[sa];
			vertex_id_t vb = seedid_to_nodeid[sb];
			boost::add_edge(va, vb, graph_);
		}

		// remove all new nodes which have been inserted between fixed and invisible vertices
		// algorithm: remove all new nodes which only have fixed neighbours
		// FIXME this is problematic as a) it does not remove all malicious vertices
		// and b) it may remove desired vertices in narrow areas
		std::vector<graph_t::vertex_descriptor> vertices_to_be_deleted;
		for(auto vid : as_range(boost::vertices(graph_))) {
			// test if new
			if(static_cast<unsigned int>(vid) < occlusion.size()) {
				continue;
			}
			// test adjacent vertices
			int num_vital = 0;
			int num_new = 0;
			for(auto aid : as_range(boost::adjacent_vertices(vid, graph_))) {
				// test if vital
				if(aid < occlusion.size() && !graph_ref_node_is_fixed[static_cast<unsigned int>(aid)]) {
					num_vital ++;
				}
				if(aid >= occlusion.size()) {
					num_new ++;
				}
			}
			if(num_vital == 0 && num_new <= 1) {
				vertices_to_be_deleted.push_back(vid);
			}
		}
		// remove vertices
		// FIXME HACK biggest to smallest works for vector!
		std::sort(vertices_to_be_deleted.begin(), vertices_to_be_deleted.end(), [](vertex_id_t x, vertex_id_t y) {
			return x > y;
		});
		for(auto vid : vertices_to_be_deleted) {
			boost::clear_vertex(vid, graph_);
			boost::remove_vertex(vid, graph_);
		}

		std::cout << "#nodes=" << boost::num_vertices(graph_) << ", #edges=" << boost::num_edges(graph_) << std::endl;
//#endif

//		{
//			typedef graph_t::vertices_size_type vertex_size_t;
//			typedef boost::property_map<graph_t, boost::vertex_index_t>::const_type vertex_index_map;
//			// color vertex graph
//			std::vector<vertex_size_t> color_vec(boost::num_vertices(graph_));
//			boost::iterator_property_map<vertex_size_t*, vertex_index_map> color(&color_vec.front(), boost::get(boost::vertex_index, graph_));
//			vertex_size_t num_colors = boost::sequential_vertex_coloring(graph_, color);
//			std::cout << "Colors required: " << num_colors << std::endl;
//			// generate colors
//			std::vector<slimage::Pixel3ub> colors = { {{255,0,0}}, {{0,255,0}}, {{0,0,255}}, {{192,192,0}}, {{192,0,192}}, {{0,192,192}} };
//			if(colors.size() < num_colors) {
//				std::vector<slimage::Pixel3ub> colors_plus = dasp::plots::CreateRandomColors(num_colors - colors.size());
//				colors.insert(colors.end(), colors_plus.begin(), colors_plus.end());
//			}
//			// set colors
//			for(std::size_t i=0; i<color_vec.size(); i++) {
//				slimage::Pixel3ub px_color = colors[color_vec[i]];
//				if(occlusion[i] <= 0.5f) {
//					px_color[0] = px_color[0] / 2;
//					px_color[1] = px_color[1] / 2;
//					px_color[2] = px_color[2] / 2;
//				}
//				graph_[i].debug_color = Eigen::Vector3f(float(px_color[0])/255.0f, float(px_color[1])/255.0f, float(px_color[2])/255.0f);
//			}
//
//			for(unsigned int i=0; i<superpixels.cluster.size(); i++) {
//				const dasp::Cluster& cluster = superpixels.cluster[i];
//				int seed_id = cluster.seed_id;
//				if(seed_id >= clusters_visible.size()) {
//					Eigen::Vector3f& color = graph_[seedid_to_nodeid[seed_id]].debug_color;
//					color[0] = color[0] == 0 ? 0.5f : 1.0f;
//					color[1] = color[1] == 0 ? 0.5f : 1.0f;
//					color[2] = color[2] == 0 ? 0.5f : 1.0f;
//				}
//			}
//		}

//		// set debug color
//		for(unsigned int i=0; i<occlusion.size(); i++) {
//			graph_[i].debug_color = (occlusion[i] > 0.5f) ? Eigen::Vector3f(0,1,1) : Eigen::Vector3f(1,0,0);
//		}
//		for(unsigned int i=0; i<superpixels.cluster.size(); i++) {
//			const dasp::Cluster& cluster = superpixels.cluster[i];
//			int seed_id = cluster.seed_id;
//			if(seed_id >= clusters_visible.size()) {
//				graph_[seedid_to_nodeid[seed_id]].debug_color = Eigen::Vector3f(1,1,0);
//			}
//		}

		// set debug color
		for(unsigned int i=0; i<occlusion.size(); i++) {
			if(is_in_frustrum[i]) {
				slimage::Pixel3ub px_color = dasp::plots::IntensityColor(1.0f - occlusion[i], 0, 1);
				graph_[i].occlusion_color = Eigen::Vector3f(float(px_color[0])/255.0f, float(px_color[1])/255.0f, float(px_color[2])/255.0f);
			}
			else {
				graph_[i].occlusion_color = Eigen::Vector3f::Zero();
			}
			graph_[i].role_color = Eigen::Vector3f(0,0,1);
		}
//#if 0
		for(unsigned int i=0; i<superpixels.cluster.size(); i++) {
			const dasp::Cluster& cluster = superpixels.cluster[i];
			int seed_id = cluster.seed_id;
			vertex_id_t node_id = seedid_to_nodeid[seed_id];
			if(seed_id >= clusters_visible.size()) {
				graph_[node_id].occlusion_color = Eigen::Vector3f(1,1,1);
				graph_[node_id].role_color = Eigen::Vector3f(0,1,0);
			}
			else {
				if(cluster.is_fixed) {
					graph_[node_id].role_color = Eigen::Vector3f(1,0,0);
				}
				else {
					graph_[node_id].role_color = Eigen::Vector3f(1,1,0);
				}
			}
		}
//#endif

//		{	// image point visualization
//			slimage::Image1f result = ProjectImagePoints(superpixels_ref.points, T, camera);
//			slimage::gui::Show("projected points", result, 1.0f, 0);
//		}
/*		{	// cluster visibility visualization
			static auto cm = Danvil::ContinuousIntervalColorMapping<unsigned char, float>::Factor_Red_Green();
			cm.setRange(0.0f, 1.0f);

			// cyan -> visible
			// red -> occluded
			// yellow -> new

			slimage::Image3ub vis(width, height, slimage::Pixel3ub{{0,0,0}});
			for(unsigned int i=0; i<occlusion.size(); i++) {
				const Eigen::Vector3f& p_world = graph_[i].center.position;
				Eigen::Vector2f p_screen = camera.project(T_global_to_cam * p_world);
				PixelLocation px = Discretize(p_screen);
				if(px.inRange(width, height)) {
					//Danvil::Colorub color = cm(occlusion[i]);
					Danvil::Colorub color = (occlusion[i] > 0.5f) ? Danvil::Colorub::Cyan : Danvil::Colorub::Red;
					vis[px.index(width, height)] = slimage::Pixel3ub{{color.r,color.g,color.b}};
				}
			}
			for(unsigned int i=0; i<superpixels.cluster.size(); i++) {
				const dasp::Cluster& cluster = superpixels.cluster[i];
				int seed_id = cluster.seed_id;
				if(seed_id >= clusters_visible.size()) {
					const Eigen::Vector3f& p_world = graph_[seedid_to_nodeid[seed_id]].center.position;
					Eigen::Vector2f p_screen = camera.project(T_global_to_cam * p_world);
					PixelLocation px = Discretize(p_screen);
					if(px.inRange(width, height)) {
						vis[px.index(width, height)] = slimage::Pixel3ub{{255,255,0}};
					}
				}
			}
			slimage::gui::Show("cluster occlusion value", vis, 0);
		}
		{	// density visualization
			slimage::gui::Show("provided density", provided_density, 30.0f, 0);

			slimage::Image3ub vis_density_delta(target_density.width(), target_density.height());
			for(unsigned int i=0; i<target_density.size(); i++) {
				vis_density_delta[i] = dasp::plots::PlusMinusColor(target_density[i] - provided_density[i], 0.025f);
			}
			slimage::gui::Show("delta density", vis_density_delta, 0);

			slimage::Image1f completed_density = dasp::ComputeDepthDensityFromSeeds(seeds, target_density);
			slimage::gui::Show("completed density", completed_density, 30.0f, 0);
		}
		{	// plot new dasp
			slimage::Image3ub vis = dasp::plots::PlotClusters(superpixels, dasp::plots::ClusterPoints, dasp::plots::Color);
			slimage::gui::Show("new dasp", vis, 0);
		}
		slimage::gui::WaitForKeypress();*/
	}

}
