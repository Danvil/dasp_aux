/*
 * Idea1Merging.cpp
 *
 *  Created on: May 7, 2012
 *      Author: david
 */

#include "Idea1Merging.hpp"
#include "SuperpointModel.hpp"

SuperModel Idea1Merging::merge(const std::vector<Frame>& frames)
{
	std::vector<SuperpointModel> frame_models;
	for(const Frame& f : frames) {
		frame_models.push_back(SuperpointModel(f.dasp.superpixel, f.T_global));
	}
	SuperpointModel super = SuperpointModel::Merge(frame_models);
	return super.toModel();
}
