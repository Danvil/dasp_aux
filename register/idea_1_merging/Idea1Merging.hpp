/*
 * Idea1Merging.hpp
 *
 *  Created on: May 7, 2012
 *      Author: david
 */

#ifndef IDEA1MERGING_HPP_
#define IDEA1MERGING_HPP_

#include "../IDaspMerger.hpp"

class Idea1Merging
: public IDaspMerger
{
public:
	SuperModel merge(const std::vector<Frame>& frames);
};

#endif
