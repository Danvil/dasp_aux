/*
 * SuperpointModel.cpp
 *
 *  Created on: May 2, 2012
 *      Author: david
 */

#include "SuperpointModel.hpp"
#include "../morgue/PointOctree.hpp"
#include <dasp/impl/BlueNoise.hpp>
#include <dasp/Neighbourhood.hpp>
#include <dasp/Plots.hpp>
#include <boost/graph/astar_search.hpp>
#include <boost/math/constants/constants.hpp>
#include <GL/glew.h>
#include <GL/gl.h>
#include <iostream>

using namespace dasp;

std::vector<Eigen::Vector3f> ThinPoints(const std::vector<Eigen::Vector3f>& points, float density, float cellsize)
{
	// create point octree
	Vector3fOctree<Eigen::Vector3f> octree;
	octree.resize(points.begin(), points.end(), cellsize);
	octree.add(points.begin(), points.end());
	// run thinning
	// read back remaining points
	return octree.extract();
}

SuperpointModel::SuperpointModel(const Superpixels& superpixels, const Eigen::Affine3f& T, bool use_edges, int layer)
{
	add(superpixels, T, use_edges, layer);
}

void SuperpointModel::add(const Superpixels& superpixels, const Eigen::Affine3f& T, bool use_edges, int layer)
{
	superpoint_radius_ = superpixels.opt.base_radius;
	// use superpixel as superpoints
	std::vector<Graph::vertex_descriptor> vids;
	vids.reserve(superpixels.cluster.size());
	for(const dasp::Cluster& p : superpixels.cluster) {
		auto v = boost::add_vertex(graph_);
		vids.push_back(v);
		graph_[v].position = T * p.center.world;
		graph_[v].color = p.center.color;
		graph_[v].normal = T.linear() * p.center.computeNormal();
		graph_[v].density_same = 0.0f;
		graph_[v].density_foreign = 0.0f;
		graph_[v].layer = layer;
	}
	// add edges
	if(use_edges) {
		dasp::BorderPixelGraph Gnb = dasp::CreateNeighborhoodGraph(superpixels);
		for(auto eid : as_range(boost::edges(Gnb))) {
			boost::add_edge(
					dasp::source_superpixel_id(eid, Gnb),
					dasp::target_superpixel_id(eid, Gnb), graph_); // exploit that vertex descriptors are integers
		}
	}
	std::cout << "frame: nodes=" << numNodes() << ", edges=" << numEdges() << std::endl;
}

void SuperpointModel::add(const SuperpointModel& model, bool use_edges, int layer)
{
	// add superpoints
	std::map<Graph::vertex_descriptor, Graph::vertex_descriptor> vids;
	vertex_it_t vi, vi_end;
	for(boost::tie(vi, vi_end)=boost::vertices(model.graph_); vi!=vi_end; ++vi) {
		auto v = boost::add_vertex(graph_);
		vids[*vi] = v;
		graph_[v] = model.graph_[*vi];
		graph_[v].density_same = 0.0f;
		graph_[v].density_foreign = 0.0f;
		graph_[v].layer = layer;
	}
	// add edges
	if(use_edges) {
		edget_it_t ei, ei_end;
		for(boost::tie(ei, ei_end)=boost::edges(model.graph_); ei!=ei_end; ++ei) {
			Graph::vertex_descriptor va = vids[boost::source(*ei, model.graph_)];
			Graph::vertex_descriptor vb = vids[boost::target(*ei, model.graph_)];
			addEdge(va, vb);
		}
	}
}

void SuperpointModel::connect(float dist_factor)
{
	const float dmax = dist_factor * 2.0f * superpoint_radius_;
	const float dmax2 = dmax * dmax;
	vertex_it_t vi, vi_end;
	for(boost::tie(vi, vi_end)=boost::vertices(graph_); vi!=vi_end; ++vi) {
		vertex_it_t vj = vi;
		++vj;
		for(; vj!=vi_end; ++vj) {
			float w2 = (graph_[*vi].position - graph_[*vj].position).squaredNorm();
			if(w2 < dmax2) {
				addEdge(*vi, *vj, std::sqrt(w2));
			}
		}
	}
}

float SuperpointModel::computeIdealDensity() const
{
	return 1.0f / (superpoint_radius_ * superpoint_radius_ * boost::math::constants::pi<float>());
}

void SuperpointModel::computeDensity()
{
	float rho = computeIdealDensity();
	std::cout << "Computing node density (rho=" << rho << ") ..." << std::flush;
	// iterate over all vertices and set density to zero
	vertex_it_t vi, vi_end;
	for(boost::tie(vi, vi_end)=boost::vertices(graph_); vi!=vi_end; ++vi) {
		graph_[*vi].density_same = 0.0f;
		graph_[*vi].density_foreign = 0.0f;
	}
	// iterate over all vertices and compute superpoint density in local neighbourhood graph
	for(boost::tie(vi, vi_end)=boost::vertices(graph_); vi!=vi_end; ++vi) {
		Graph::vertex_descriptor v = *vi;
//			Graph::vertex_descriptor v = boost::vertex(100, graph_);
		propagateVertexDensityAdd(v, rho);
	}
	std::cout << "Finished." << std::endl;
}

constexpr bool cUseLayering = false;

void SuperpointModel::removeDenseVertices(float node_density_factor)
{
	float rho = computeIdealDensity();
	std::cout << "Removing dense vertices (rho=" << rho << ") ..." << std::flush;

	// remove one layer after the other so that the topology grid is only changed in areas with overlap.
	// -> less re-adjusting
	// -> less difficult re-meshing
	std::vector<int> layer_ids;
	{
		std::set<int> layer_ids_set;
		Graph::vertex_iterator vit, vend;
		for(boost::tie(vit, vend) = boost::vertices(graph_); vit!=vend; ++vit) {
			layer_ids_set.insert(graph_[*vit].layer);
		}
		layer_ids = std::vector<int>(layer_ids_set.begin(), layer_ids_set.end());
		std::sort(layer_ids.begin(), layer_ids.end());
	}
	if(!cUseLayering) {
		layer_ids = { layer_ids.back() + 1 };
	}

	// greedy find vertices and remove them by deleting their density
	// work layer by layer
	// do not directly remove vertices to not damage the topology graph
	std::vector<vertex_desc_t> removed_vertices;
	for(auto lit=layer_ids.begin(); lit!=layer_ids.end(); ++lit) {
		int layer_id = *lit;
		while(true) {
			// find next best vertex
			Graph::vertex_iterator vstart, vend;
			boost::tie(vstart, vend) = boost::vertices(graph_);
			Graph::vertex_iterator it = std::min_element(vstart, vend, [this,layer_id](vertex_desc_t a, vertex_desc_t b) {
				// first vertices with the correct layer, then remaining vertices
				const Superpoint& spa = graph_[a];
				const Superpoint& spb = graph_[b];
				if(cUseLayering) {
					// each section ordered by density
					if((spa.layer == layer_id) == (spb.layer == layer_id) ) {
						// same section
						return spa.density_foreign > spb.density_foreign;
					}
					else {
						// different section
						if(spa.layer == layer_id) {
							return true;
						}
						else {
							return false;
						}
					}
				}
				else {
					return spa.density_foreign + spa.density_same > spb.density_foreign + spb.density_same;
				}
			});
			if(it == vend) {
				break;
			}
			vertex_desc_t v = *it;
			// check if density from other layers is too big
			if(cUseLayering) {
				// check for correct layer id
				if(graph_[v].layer != layer_id) {
					break;
				}
				if(graph_[v].density_foreign < node_density_factor*rho) {
					break;
				}
			}
			else {
				if(graph_[v].density_foreign + graph_[v].density_same < node_density_factor*rho) {
					break;
				}
			}
			// remove vertex
//			std::cout << "Removing " << v << " (density=" << graph_[v].density << ")" << std::endl;
			propagateVertexDensityRemove(v, rho);
			graph_[v].density_same = 0.0f;
			graph_[v].density_foreign = 0.0f;
			removed_vertices.push_back(v);
		}
	}

	// now also remove vertices from graph
	// hacking boost graph library. remove vertex with biggest index first. what a mess.
	std::sort(removed_vertices.begin(), removed_vertices.end(), [](vertex_desc_t a, vertex_desc_t b) {
		return a > b;
	});
	for(vertex_desc_t v : removed_vertices) {
		boost::clear_vertex(v, graph_);
		boost::remove_vertex(v, graph_);
	}

	std::cout << "Removed " << removed_vertices.size() << " vertices." << std::endl;
}

SuperModel SuperpointModel::toModel() const
{
	SuperModel model;
	// add points
	vertex_it_t vi, vi_end;
	for(boost::tie(vi, vi_end)=boost::vertices(graph_); vi!=vi_end; ++vi) {
		const Superpoint& sp = graph_[*vi];
		::Superpoint p;
		p.center.position = sp.position;
		p.center.normal = sp.normal;
		p.center.color = sp.color;
		model.superpoints.push_back(p);
	}
	// add edges
	float rho = computeIdealDensity();
	static auto cm = Danvil::ContinuousIntervalColorMapping<unsigned char, float>::Factor_Blue_Red_Yellow();
	cm.setRange(1.0f, 1.5f*rho);
	cm.useCustomBorderColors(Danvil::Color::Black, Danvil::Color::White);

	edget_it_t ei, ei_end;
	for(boost::tie(ei, ei_end)=boost::edges(graph_); ei!=ei_end; ++ei) {
		auto a = boost::source(*ei, graph_);
		auto b = boost::target(*ei, graph_);
		SuperModel::Edge edge;
		edge.a = a;
		edge.b = b;
		Danvil::Colorub color_a = cm(graph_[a].density_same + graph_[a].density_foreign);
		Danvil::Colorub color_b = cm(graph_[b].density_same + graph_[b].density_foreign);
		edge.color_a = Eigen::Vector3f(static_cast<float>(color_a.r)/255.0f, static_cast<float>(color_a.g)/255.0f, static_cast<float>(color_a.b)/255.0f);
		edge.color_b = Eigen::Vector3f(static_cast<float>(color_b.r)/255.0f, static_cast<float>(color_b.g)/255.0f, static_cast<float>(color_b.b)/255.0f);
		model.edges.push_back(edge);
	}
	return model;
}

void SuperpointModel::render()
{
	// render points
	{
//			float d_min = +1e12;
//			float d_max = -1e12;
		glPointSize(3.0f);
		glBegin(GL_POINTS);
		vertex_it_t vi, vi_end;
		for(boost::tie(vi, vi_end)=boost::vertices(graph_); vi!=vi_end; ++vi) {
			const Superpoint& sp = graph_[*vi];
			glColor3fv(sp.color.data());
			glVertex3fv(sp.position.data());
//				d_min = std::min(d_min, sp.density);
//				d_max = std::max(d_max, sp.density);
		}
		glEnd();
//			std::cout << "min=" << d_min << ", max=" << d_max << std::endl;
	}

	// render edges
	{
		float rho = computeIdealDensity();
		static auto cm = Danvil::ContinuousIntervalColorMapping<unsigned char, float>::Factor_Blue_Red_Yellow();
		cm.setRange(1.0f, 1.5f*rho);
		cm.useCustomBorderColors(Danvil::Color::Black, Danvil::Color::White);

		glBegin(GL_LINES);
		glColor3f(0.65f, 0.65f, 0.65f);
		edget_it_t ei, ei_end;
		for(boost::tie(ei, ei_end)=boost::edges(graph_); ei!=ei_end; ++ei) {
			auto a = boost::source(*ei, graph_);
			auto b = boost::target(*ei, graph_);

			// color by density
			Danvil::Colorub color_a = cm(graph_[a].density_same + graph_[a].density_foreign);
			Danvil::Colorub color_b = cm(graph_[b].density_same + graph_[b].density_foreign);
			glColor3ub(color_a.r, color_a.g, color_a.b);
			glVertex3fv(graph_[a].position.data());
			glColor3ub(color_b.r, color_b.g, color_b.b);
			glVertex3fv(graph_[b].position.data());

//				// real color
//				glColor3fv(graph_[a].color.data());
//				glVertex3fv(graph_[a].position.data());
//				glColor3fv(graph_[b].color.data());
//				glVertex3fv(graph_[b ].position.data());
		}
		glEnd();
	}

}

SuperpointModel SuperpointModel::Merge(const std::vector<SuperpointModel>& models)
{
//		constexpr float cNodeDensityFactor = 0.1f;
	constexpr float cNodeDensityFactor = 1.25f;
	constexpr float cNodeConnectionFactor = 1.5f;

	SuperpointModel big;
	assert(models.size() > 0);
	big.superpoint_radius_ = models[0].superpoint_radius_;
	for(unsigned int i=0; i<models.size(); i++) {
		const SuperpointModel& m = models[i];
		assert(big.superpoint_radius_ == m.superpoint_radius_);
		big.add(m, true, i);
	}
	big.connect(cNodeConnectionFactor); // connect only vertices between layers
	std::cout << "Big: frames=" << models.size() << ", nodes=" << big.numNodes() << ", edges=" << big.numEdges() << std::endl;
	big.computeDensity();

//		{
//			Graph::vertex_iterator vstart, vend;
//			boost::tie(vstart, vend) = boost::vertices(big.graph_);
//			float d_min = 1e9, d_mean = 0, d_max = -1e9;
//			std::for_each(vstart, vend, [&big,&d_mean,&d_min,&d_max](vertex_desc_t a) {
//				float d = big.graph_[a].density;
//				d_mean += d;
//				d_min = std::min(d_min, d);
//				d_max = std::max(d_max, d);
//			});
//			d_mean /= boost::num_vertices(big.graph_);
//			std::cout << d_min << " " << d_mean << " " << d_max << std::endl;
//			float rho = models[0].computeIdealDensity();
//			std::cout << d_min/rho << " " << d_mean/rho << " " << d_max/rho << std::endl;
//		}

	big.removeDenseVertices(cNodeDensityFactor);

	std::cout << "Finished density." << std::endl;
	return big;
}

void SuperpointModel::addEdge(const Graph::vertex_descriptor& va, const Graph::vertex_descriptor& vb)
{
	float w = (graph_[va].position - graph_[vb].position).norm();
	addEdge(va, vb, w);
}

void SuperpointModel::addEdge(const Graph::vertex_descriptor& va, const Graph::vertex_descriptor& vb, float w)
{
	Graph::edge_descriptor e;
	bool inserted;
	boost::tie(e, inserted) = boost::add_edge(va, vb, graph_);
	boost::put(boost::edge_weight_t(), graph_, e, w);
}

namespace PropagateDensity
{

	struct out_of_range_t {};

	// this will break if the distance is greater than the kernel distance
	template<class Graph, bool Add=true>
	class density_visitor
	: public boost::default_astar_visitor
	{
	public:
		typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
		density_visitor(Graph& graph, Vertex source, float rho, const std::vector<float>& d)
		: graph_(graph), source_point_(graph_[source]), rho_(rho), visits_(0), d(d) {}
		~density_visitor() {
		}
		void examine_vertex(Vertex u, const Graph&) {
			SuperpointModel::Superpoint& sp = graph_[u];
			//float d2 = (sp.position - source_point_.position).squaredNorm();
			float d2 = d[u] * d[u];
			float rho_d2 = rho_ * d2;
			if(rho_d2 > dasp::BlueNoise::KernelRange * dasp::BlueNoise::KernelRange) {
//					std::cout << "final=" << rho_d2 << ", visits: " << visits_ << std::endl;
				throw out_of_range_t();
			}
			else {
				// increment density
				float density_delta = rho_ * dasp::BlueNoise::KernelFunctorSquare(rho_d2);
				float* density = (source_point_.layer == sp.layer) ? &sp.density_same : &sp.density_foreign;
				if(Add) {
					*density += density_delta;
				}
				else {
					*density -= density_delta;
					if(*density < 0) {
						*density = 0;
					}
				}
				//std::cout << "rho_d2=" << rho_d2 << ", d_eucl=" << std::sqrt(d2) << ", d_graph=" << d[u] << ", density=" << sp.density << std::endl;
			}
			visits_++;
		}
	private:
		Graph& graph_;
		const SuperpointModel::Superpoint& source_point_;
		float rho_;
		const std::vector<float>& d;
		unsigned int visits_;
	};


	template<class Graph, bool Add=true>
	void PropagateDensity(Graph& graph_, typename boost::graph_traits<Graph>::vertex_descriptor v, float rho)
	{
		std::vector<float> d(boost::num_vertices(graph_));
		// iterate over vertices in the local graph neighbourhood
		// consider topological distance (distance walking the graph)
		try {
			boost::astar_search(graph_, v,
					boost::astar_heuristic<Graph, float>(), // zero heuristic always expands nearest
					boost::distance_map(&d[0]).visitor(PropagateDensity::density_visitor<Graph,Add>(graph_, v, rho, d))
				);
		}
		catch(PropagateDensity::out_of_range_t&) {
			// finished
		}
	}

}

void SuperpointModel::propagateVertexDensityAdd(const vertex_desc_t& v, float rho)
{
	PropagateDensity::PropagateDensity<Graph,true>(graph_, v, rho);
}

void SuperpointModel::propagateVertexDensityRemove(const vertex_desc_t& v, float rho)
{
	PropagateDensity::PropagateDensity<Graph,false>(graph_, v, rho);
}
