/*
 * SuperpointModel.hpp
 *
 *  Created on: May 2, 2012
 *      Author: david
 */

#ifndef DASPMERGER_HPP_
#define DASPMERGER_HPP_

#include "../SuperModel.hpp"
#include <dasp/Superpixels.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <Eigen/Dense>
#include <Eigen/Geometry>

/** Thins a list of points such that the desired density (number/cube of unit length) is achieved */
std::vector<Eigen::Vector3f> ThinPoints(const std::vector<Eigen::Vector3f>& points, float density, float cellsize);

/** A superpoint model is a graph together with a set of points
 * The graph nodes are the superpoints.
 * The graph edges define a topology on superpoints.
 * Each superpoint is connected to a subset of the points.
 */
class SuperpointModel
{
public:
	SuperpointModel() {}

	SuperpointModel(const dasp::Superpixels& superpixels, const Eigen::Affine3f& T=Eigen::Affine3f::Identity(), bool use_edges=true, int layer=0);

	void add(const dasp::Superpixels& superpixels, const Eigen::Affine3f& T=Eigen::Affine3f::Identity(), bool use_edges=true, int layer=0);

	void add(const SuperpointModel& model, bool use_edges=true, int layer=0);

	/** Connects superpoints with nearest neighbours
	 * All nodes which are nearer than 2 * R * dist_factor are connected
	 * Edges already present are preserved!
	 */
	void connect(float dist_factor);

	/** Computes the desired node density based on superpoint radius */
	float computeIdealDensity() const;

	/** Computes node density for all superpoints */
	void computeDensity();

	/** Removes vertices to such that the target density is reached approximatelly */
	void removeDenseVertices(float node_density_factor);

	/** Renders superpoints and connecting edges */
	void render();

	unsigned int numNodes() const {
		return boost::num_vertices(graph_);
	}

	unsigned int numEdges() const {
		return boost::num_edges(graph_);
	}

	SuperModel toModel() const;

	static SuperpointModel Merge(const std::vector<SuperpointModel>& models);

private:
	struct Superpoint {
		Eigen::Vector3f position;
		Eigen::Vector3f color;
		Eigen::Vector3f normal;
		float density_same; // density from same layer
		float density_foreign; // density from other layers
		int layer;
	};

	typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
			Superpoint,
			boost::property<boost::edge_weight_t, float>> Graph;

	typedef boost::graph_traits<Graph>::vertex_iterator vertex_it_t;
	typedef boost::graph_traits<Graph>::edge_iterator edget_it_t;
	typedef boost::graph_traits<Graph>::vertex_descriptor vertex_desc_t;

private:
	void addEdge(const vertex_desc_t& va, const vertex_desc_t& vb);
	void addEdge(const vertex_desc_t& va, const vertex_desc_t& vb, float w);

	void propagateVertexDensityAdd(const vertex_desc_t& v, float rho);
	void propagateVertexDensityRemove(const vertex_desc_t& v, float rho);

private:
	float superpoint_radius_;

	Graph graph_;

};

#endif
