/*
 * PointOctree.hpp
 *
 *  Created on: May 2, 2012
 *      Author: david
 */

#ifndef POINTOCTREE_HPP_
#define POINTOCTREE_HPP_

#include <dasp/Superpixels.hpp>
#include <octree.h>
#include <iostream>
#include <vector>
#include <map>

namespace dasp
{

	struct Index {
		int x, y, z;
	};

	struct IdentityPositionExtractor {
		template<typename K>
		const Eigen::Vector3f& operator()(const K& p) const {
			return p;
		}
	};

	/**
	 * F = { Point -> Eigen::Vector3f }, extracts point position
	 */
	template<typename Point=Eigen::Vector3f, typename PositionExtractor=IdentityPositionExtractor>
	struct Vector3fOctree
	{
		typedef std::vector<Point> cell_t;
		typedef Octree<cell_t> octree_t;

		Vector3fOctree() {}

		/**
		 * It is an iterator which yields elements of type Point
		 */
		template<typename It>
		void resize(It begin, It end, float cell_size) {
			Eigen::Vector3f pmin(1e9, 1e9, 1e9);
			Eigen::Vector3f pmax(-1e9, -1e9, -1e9);
			for(; begin!=end; ++begin) {
				const Eigen::Vector3f& p = position_extractor_(*begin);
				pmin.cwiseMin(p);
				pmax.cwiseMax(p);
			}
			resize(pmin, pmax, cell_size);
		}

		void resize(const Eigen::Vector3f& pmin, const Eigen::Vector3f& pmax, float cell_size) {
			cell_size_ = cell_size;
			// max side length of cube
			float delta = (pmax - pmin).maxCoeff();
			// compute octree size
			size_ = 2 + static_cast<int>(delta / cell_size_ + 0.5f); // assure some safty border
			// choose next highest power of 2
			if((size_ - 1) & size_ != 0) {
				std::cout << "old=" << size_;
				size_ = size_ + (size_ & (-size_));
				std::cout << ", new=" << size_ << std::endl;
			}
			// set range
			float range = static_cast<float>(size_) * cell_size_;
			range_min_ = 0.5f * (pmax + pmin) - 0.5f * range * Eigen::Vector3f(1, 1, 1);
			range_max_ = 0.5f * (pmax + pmin) + 0.5f * range * Eigen::Vector3f(1, 1, 1);
			// create octree
			octree_impl_.reset(new octree_t(size_));
		}

		Index position2index(const Eigen::Vector3f& position) const {
			Eigen::Vector3f u = static_cast<float>(size_) * (position - range_min_) / cell_size_;
			int ux = static_cast<int>(u[0] + 0.5f);
			int uy = static_cast<int>(u[1] + 0.5f);
			int uz = static_cast<int>(u[2] + 0.5f);
			return Index{ux, uy, uz};
		}

		bool isValid(const Index& i) const {
			return !(i.x < 0 || size_ <= i.x || i.y < 0 || size_ <= i.y || i.z < 0 || size_ <= i.z);
		}

		void add(const Point& p) {
			// get octree coordinates
			Index i = position2index(position_extractor_(p));
			if(!isValid(i)) {
				// point is of of bounds
				std::cerr << "Point is out of bounds! - skipped." << std::endl;
				return;
			}
			// add point (creates node if necessary)
			cell_t& cell = (*octree_impl_)(i.x, i.y, i.z);
			cell.push_back(p);
		}

		/**
		 * It is an iterator which yields elements of type Point
		 */
		template<typename It>
		void add(It begin, It end) {
			for(; begin!=end; ++begin) {
				add(*begin);
			}
		}

		std::vector<Point> extract() const {
			std::vector<Point> points;
			octree_impl_->foreach([&points](const cell_t& cell) {
				points.insert(points.begin(), cell.begin(), cell.end());
			});
			return points;
		}

		/** Checks all points which have a distance smaller or equal than 'radius' from 'position'
		 * The functor 'f' is called for all points in range.
		 * F = { Point -> {} }
		 */
		template<typename F>
		void foreachPointInRange(const Eigen::Vector3f& position, float radius, F f) const {
			float radius2 = radius * radius;
			Index i = position2index(position);
			int q = static_cast<float>(radius / cell_size_) + 1; // round up (value is positive)
			int w = 2*q + 1;
			for(int z=i.z-q; z<=i.z+q; z++) {
				for(int y=i.y-q; y<=i.y+q; y++) {
					for(int x=i.x-q; x<=i.x+q; x++) {
						const cell_t& cell = octree_impl_->at(x, y, z);
						for(const Point& cp : cell) {
							if((position_extractor_(cp) - position).squaredNorm() <= radius2) {
								f(cp);
							}
						}
					}
				}
			}
		}

	protected:
		PositionExtractor position_extractor_;
		boost::shared_ptr<octree_t> octree_impl_;

		float cell_size_;
		Eigen::Vector3f range_min_, range_max_;
		int size_;

	};

	struct CellPoint
	{
		// global point index
		unsigned int id;
		// frame index
		unsigned int frame_id;
		// point index
		unsigned int point_id;
		// point data
		dasp::Point data;

		const Eigen::Vector3f& position() const {
			return data.world;
		}
	};

	struct CellPointPosition {
		const Eigen::Vector3f& operator()(const CellPoint& p) const {
			return p.position();
		}
	};

	class PointOctree
	: public Vector3fOctree<CellPoint, CellPointPosition>
	{
	public:
		PointOctree() {}

		PointOctree(const std::vector<Superpixels>& frame_superpixel, float cell_size);

		unsigned int numTotalPoints() const {
			return frame_superpixels_.size() * frame_superpixels_[0].points.size();
		}

		std::vector<unsigned int> findPointIndicesInRange(const Eigen::Vector3f& position, float radius) const {
			std::vector<unsigned int> indices;
			foreachPointInRange(position, radius, [&indices](const CellPoint& p) { indices.push_back(p.id); });
			return indices;
		}

	private:
		void createOctree();

		void addPoints();

	private:
		unsigned int num_points_per_frame_;

		std::vector<Superpixels> frame_superpixels_;
	};

}

#endif
