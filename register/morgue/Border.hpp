/*
 * Border.hpp
 *
 *  Created on: May 7, 2012
 *      Author: david
 */

#ifndef BORDER_HPP_
#define BORDER_HPP_

#include "../Frame.hpp"
#include <vector>

/** Computes and renders superpixel 2D border points */
struct Border
{
	Border();

	void addFinishedFrame(const Frame& frame);

	void render();

private:
	std::vector<Eigen::Vector3f> border_points_;

	unsigned int cnt_frames_;

};

#endif
