/*
 * Border.cpp
 *
 *  Created on: May 7, 2012
 *      Author: david
 */

#include "Border.hpp"
#include <Candy/OpenGL.h>

Border::Border()
{
	cnt_frames_ = 0;
}

void Border::addFinishedFrame(const Frame& frame)
{
	std::vector<Eigen::Vector3f> u = frame.computeBorderPointsGlobal();
	border_points_.insert(border_points_.end(), u.begin(), u.end());
	cnt_frames_++;
}

void Border::render()
{
	if(cnt_frames_ == 0) {
		return;
	}
	float alpha = 0.25f / static_cast<float>(cnt_frames_);
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glPointSize(3.0f);
	glColor4f(1.0f,1.0f,1.0f, alpha);
	glBegin(GL_POINTS);
	for(const Eigen::Vector3f& p : border_points_) {
		glVertex3fv(p.data());
	}
	glEnd();
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
}
