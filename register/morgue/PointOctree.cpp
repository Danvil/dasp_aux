/*
 * PointOctree.cpp
 *
 *  Created on: May 2, 2012
 *      Author: david
 */

#include "PointOctree.hpp"
#include <octree.h>
#include <iostream>

namespace dasp
{

	PointOctree::PointOctree(const std::vector<Superpixels>& frame_superpixel, float cell_size)
	{
		frame_superpixels_ = frame_superpixel;
		cell_size_ = cell_size;
		createOctree();
		addPoints();
	}

	void PointOctree::createOctree()
	{
		assert(frame_superpixels_.size() == 0);
		// fixed cell size
		const float cell_radius = frame_superpixels_[0].opt.base_radius;
		num_points_per_frame_ = frame_superpixels_.size();
		// find min and max point
		Eigen::Vector3f pmin(1e9, 1e9, 1e9);
		Eigen::Vector3f pmax(-1e9, -1e9, -1e9);
		for(unsigned int frame_id=0; frame_id<frame_superpixels_.size(); frame_id++) {
			const Superpixels& super = frame_superpixels_[frame_id];
			assert(super.points.size() == num_points_per_frame_);
			assert(super.opt.base_radius == cell_radius && super.opt.count == 0);
			for(unsigned int i=0; i<super.points.size(); i++) {
				const dasp::Point& u = super.points[i];
				if(u.isValid()) {
					continue;
				}
				const Eigen::Vector3f& p = u.world;
				pmin.cwiseMin(p);
				pmax.cwiseMax(p);
			}
		}
		resize(pmin, pmax, cell_size_);
		std::cout << "Octree range: size=" << size_ << ", min=" << range_min_ << ", max=" << range_max_ << std::endl;
	}

	void PointOctree::addPoints()
	{
		assert(size_ != 0);
		for(unsigned int frame_id=0; frame_id<frame_superpixels_.size(); frame_id++) {
			const Superpixels& super = frame_superpixels_[frame_id];
			for(unsigned int i=0; i<super.points.size(); i++) {
				CellPoint cp;
				cp.id = frame_id * num_points_per_frame_ + i;
				cp.frame_id = frame_id;
				cp.point_id = i;
				cp.data = super.points[i];
				if(cp.data.isValid()) {
					add(cp);
				}
			}
		}
	}

}
