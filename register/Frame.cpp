/*
 * Frame.cpp
 *
 *  Created on: May 22, 2012
 *      Author: david
 */

#include "Frame.hpp"
#include <Slimage/IO.hpp>
#include <boost/math/constants/constants.hpp>

void Rgbd::load(const std::string& fn)
{
	color = slimage::Load3ub(fn + "_color.png");
	depth = slimage::Load1ui16(fn + "_depth.pgm");
}

void DaspPointSet::createWithDasp(const Rgbd& u)
{
	// dasp parameters
	const float R = 0.04f;
	dasp::Parameters opt;
	opt.base_radius = R;
	opt.camera = dasp::Camera{320.0f, 240.0f, 540.0f, 0.001f};
	opt.is_repair_depth = false;
	opt.ignore_pixels_with_bad_visibility = true;
//	// provide safe depth image
//	// condition: A = pi * R^2 <= N_min * s_px^2
//	//   => s_px <= sqrt(pi / N_min) * R
//	// s_px = z / f * sqrt(1 + ||nabla z||^2)
//	// (nabla z)_i <= tan(alpha_max)
//	const float cAlphaMax = 60.0f / 180.0f * boost::math::constants::pi<float>();
//	constexpr unsigned int cNmin = 5;
//	float threshold = std::sqrt(1 + 2.0f * std::pow(std::tan(cAlphaMax), 2)) * std::sqrt(static_cast<float>(cNmin) / boost::math::constants::pi<float>()) / opt.camera.focal;
//	std::cout << "Threshold: " << threshold << std::endl;
//	for(unsigned int i=0; i<u.depth.size(); i++) {
//		uint16_t d = u.depth[i];
//		float z = opt.camera.convertKinectToMeter(d);
//		if(z * threshold > opt.base_radius) {
//			d = 0;
//		}
//		u.depth[i] = d;
//	}
	// compute dasp
	superpixel = dasp::ComputeSuperpixels(u.color, u.depth, opt);
}
