/*
 * register.cpp
 *
 *  Created on: Apr 10, 2012
 *      Author: david
 */

#include "TransformationProvider.hpp"
#include "ModelProvider.hpp"
#include "KinectGrabberObject.hpp"
#include <dasp/Plots.hpp>
#include <Candy.h>
#include <Slimage/Slimage.hpp>
#include <Slimage/IO.hpp>
#include <Slimage/Gui.hpp>
#include <Eigen/Dense>
#include <boost/bind.hpp>
#include <boost/format.hpp>
#include <boost/math/constants/constants.hpp>
#include <iostream>
#include <vector>

void RenderPoints(const PointSet& pnts, float size)
{
	glPointSize(size);
	glBegin(GL_POINTS);
	for(const Point& p : pnts) {
		glColor3fv(p.color.data());
		glVertex3fv(p.position.data());
	}
	glEnd();
}

class MyControlObject
: public Candy::IUpdateable,
  public Candy::IRenderable
{
public:
	MyControlObject(const Batch::Ptr& batch) {
		obj_T_.reset(new TransformationProvider(batch));
		obj_model_.reset(new ModelProvider(batch));
		auto_update_ = true;
		phase_ = 0;
		is_finished_ = false;
	}

	bool auto_update_;

	void update(double dt, double current_time) {
		if(auto_update_) {
			onStep();
		}
	}

	void onStep() {
		if(phase_ == 0) {
			obj_T_->next();
			if(!obj_T_->isFinished()) {
				std::cout << "Computing transformation..." << std::endl;
				phase_++;
				is_finished_ = false;
			}
			else {
				is_finished_ = true;
			}
		}
		else if(phase_ == 1) {
			obj_T_->update();
			if(obj_T_->isFinished()) {
				phase_++;
			}
		}
		else if(phase_ == 2) {
			std::cout << "Computing model..." << std::endl;
			obj_model_->next();
			phase_++;
		}
		else if(phase_ == 3) {
			obj_model_->update();
			if(obj_model_->isFinished()) {
				phase_++;
			}
		}
		if(phase_ >= 4) {
			phase_ = 0;
			auto_update_ = false;
		}
	}

	bool isFinished() const {
		return is_finished_;
	}

	void render() {
		if(phase_ == 1) {
			obj_T_->render();
		}
		obj_model_->render();
	}

private:
	boost::shared_ptr<TransformationProvider> obj_T_;
	boost::shared_ptr<ModelProvider> obj_model_;
	int phase_;
public:
	bool is_finished_;


};


#include <Candy/System/GlutSystem.h>

//#define USE_KINECT

int main(int argc, char** argv)
{
	const std::string cDatabasePath = "/home/david/Documents/DataSets/dasp_register_dataset/";
//	const std::string cDatabasePath = "/tmp/";

	const std::string p_dataset_name = "004_handheld/kinect";
	const unsigned int p_dataset_image_count = 107;
	const unsigned int p_dataset_skip = 2;
	const bool use_ground_truth_T = false;

//	const std::string p_dataset_name = "005_sim_spheres/sim";
//	const unsigned int p_dataset_image_count = 5;
//	const unsigned int p_dataset_skip = 1;
//	const bool use_ground_truth_T = true;

//	const std::string p_dataset_name = "sim";
//	const unsigned int p_dataset_image_count = 5;
//	const unsigned int p_dataset_skip = 1;
//	const bool use_ground_truth_T = true;

	Candy::EnginePtr engine = Candy::Engine::CreateDefault();

	boost::format p_fn(cDatabasePath + p_dataset_name + "_%03d");

//	boost::shared_ptr<IterativeClosestPoints> icp(new IterativeClosestPoints());
//	icp->pnts_source = DaspPointSet(Rgbd((p_fn % 1).str()));
//	icp->pnts_target = DaspPointSet(Rgbd((p_fn % 3).str()));
//	engine->getScene()->addItem(icp);
//	engine->getUpdater()->addUpdateable(icp);

	Batch::Ptr batch(new Batch());

	boost::shared_ptr<MyControlObject> icp_obj(new MyControlObject(batch));

#ifdef USE_KINECT
	boost::shared_ptr<KinectGrabberObject> kinect_grabber(new KinectGrabberObject());
	kinect_grabber->setNotification([micp](slimage::Image1ui16 depth, slimage::Image3ub color) {
		if(micp->isFinished()) {
			micp->addFrame(Rgbd{color, depth});
			micp->next();
		}
	});
	engine->getUpdater()->add(kinect_grabber);
	icp_obj->render_all_ = true;
#else
//	for(unsigned int i=0; i<p_dataset_image_count; i+=p_dataset_skip) {
//		micp->addFrame(Rgbd::Load((p_fn % i).str()));
//	}
//	micp->addFrame(Rgbd::Load((p_fn % 1).str()));
//	micp->addFrame(Rgbd::Load((p_fn % 3).str()));
	std::vector<Eigen::Matrix4f> Ts;
	engine->getUpdater()->addFunctor([icp_obj,batch,&p_fn,p_dataset_image_count,&Ts](double,double) {
		static unsigned int frame_loaded = 0;
		if(frame_loaded >= p_dataset_image_count) {
			return;
		}
		if(icp_obj->isFinished()) {
			icp_obj->is_finished_ = false;
			std::cout << "Loading frame " << frame_loaded << std::endl;
			std::string fn = (p_fn % frame_loaded).str();
			// load frame
			Frame::Ptr frame = batch->add(Rgbd::Load(fn));
			// load transformation
			if(use_ground_truth_T) {
				std::ifstream ifs(fn + ".txt");
				float data[16];
				for(unsigned int i=0; i<16; i++) {
					ifs >> data[i];
				}
				Eigen::Matrix4f m = Eigen::Matrix4f(data).transpose();
				Ts.push_back(m);
				Eigen::Matrix4f delta = Ts.front().inverse() * Ts.back();
				frame->T_is_ground_truth = true;
				frame->T_global = Eigen::Affine3f(delta);
			}
			// go
			frame_loaded++;
		}
	});
#endif

	engine->getScene()->addItem(icp_obj);
	engine->getUpdater()->add(icp_obj);

	engine->setClearColor(Danvil::Colorf(0,0,0));

	engine->getKeyboardListener()->set('s', icp_obj, &MyControlObject::onStep);
	engine->getKeyboardListener()->set('a', [&icp_obj]() {
		icp_obj->auto_update_ = !icp_obj->auto_update_;
	});

	Candy::GlutSystem sys(engine, "dasp register");
	return sys.main(argc, argv);
}
