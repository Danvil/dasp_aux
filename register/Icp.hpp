/*
 * Icp.hpp
 *
 *  Created on: May 7, 2012
 *      Author: david
 */

#ifndef ICP_HPP_
#define ICP_HPP_

#include "PointSet.hpp"
#include "Frame.hpp"
#include <Eigen/Dense>
#include <boost/bind.hpp>
#include <boost/math/constants/constants.hpp>
#include <iostream>

namespace impl {

	struct NearestPairing
	{
		Pairings operator()(const PointSet& pnts_source, const PointSet& pnts_target) {
			unsigned int n = pnts_source.size();
			std::vector<Pairings::Pair> u(n);
			for(unsigned int i=0; i<n; i++) {
				Pairings::Pair& p = u[i];
				p.source_id = i;
				p.target_id = FindClosestPoint(pnts_target, pnts_source[i]);
				p.weight = 1.0f;
			}
			return Pairings{u};
		}
	};
}

/** Performs iterative closest points for depth-adative superpixels */
struct IterativeClosestPoints
{
public:
	typedef boost::function<Pairings(const PointSet& pnts)> PairingFunctionType;

	IterativeClosestPoints() {
		is_ready = false;
	}

	const Transformation& getTransformation() const {
		return T;
	}

	const Pairings& getPairings() const {
		return pairings_;
	}

	const PointSet& getSourcePoints() const {
		return pnts_source_0;
	}

	const PointSet& getTargetPoints() const {
		return pnts_target;
	}

	void start(const PointSet& pnts_src, const PointSet& pnts_dst, const Transformation& T_initial, PairingFunctionType f) {
		pairing_function_ = f;
		pnts_source_0 = pnts_src;
		pnts_target = pnts_dst;
		T = T_initial;

		PointSet pnts_source = transform(T, pnts_source_0);
		pairings_ = pairing_function_(pnts_source);
	}

	bool isReady() const {
		return is_ready;
	}

	void step() {
		if(is_ready) {
			return;
		}
		PointSet pnts_source = transform(T, pnts_source_0);

		pairings_ = pairing_function_(pnts_source);

		Eigen::Affine3f delta = IcpStepImpl(pnts_source, pnts_target, pairings_.pairings_);
		T = delta * T;
		if(IsNearIdentity(delta)) {
			std::cout << "ICP converged" << std::endl;
			is_ready = true;
			return;
		}
	}

	void run() {
		while(!is_ready) {
			step();
		}
	}

private:
	PointSet pnts_source_0;
	PointSet pnts_target;
	PairingFunctionType pairing_function_;
	Pairings pairings_;
	Transformation T;
	bool is_ready;

public:
	static bool IsNearIdentity(const Eigen::Affine3f& T) {
		const float cMaxDt = 0.0005f; // 0.5 mm
		const float cMaxDr = 0.5f / 180.0f * boost::math::constants::pi<float>(); // 0.5 deg
		float dt = T.translation().norm();
		float dr = Eigen::Quaternionf(T.rotation()).angularDistance(Eigen::Quaternionf::Identity());
		return dt < cMaxDt && dr < cMaxDr;
	}

	static std::vector<int> ComputePointCorrespondence(const PointSet& pnts_source, const PointSet& pnts_target)
	{
		unsigned int n = pnts_source.size();
		std::vector<int> cp_map(n);
		for(unsigned int i=0; i<n; i++) {
			cp_map[i] = FindClosestPoint(pnts_target, pnts_source[i]);
		}
		return cp_map;
	}

	/** Performs a step in iterative closest points
	 * @param T initial transformation
	 * @param pnts_source point set to move around
	 * @param pnts_target point set to converge to
	 * @param correspondence for each point in pnts_source the corresponding point in pnts_target
	 */
	static Eigen::Affine3f IcpStepImpl(const PointSet& pnts_source, const PointSet& pnts_target, const std::vector<Pairings::Pair>& pairings);

};

#endif
