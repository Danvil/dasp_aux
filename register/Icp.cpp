/*
 * Icp.cpp
 *
 *  Created on: May 7, 2012
 *      Author: david
 */

#include "Icp.hpp"

Eigen::Affine3f IterativeClosestPoints::IcpStepImpl(const PointSet& pnts_source, const PointSet& pnts_target, const std::vector<Pairings::Pair>& pairings)
{
//	std::cout << "Pairings in segment: " << pairings.size() << std::endl;
	if(pairings.size() < 5) {
		return Eigen::Affine3f::Identity();
	}
	// assemble problem accordingly to
	// http://www.cs.princeton.edu/~smr/papers/icpstability.pdf
	typedef Eigen::Matrix<float,6,6> Mat6;
	typedef Eigen::Matrix<float,6,1> Vec6;
	Mat6 A = Mat6::Zero();
	Vec6 b = Vec6::Zero();

	struct Connection {
		Eigen::Vector3f n;
		float dn;
		Eigen::Vector3f c;
	};
	std::vector<Connection> connections(pairings.size());
	for(unsigned int i=0; i<pairings.size(); i++) {
		const Pairings::Pair& pair = pairings[i];
		const Point& pa = pnts_source[pair.source_id];
		const Point& pb = pnts_target[pair.target_id];
		const Eigen::Vector3f& p = pa.position;
		const Eigen::Vector3f& q = pb.position;
		const Eigen::Vector3f& n = pb.normal;
		connections[i].n = n;
		connections[i].dn = n.dot(p - q);
		connections[i].c = p.cross(n);
	}

	// robust icp: optimize only least k points
	const float percent_points_taken = 0.85f;
	unsigned int num_points_taken = static_cast<unsigned int>(percent_points_taken * static_cast<float>(connections.size()));
	if(num_points_taken < connections.size()) {
		std::partial_sort(connections.begin(), connections.begin() + num_points_taken, connections.end(), [](const Connection& x, const Connection& y) {
			return x.dn < y.dn;
		});
	}

	for(auto it=connections.begin(); it!=connections.begin() + num_points_taken; ++it) {
		const Eigen::Vector3f& n = it->n;
		float dn = it->dn;
		const Eigen::Vector3f& c = it->c;
		b[0] += c[0] * dn;
		b[1] += c[1] * dn;
		b[2] += c[2] * dn;
		b[3] += n[0] * dn;
		b[4] += n[1] * dn;
		b[5] += n[2] * dn;
		for(int u=0; u<3; u++) {
			for(int v=u; v<3; v++) {
				float x = c[u] * c[v];
				A(u,v) += x;
				A(v,u) += x;
			}
		}
		for(int u=0; u<3; u++) {
			for(int v=0; v<3; v++) {
				float x = c[u] * n[v];
				A(u,3+v) += x;
				A(3+v,u) += x;
			}
		}
		for(int u=0; u<3; u++) {
			for(int v=u; v<3; v++) {
				float x = n[u] * n[v];
				A(u+3,v+3) += x;
				A(v+3,u+3) += x;
			}
		}
	}
	// solve equations
//	std::cout << A << std::endl;
//	std::cout << b.transpose() << std::endl;
	Vec6 x = A.ldlt().solve(-b);
	// construct transformation matrix
	Eigen::Matrix3f Trot;
	Trot <<
			1, -x[2], x[1],
			x[2], 1, -x[0],
			-x[1], x[0], 1;
	Eigen::Vector3f Tpos;
	Tpos << x[3], x[4], x[5];
	Eigen::Affine3f Tinc;
	Tinc.linear() = Trot;
	Tinc.translation() = Tpos;
	return Tinc;
}

