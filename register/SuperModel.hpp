/*
 * SuperModel.hpp
 *
 *  Created on: May 7, 2012
 *      Author: david
 */

#ifndef SUPERMODEL_HPP_
#define SUPERMODEL_HPP_

#include "PointSet.hpp"
#include <Eigen/Dense>
#include <vector>

struct SuperModel
{
	SuperpointSet superpoints;

	struct Edge {
		unsigned int a, b;
		Eigen::Vector3f color_a, color_b;
	};

	std::vector<Edge> edges;

	void render() const;

};

#endif
