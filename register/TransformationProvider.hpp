/*
 * TransformationProvider.hpp
 *
 *  Created on: May 15, 2012
 *      Author: david
 */

#ifndef TRANSFORMATIONPROVIDER_HPP_
#define TRANSFORMATIONPROVIDER_HPP_

#include "Frame.hpp"
#include "Batch.hpp"
#include "Icp.hpp"

/**
 * Given two frames F_src and F_dst with points representing the same scene
 * computes the relative transformation between the points.
 * Assumes that the global transformation of F_dst is known and computes
 * the global transformation of F_src.
 */
class TransformationProvider
{
public:
	TransformationProvider(const Batch::Ptr& batch);

	void next();

	void update();

	bool isFinished() const;

	void render() const;

private:
	Batch::Ptr batch_;
	unsigned int current_frame_;
	Frame::Ptr f_src_;
	Frame::Ptr f_dst_;
	boost::shared_ptr<IterativeClosestPoints> icp_;

};

#endif
