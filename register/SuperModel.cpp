/*
 * SuperModel.cpp
 *
 *  Created on: May 7, 2012
 *      Author: david
 */

#include "SuperModel.hpp"
#include <Candy/OpenGL.h>

void SuperModel::render() const
{
//	// render superpoint points
//	if(superpoints.size() > 0) {
//		glPointSize(1.0f);
//		for(const Superpoint& sp : superpoints) {
//			sp.renderPoints();
//		}
//	}
	// render superpoint meshes
	if(superpoints.size() > 0) {
		for(const Superpoint& sp : superpoints) {
			sp.renderHull();
		}
	}
	// render edges
	if(edges.size() > 0) {
		glBegin(GL_LINES);
		for(const Edge& e : edges) {
			glColor3fv(e.color_a.data());
			glVertex3fv(superpoints[e.a].center.position.data());
			glColor3fv(e.color_b.data());
			glVertex3fv(superpoints[e.b].center.position.data());
		}
		glEnd();
	}
	// render superpoint centers
	if(superpoints.size() > 0) {
		glPointSize(5.0f);
		glBegin(GL_POINTS);
		for(const Superpoint& sp : superpoints) {
			glColor3fv(sp.center.color.data());
			glVertex3fv(sp.center.position.data());
		}
		glEnd();
	}
	// render superpoint normals
	if(superpoints.size() > 0) {
		glLineWidth(1.0f);
		glBegin(GL_LINES);
		for(const Superpoint& sp : superpoints) {
//			glColor3f(0.0f, 1.0f, 1.0f);
			glColor3fv(sp.center.color.data());
			glVertex3fv(sp.center.position.data());
			Eigen::Vector3f b = sp.center.position + 0.05f * sp.center.normal;
			glVertex3fv(b.data());
		}
		glEnd();
	}
}
