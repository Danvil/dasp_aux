/*
 * PointSet.cpp
 *
 *  Created on: May 21, 2012
 *      Author: david
 */

#include "PointSet.hpp"
#include <Eigen/Eigenvalues>
#include <iostream>
#include <Slimage/Gui.hpp>
#include <Candy/OpenGL.h>

void Superpoint::computeMeans()
{
	assert(points.size() >= 1);
	center.position = Eigen::Vector3f::Zero();
	center.color = Eigen::Vector3f::Zero();
	for(const Point& p : points) {
		center.position += p.position;
		center.color += p.color;
	}
	// normalize position/color
	center.position /= static_cast<float>(points.size());
	center.color /= static_cast<float>(points.size());
}

void Superpoint::computeNormal()
{
	assert(points.size() >= 3);
	// point plane fit
	float xx=0.0f, xy=0.0f, xz=0.0f, yy=0.0f, yz=0.0f, zz=0.0f;
	for(const Point& p : points) {
		float x = p.position[0];
		float y = p.position[1];
		float z = p.position[2];
		xx += x*x;
		xy += x*y;
		xz += x*z;
		yy += y*y;
		yz += y*z;
		zz += z*z;
	}
	Eigen::Matrix3f A; A << xx, xy, xz, xy, yy, yz, xz, yz, zz;
	A /= static_cast<float>(points.size());
	Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> solver;
	solver.compute(A);
	// normal
	center.normal = solver.eigenvectors().col(0);
}

void Superpoint::computeQuad(bool store_normal)
{
	assert(points.size() >= 3);
	// point plane fit
	float xx=0.0f, xy=0.0f, xz=0.0f, yy=0.0f, yz=0.0f, zz=0.0f;
	for(const Point& p : points) {
		Eigen::Vector3f q = p.position - center.position;
		float x = q[0];
		float y = q[1];
		float z = q[2];
		xx += x*x;
		xy += x*y;
		xz += x*z;
		yy += y*y;
		yz += y*z;
		zz += z*z;
	}
	Eigen::Matrix3f A; A << xx, xy, xz, xy, yy, yz, xz, yz, zz;
	A /= static_cast<float>(points.size());
	Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> solver;
	solver.compute(A);
	// normal
	if(store_normal) {
		center.normal = solver.eigenvectors().col(0);
	}
	// plane
	Eigen::Vector3f u = 2.0f * std::sqrt(solver.eigenvalues()[1]) * solver.eigenvectors().col(1);
	Eigen::Vector3f v = 2.0f * std::sqrt(solver.eigenvalues()[2]) * solver.eigenvectors().col(2);
	// find right hand coordinate system
	if(u.cross(v)[2] > 0) {
		std::swap(u, v);
	}
	// create polygon
	polygon = {
			center.position + u + v,
			center.position - u + v,
			center.position - u - v,
			center.position + u - v };
}

float ccw(const Eigen::Vector2f& p1, const Eigen::Vector2f& p2, const Eigen::Vector2f& p3) {
	return (p2.x() - p1.x())*(p3.y() - p1.y()) - (p2.y() - p1.y())*(p3.x() - p1.x());
}

void Superpoint::computeConvexHull()
{
	if(this->points.size() < 3) {
		polygon.clear();
		return;
	}
	// see http://en.wikipedia.org/wiki/Graham_scan
	struct HullPoint {
		Eigen::Vector3f global;
		Eigen::Vector2f local;
		float angle;
	};
	std::vector<HullPoint> points;
	{	// create list with hull points
		Eigen::Vector3f base_pos = center.position;
		Eigen::Vector3f base_normal = center.normal;

		Eigen::Vector3f tangent;
		Eigen::Vector3f c1 = base_normal.cross(Eigen::Vector3f(0.0, 0.0, 1.0));
		Eigen::Vector3f c2 = base_normal.cross(Eigen::Vector3f(0.0, 1.0, 0.0));
		if(c1.squaredNorm() > c2.squaredNorm()) {
		    tangent = c1;
		}
		else {
		    tangent = c2;
		}
		Eigen::Vector3f binormal = base_normal.cross(tangent);

		for(const Point& p : this->points) {
			Eigen::Vector3f w = p.position - base_pos;
			float u = w.dot(tangent);
			float v = w.dot(binormal);
			points.push_back(HullPoint{p.position, Eigen::Vector2f(u, v)});
		}
	}
	{	// swap first point with the point with the lowest y-coordinate
		auto it = std::min_element(points.begin(), points.end(), [](const HullPoint& a, const HullPoint& b) {
			return a.local.y() < b.local.y();
		});
		std::swap(points.front(), *it);
	}
#ifdef MY_TEST
	slimage::Image3ub test(256,256,slimage::Pixel3ub{0,0,0});
#endif
	{	// sort points by polar angle with points[1]
		Eigen::Vector2f base_pos = points[0].local;
		points[0].angle = 0; // must be first!
		for(unsigned int i=1; i<points.size(); i++) {
			HullPoint& hp = points[i];
			// compute polar angle
			Eigen::Vector2f q = hp.local - base_pos;
			hp.angle = std::atan2(q.y(), q.x());
		}
		std::sort(points.begin() + 1, points.end(), [](const HullPoint& a, const HullPoint& b) {
			return a.angle < b.angle;
		});
#ifdef MY_TEST
		std::cout << "Number of points " << points.size() << std::endl;
		for(auto q : points) {
			std::cout << "Angle: " << q.angle << " => " << q.local.x() << "/" << q.local.y() << std::endl;
			int x = static_cast<int>(static_cast<float>(test.width()) * 0.5 * (q.local.x()/0.07 + 1));
			int y = static_cast<int>(static_cast<float>(test.height()) * 0.5 * (q.local.y()/0.07 + 1));
			test(x,y) = slimage::Pixel3ub{255,255,255};
		}
		slimage::gui::Show("hull", test, 100);
#endif
	}
	{	// We want points[0] to be a sentinel point that will stop the loop.
		HullPoint last = points.back();
		points.insert(points.begin(), last);
	}
	//  M will denote the number of points on the convex hull.
	unsigned int m = 1;
	for(unsigned int i=2; i<points.size(); i++) {
#ifdef MY_TEST
		std::cout << "i=" << i << ", m=" << m << std::endl;
		int x = static_cast<int>(static_cast<float>(test.width()) * 0.5 * (points[i].local.x()/0.07 + 1));
		int y = static_cast<int>(static_cast<float>(test.height()) * 0.5 * (points[i].local.y()/0.07 + 1));
		test(x,y) = slimage::Pixel3ub{255,0,0};
		slimage::gui::Show("hull", test, 50);
#endif
		// Find next valid point on convex hull.
		while(ccw(points[m-1].local, points[m].local, points[i].local) <= 0) {
#ifdef MY_TEST
			std::cout << "i=" << i << ", m=" << m << std::endl;
#endif
			if(m > 1) {
				m -= 1;
			}
			else if(i == points.size() - 1) {
				// All points are collinear
				break;
			}
			else {
				i += 1;
			}
		}
		// Update M and swap points[i] to the correct place.
		m += 1;
		std::swap(points[m], points[i]);
	}
#ifdef MY_TEST
	for(unsigned int i=0; i<m; i++) {
		int x = static_cast<int>(static_cast<float>(test.width()) * 0.5 * (points[i].local.x()/0.07 + 1));
		int y = static_cast<int>(static_cast<float>(test.height()) * 0.5 * (points[i].local.y()/0.07 + 1));
		test(x,y) = slimage::Pixel3ub{0,255,255};
	}
	slimage::gui::Show("hull", test, 0);
	slimage::gui::WaitForKeypress();
#endif
	// compute convex hull triangle using a fan around center point
	polygon.clear();
	for(unsigned int i=0; i<m; i++) {
		polygon.push_back(points[i].global);
	}
//	computeQuad(false);
}

void Superpoint::renderPoints() const
{
	glBegin(GL_POINTS);
	for(const Point& p : points) {
		glColor3fv(p.color.data());
		glVertex3fv(p.position.data());
	}
	glEnd();
}

void Superpoint::renderHull() const
{
	if(polygon.size() < 2) {
		return;
	}
	glBegin(GL_TRIANGLE_FAN);
	glColor3fv(center.color.data());
	glNormal3fv(center.normal.data());
	glVertex3fv(center.position.data());
	for(const Eigen::Vector3f& v : polygon) {
		glVertex3fv(v.data());
	}
	glVertex3fv(polygon.front().data());
	glEnd();
}

void Superpoint::renderHullScaled(float q) const
{
	if(polygon.size() < 2) {
		return;
	}
	glBegin(GL_TRIANGLE_FAN);
	glColor3fv(center.color.data());
	glNormal3fv(center.normal.data());
	glVertex3fv(center.position.data());
	for(const Eigen::Vector3f& v : polygon) {
		Eigen::Vector3f w = center.position + q * (v - center.position);
		glVertex3fv(w.data());
	}
	{
		Eigen::Vector3f w = center.position + q * (polygon.front() - center.position);
		glVertex3fv(w.data());
	}
	glEnd();
}

