/*
 * DaspRegistration.cpp
 *
 *  Created on: Apr 20, 2012
 *      Author: david
 */

#include "ModelProvider.hpp"
#include "IDaspMerger.hpp"
#include <dasp/Segmentation.hpp>
#include <dasp/Metric.hpp>
#include <Candy.h>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphml.hpp>
#include <boost/format.hpp>
#include <iostream>
#include <map>
#include <set>

ModelProvider::ModelProvider(const Batch::Ptr& batch)
{
	batch_ = batch;
	current_frame_ = 0;
	last_merged_ = -1;
}

void ModelProvider::next()
{
	if(current_frame_ + 1 >= batch_->numFrames()) {
		return;
	}
	current_frame_ ++;
//	if(current_frame_ % 5 == 0) {
		std::cout << ">>>>> Merging " << current_frame_ << " frames ..." << std::endl;
//		IDaspMerger::Ptr merger = IDaspMerger::Factor("merge");
		IDaspMerger::Ptr merger = IDaspMerger::Factor("unroll");
		std::vector<Frame> frames_taken;
//		for(unsigned int i=0; i<=current_frame_; i++) {
//			frames_taken.push_back(*batch_->at(i));
//		}
		frames_taken.push_back(*batch_->front());
		frames_taken.push_back(*batch_->back());
		model_ = merger->merge(frames_taken);
		std::cout << ">>>>> Merging finished." << std::endl;

		std::cout << ">>>>> Segmenting" << std::endl;
		dasp::ClassicSpectralAffinity<true> metric(model_.superpoints.size(), frames_taken[0].dasp.superpixel.opt.base_radius);
		dasp::EdgeWeightGraph graph_local(model_.superpoints.size());
		for(auto edge : model_.edges) {
			// TODO avoid copying around
			const Point& qa = model_.superpoints[edge.a].center;
			const Point& qb = model_.superpoints[edge.b].center;
			dasp::ClassicSpectralAffinity<true>::Data pa{qa.position, qa.normal, qa.color};
			dasp::ClassicSpectralAffinity<true>::Data pb{qb.position, qb.normal, qb.color};
			float w = metric(pa, pb);
			auto p = boost::add_edge(edge.a, edge.b, graph_local);
			boost::put(boost::edge_weight, graph_local, p.first, w);
		}
		dasp::EdgeWeightGraph graph_seg = dasp::SpectralSegmentation(graph_local, boost::get(boost::edge_weight, graph_seg), 24);
		dasp::ClusterLabeling labels = dasp::ComputeSegmentLabels(graph_seg, 8.5f);
		std::cout << "Number of segments: " << labels.num_labels << std::endl;
		std::vector<slimage::Pixel3ub> colors = dasp::plots::CreateRandomColors(labels.num_labels);
		for(unsigned int i=0; i<model_.superpoints.size(); i++) {
			slimage::Pixel3ub cub = colors[labels.labels[i]];
			model_.superpoints[i].center.color = Eigen::Vector3f(cub[0]/255.0f, cub[1]/255.0f, cub[2]/255.0f);
		}
		std::cout << ">>>>> Segmenting finished" << std::endl;

//	}
	//writeGraphML("register.graphml");
}

void ModelProvider::update()
{
}

bool ModelProvider::isFinished() const
{
	return (current_frame_ + 1 >= batch_->numFrames());
}

void ModelProvider::render() const
{
	model_.render();
}

//std::string ColorToHex(const slimage::Pixel3ub& color) {
//	static boost::format hex("#%02x%02x%02x");
//	return (hex % static_cast<int>(color[0]) % static_cast<int>(color[1]) % static_cast<int>(color[2])).str();
//}
//
//enum vertex_size_t { vertex_size };
//
//namespace boost
//{
//    BOOST_INSTALL_PROPERTY(vertex, size);
//}

void ModelProvider::writeGraphML(const std::string& fn)
{
	// FIXME
	throw 0;
//	using namespace boost;
//	using namespace std;
//	typedef adjacency_list< vecS, vecS, directedS,
//			property<vertex_name_t,string, property<vertex_color_t,string, property<vertex_size_t,unsigned int> > >,
//			property<edge_weight_t,unsigned int>
//			> Graph;
//
//	// connect all superpixels with via pairings
//	Graph g;
//
//	boost::format node_name("node_%02d_%04d");
//	boost::format seg_name("seg_%02d_%03d");
//
//	// add nodes
////	std::vector<std::vector<Graph::vertex_descriptor> > vids(frames_.size());
//	std::vector<std::vector<Graph::vertex_descriptor> > seg_vids(frames_.size());
//	for(unsigned int k=0; k<=current_frame_; k++) {
//		// add segment nodes
//		for(unsigned int i=0; i<frames_[k].countSegments(); i++) {
//			Graph::vertex_descriptor vid = add_vertex(g);
//			seg_vids[k].push_back(vid);
//			// TODO add vertex properties
//			put(vertex_name_t(), g, vid, (seg_name % k % i).str());
//			put(vertex_color_t(), g, vid, ColorToHex(frames_[k].segment_colors[i]));
//			put(vertex_size_t(), g, vid, frames_[k].countSegmentSuperpixels(i));
//		}
////		// add superpixel nodes
////		vids[k].resize(frames_[k].countSuperpixels());
////		for(unsigned int i=0; i<vids[k].size(); i++) {
////			Graph::vertex_descriptor vid = add_vertex(g);
////			vids[k][i] = vid;
////			// TODO add vertex properties
////			put(vertex_name_t(), g, vid, (node_name % k % i).str());
////			put(vertex_color_t(), g, vid, ColorToHex(frames_[k].segment_colors[frames_[k].partition(i)]));
////		}
//	}
//
////	// add segment to superpixel edges
////	for(unsigned int k=0; k<=current_frame_; k++) {
////		for(unsigned int i=0; i<vids[k].size(); i++) {
////			auto r = add_edge(vids[k][i], seg_vids[k][frames_[k].partition(i)], g);
////			assert(r.second);
////			Graph::edge_descriptor eid = r.first;
////			// TODO add edge properties
////			put(edge_weight_t(), g, eid, 0.0f);
////		}
////	}
//
////	// add superpixel frame to frame edges
////	for(unsigned int k=1; k<=current_frame_; k++) {
////		for(const std::vector<Pairings::Pair>& u : frames_[k].pairings.pairings_) {
////			for(const Pairings::Pair& p : u) {
////				auto r = add_edge(vids[k][p.source_id], vids[k-1][p.target_id], g);
////				assert(r.second);
////				Graph::edge_descriptor eid = r.first;
////				// TODO add edge properties
////				put(edge_weight_t(), g, eid, 0.0f);
////			}
////		}
////	}
//
//	// add segment frame to frame edges
//	for(unsigned int k=1; k<=current_frame_; k++) {
//		std::vector<std::map<unsigned int,unsigned int>> refs(frames_[k].countSegments());
//		for(const Pairings::Pair& p : frames_[k].pairings.pairings_) {
//			unsigned int s = frames_[k].partition(p.source_id);
//			unsigned int t = frames_[k-1].partition(p.target_id);
//			refs[s][t] ++;
//		}
//		for(unsigned int i=0; i<refs.size(); i++) {
//			for(std::pair<unsigned int, unsigned int> p : refs[i]) {
//				auto r = add_edge(seg_vids[k][i], seg_vids[k-1][p.first], g);
//				//double q = static_cast<double>(p.second) / static_cast<double>(frames_[k].countSegmentSuperpixels(i));
//				//assert(q <= 1.0);
//				put(edge_weight_t(), g, r.first, p.second);
//			}
//		}
//	}
//
//	std::cout << "Writing graph with " << num_vertices(g) << " vertices and " << num_edges(g) << " edges to file '" << fn << "'" << std::endl;
//
//	// http://thirld.com/blog/2012/01/31/making-yed-import-labels-from-graphml-files/
//	dynamic_properties dp;
//	dp.property("text", get(vertex_name_t(), g));
//	dp.property("color", get(vertex_color_t(), g));
//	dp.property("size", get(vertex_size_t(), g));
//	dp.property("weight", get(edge_weight_t(), g));
//
//	std::ofstream ofs(fn);
//	write_graphml(ofs, g, dp, true);
}

