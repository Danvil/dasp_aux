/*
 * Batch.hpp
 *
 *  Created on: May 15, 2012
 *      Author: david
 */

#ifndef BATCH_HPP_
#define BATCH_HPP_

#include "Frame.hpp"
#include <boost/shared_ptr.hpp>

struct Batch
{
	typedef boost::shared_ptr<Batch> Ptr;

	unsigned int numFrames() const {
		return frames_.size();
	}

	void add(const Frame::Ptr& frame) {
		frames_.push_back(frame);
	}

	const Frame::Ptr& add(const Rgbd& rgbd, bool use_segments=false);

	const Frame::Ptr& at(unsigned int i) const {
		return frames_[i];
	}

	const Frame::Ptr& front() const {
		return frames_.front();
	}

	const Frame::Ptr& back() const {
		return frames_.back();
	}

private:
	std::vector<Frame::Ptr> frames_;

};


#endif
