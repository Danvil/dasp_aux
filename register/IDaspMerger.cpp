/*
 * IDaspMerger.cpp
 *
 *  Created on: May 7, 2012
 *      Author: david
 */

#include "IDaspMerger.hpp"
#include "idea_1_merging/Idea1Merging.hpp"
#include "idea_2_unrolling/Idea2Unrolling.hpp"
#include <iostream>

boost::shared_ptr<IDaspMerger> IDaspMerger::Factor(const std::string& name)
{
	if(name == "merge") {
		return Ptr(new Idea1Merging());
	}
	else if(name == "unroll") {
		return Ptr(new Idea2Unrolling());
	}
	else {
		std::cerr << "Unknown dasp merger! Must be 'merge' or 'unroll'." << std::endl;
	}
}
