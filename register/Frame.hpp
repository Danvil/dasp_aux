/*
 * Frame.hpp
 *
 *  Created on: May 7, 2012
 *      Author: david
 */

#ifndef FRAME_HPP_
#define FRAME_HPP_

#include "PointSet.hpp"
#include <dasp/Superpixels.hpp>
#include <dasp/Neighbourhood.hpp>
#include <dasp/Segmentation.hpp>
#include <Slimage/Slimage.hpp>
#include <Eigen/Dense>
#include <boost/shared_ptr.hpp>
#include <vector>

/** An Rgbd frame with color and depth image */
struct Rgbd
{
	slimage::Image3ub color;
	slimage::Image1ui16 depth;

	void load(const std::string& fn);

	static Rgbd Load(const std::string& fn) {
		Rgbd x;
		x.load(fn);
		return x;
	}
};

/** A set of depth-adaptive superpixels */
struct DaspPointSet
{
	dasp::Superpixels superpixel;

	DaspPointSet() {
	}

	DaspPointSet(const Rgbd& u) {
		createWithDasp(u);
	}

	void createWithDasp(const Rgbd& u);

	PointSet createPointSet() const {
		PointSet points(superpixel.clusterCount());
		for(unsigned int i=0; i<points.size(); i++) {
			points[i].position = superpixel.cluster[i].center.world;
			points[i].normal = superpixel.cluster[i].center.computeNormal();
			points[i].color = superpixel.cluster[i].center.color;
		}
		return points;
	}

};

constexpr int NoId = -1;

inline bool IsValidId(int id) {
	return id != NoId;
}

struct Pairings
{
	struct Pair {
		unsigned int source_id;
		unsigned int target_id;
		float weight;
	};
	std::vector<Pair> pairings_;
};

typedef Eigen::Affine3f Transformation;

inline PointSet transform(const Transformation& T, const PointSet& points) {
	PointSet q(points.size());
	for(unsigned int i=0; i<q.size(); i++) {
		q[i] = T * points[i];
	}
	return q;
}

struct Frame
{
	typedef boost::shared_ptr<Frame> Ptr;

	Rgbd rgdb;

	DaspPointSet dasp;

//	dasp::Segmentation segments;

	std::vector<slimage::Pixel3ub> segment_colors;

	Pairings pairings;

	bool T_is_ground_truth;

//	Transformation T;

	Transformation T_global;

	Frame() {
//		T = Transformation::Identity();
		T_global = Transformation::Identity();
	}

	unsigned int countSuperpixels() const {
		return dasp.superpixel.clusterCount();
	}

//	PointSet transformPoints() const {
//		return transform(T, dasp.createPointSet());
//	}

	PointSet transformPointsGlobal() const {
		return transform(T_global, dasp.createPointSet());
	}

	std::vector<Eigen::Vector3f> computeBorderPoints() const {
		std::vector<unsigned int> indices = dasp::ComputeAllBorderPixels(dasp.superpixel);
		std::vector<Eigen::Vector3f> pnts(indices.size());
		for(unsigned int i=0; i<indices.size(); i++) {
			pnts[i] = dasp.superpixel.points[indices[i]].world;
		}
		return pnts;
	}

	std::vector<Eigen::Vector3f> computeBorderPointsGlobal() const {
		std::vector<Eigen::Vector3f> pnts = computeBorderPoints();
		for(unsigned int i=0; i<pnts.size(); i++) {
			pnts[i] = T_global * pnts[i];
		}
		return pnts;
	}

};

#endif
