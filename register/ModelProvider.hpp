/*
 * ModelProvider.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: david
 */

#ifndef ModelProvider_HPP_
#define ModelProvider_HPP_

#include "Batch.hpp"
#include "PointSet.hpp"
#include "Frame.hpp"
#include "Icp.hpp"
#include "SuperModel.hpp"
#include <dasp/Superpixels.hpp>
#include <dasp/Plots.hpp>
#include <dasp/Segmentation.hpp>
#include <Slimage/Slimage.hpp>
#include <Slimage/IO.hpp>
#include <Slimage/Gui.hpp>
#include <Eigen/Dense>
#include <boost/bind.hpp>
#include <boost/format.hpp>
#include <iostream>
#include <vector>

class MyControlObject;

struct ModelProvider
{
	friend class MyControlObject;

public:
	ModelProvider(const Batch::Ptr& batch);

	void next();

	void update();

	bool isFinished() const;

	void render() const;

	void writeGraphML(const std::string& fn);

private:
	Batch::Ptr batch_;

	unsigned int current_frame_;

	int last_merged_;

	SuperModel model_;

};

#endif
