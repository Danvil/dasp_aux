/*
 * DaspMerging.hpp
 *
 *  Created on: May 7, 2012
 *      Author: david
 */

#ifndef DASPMERGING_HPP_
#define DASPMERGING_HPP_

#include "Frame.hpp"
#include "SuperModel.hpp"
#include <boost/shared_ptr.hpp>
#include <string>

class IDaspMerger
{
public:
	typedef boost::shared_ptr<IDaspMerger> Ptr;

	virtual ~IDaspMerger() {}

	virtual SuperModel merge(const std::vector<Frame>& frames) = 0;

	static Ptr Factor(const std::string& name);

};

#endif
