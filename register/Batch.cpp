/*
 * Batch.cpp
 *
 *  Created on: May 15, 2012
 *      Author: david
 */


#include "Batch.hpp"
#include <dasp/Plots.hpp>
#include <Slimage/Gui.hpp>
#include <iostream>

static constexpr bool cGui = true;

const Frame::Ptr& Batch::add(const Rgbd& rgbd, bool use_segments)
{
	std::cout << "IcpBatchObject: Adding frame ..." << std::endl;
	Frame::Ptr frame(new Frame());
	frame->rgdb = rgbd;
	frame->dasp = DaspPointSet(rgbd);

	unsigned int num_segments = 1;

//	if(use_segments) {
//		dasp::SpectralSettings segs_settings;
//		segs_settings.num_eigenvectors = 24;
//		segs_settings.w_spatial = 1.0f;
//		segs_settings.w_color = 2.0f;
//		segs_settings.w_normal = 3.0f;
//
//		frame->segments = dasp::SpectralSegmentation(frame->dasp.superpixel, segs_settings);
//		frame->segments.ucm(frame->dasp.superpixel, 5.0f); // FIXME what is the threshold?
//	}
//	else {
//		frame->segments.segment_count = 1;
//		frame->segments.cluster_labels.resize(frame->countSuperpixels(), 0);
//		frame->segments.boundaries = slimage::Image1f(frame->dasp.superpixel.width(), frame->dasp.superpixel.height());
//		frame->segments.boundaries_wt = slimage::Image1ub(frame->dasp.superpixel.width(), frame->dasp.superpixel.height());
//		frame->segments.original_boundaries = slimage::Image1f(frame->dasp.superpixel.width(), frame->dasp.superpixel.height());
//		frame->segments.boundaries = frame->segments.original_boundaries;
//	}

	frame->pairings.pairings_.reserve(frame->countSuperpixels());
	for(unsigned int i=0; i<frame->countSuperpixels(); i++) {
		frame->pairings.pairings_.push_back(Pairings::Pair{i,0,0.0f});
	}

//		// FIXME HACK HACK HACK for the case with only one partition
//		frame->partition = Partition{ std::vector<int>(dasp_points_.back().superpixel.clusterCount(), 0), 1};

	std::cout << "\tNumber of superpixels: " << frame->dasp.superpixel.clusterCount() << std::endl;

	if(cGui) {
		slimage::gui::Show("color", frame->rgdb.color, 0);
		slimage::gui::Show("depth", frame->rgdb.depth, 0, 16000, 0);
		slimage::gui::Show("dasp", dasp::plots::PlotClusters(frame->dasp.superpixel, dasp::plots::ClusterPoints, dasp::plots::Color), 0);
		if(use_segments) {
//			slimage::gui::Show("segments", frame->segments.computeLabelImage(frame->dasp.superpixel), 0);
//			slimage::gui::Show("contours", dasp::Segmentation::CreateSmoothedContourImage(frame->segments.boundaries, 0.013f), 0);
//			std::cout << "Press key to continue" << std::endl;
//			slimage::gui::WaitForKeypress();
		}
		else {
			slimage::gui::Wait(50);
		}
	}

	frame->T_global = Transformation::Identity();
	frame->T_is_ground_truth = false;

	//frame->segment_colors = micp_->dasp_segments_[micp_->current_frame_].computeSegmentColors(micp_->dasp_points_[micp_->current_frame_].superpixel);
	frame->segment_colors = dasp::plots::CreateRandomColors(num_segments);

//	assert(frame->segment_colors.size() == frame->pairings.pairings_.size());

	add(frame);
	return frames_.back();
}
