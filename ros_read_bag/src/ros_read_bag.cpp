/*
 * rgbd_evaluator_preprocessing.cpp
 *
 *  Created on: Jan 13, 2012
 *      Author: praktikum
 */

#include "ros_read_bag/ros_read_bag.h"

#include <iostream>
#include <fstream>

#include <stdio.h>
#include <stdlib.h>

#include <boost/format.hpp>

namespace dasp
{

RosReadBag::RosReadBag(const std::string& file_path)
{
  std::cout << "Reading bagfile from " << file_path.c_str() << std::endl;

  file_created_folder_ = "/tmp/ros_read_bag/";
  std::string makeFolder;
  makeFolder.append("mkdir ");
  makeFolder.append(file_created_folder_);

  if( system(makeFolder.c_str()) < 0) // -1 on error
  {
    std::cout << "Error when executing: " << makeFolder  << std::endl;
    std::cout << "--> check user permissions"  << std::endl;
    return;
  }

  bag_.open(file_path, rosbag::bagmode::Read);

}

RosReadBag::~RosReadBag()
{
  cv::destroyAllWindows();

  bag_.close();
}

void RosReadBag::createTestFiles()
{
  uint32_t count = 1;

  // Image topics to load
  std::vector<std::string> topics;
  topics.push_back("/camera/rgb/image_rect_color");
  topics.push_back("/camera/depth_registered/image");

  rosbag::View view(bag_, rosbag::TopicQuery(topics));

  sensor_msgs::Image::ConstPtr p_current_img;
  geometry_msgs::TransformStamped::ConstPtr p_current_transform;
  sensor_msgs::CameraInfo::ConstPtr p_cam_info;

  ImageData current;

  // Load all messages into our dataset
  BOOST_FOREACH(rosbag::MessageInstance const m, view)
  {
    std::cout << "." << std::flush;
      // if the current image data is complete, go to next one
      if ( current.isComplete() )
      {
        image_store_.push_back( current );
	current = ImageData();
        count++;
      }

      // load rgb image
      sensor_msgs::Image::ConstPtr p_rgb_img = m.instantiate<sensor_msgs::Image>();

      //check if rgb_img message arrived
      if (p_rgb_img != NULL && p_rgb_img->encoding == "bgr8" )
      {

        // transform bag image to cvimage
        cv_bridge::CvImagePtr ptr = cv_bridge::toCvCopy(p_rgb_img);

        // store data in vectorImageData
        current.rgb_image = ptr;
      }

      /**********************************************************************************************************************/

      // load depth image
      sensor_msgs::Image::ConstPtr p_depth_img = m.instantiate<sensor_msgs::Image>();

      //check if depth_img message arrived
      if (p_depth_img != NULL && p_depth_img->encoding == "32FC1" )
      {
         // transform bag image to cvimage
        cv_bridge::CvImagePtr ptr = cv_bridge::toCvCopy(p_depth_img);

        // store data in vectorImageData
        current.depth_image = ptr;
      }

  }
  std::cout << std::endl;
  std::cout << "Read " << image_store_.size() << " images." << std::endl;
}

void RosReadBag::readImages()
{
  uint32_t count = 1;

  boost::format fn_color(file_created_folder_ + "/%05d_color.png");
  boost::format fn_depth(file_created_folder_ + "/%05d_depth.pgm");

  // !!! -1 because of the initial push_back in createTestFiles() ... !!!
  for (std::vector< ImageData >::iterator it=image_store_.begin(); it!=image_store_.end(); it++, count++)
  {
    if(count % 10 != 0) continue;
    std::cout << "Writing image " << count << "..." << std::endl;

    cv::Mat image_color1 = it->rgb_image->image;
    cv::imwrite((fn_color % count).str(), image_color1);
//    cv::Mat image_color2;
//    cv::resize(image_color1, image_color2, cv::Size(640,512));
//    cv::Mat image_color3 = image_color2(cv::Rect(0, 16, 640, 480));
//    cv::imwrite((fn_color % count).str(), image_color3);

    // write depth map
    // Convert float to 16-bit int
    cv::Mat1w depth_img;
    it->depth_image->image.convertTo( depth_img, CV_16U, 1000.0, 0.0 );

    std::ofstream fs((fn_depth % count).str().c_str());

    fs << "P2" << std::endl;
    fs << depth_img.cols << " " << depth_img.rows << std::endl;
    fs << 65535 << std::endl;

    for ( int y=0; y<depth_img.rows; y++ )
    {
      for ( int x=0; x<depth_img.cols; x++ )
      {
        fs << depth_img[y][x] << " ";
      }
      fs << std::endl;
    }
  }
}


} // end namespace


int main( int argc, char** argv )
{
  if(argc < 2)
  {
    std::cout << "Wrong usage, Enter: " << argv[0] << " <bagfileName>" << std::endl;
    return -1;
  }

  std::string file_path = std::string(argv[1]);

  dasp::RosReadBag fd(file_path);
std::cout << "Reading images" << std::endl;
  fd.createTestFiles();
std::cout << "Writing images" << std::endl;
  fd.readImages();

  std::cout << "Ready." << std::endl;
  return 0;
}

