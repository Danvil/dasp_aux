/*
 * kinect_grabber.cpp
 *
 *  Created on: Apr 10, 2012
 *      Author: david
 */

#include <KinectGrabber.h>
#include <Slimage/Gui.hpp>
#include <boost/bind.hpp>
#include <boost/format.hpp>
#include <iostream>

bool running = true;
unsigned int frame_count = 0;

boost::format p_save_pattern("/tmp/kinect_%03d");

bool capture_running = false;
unsigned int capture_nth = 1;
unsigned int capture_tick = 0;

void OnImages(const slimage::Image1ui16& depth, const slimage::Image3ub& color)
{
	slimage::gui::Show("kinect color", color, 0);
	slimage::gui::Show("kinect depth", ColorizeDepth(depth), 0);
	bool save_to_file = false;
	if(capture_running) {
		if(capture_nth == 0 || capture_tick % capture_nth == 0) {
			save_to_file = true;
		}
		capture_tick++;
	}
	// HACK
	int q = cv::waitKey(20);
	if(q != -1) {
		if(q == 1048603) {
			running = false;
			std::cout << "Shutting down..." << std::endl;
		}
		else if(q == 1114027 || q == 1114029) {
			if(q == 1114027) {
				capture_nth++;
			}
			else {
				capture_nth--;
			}
			if(capture_nth == 0) {
				capture_nth = 1;
			}
			if(capture_nth == 1) {
				std::cout << "Capturing every frame..." << std::endl;
			}
			else {
				std::cout << "Capturing every " << capture_nth << "-th frame" << std::endl;
			}
		}
		else if(q == 1048690) {
			capture_running = !capture_running;
			if(capture_running) {
				std::cout << "Now recording..." << std::endl;
			}
			else {
				std::cout << "Stopped recording." << std::endl;
			}
		}
		else if(q == 1048689) {
			frame_count = 0;
			capture_tick = 0;
			std::cout << "Reset frame counter." << std::endl;
			std::cout << "!!! WARNING WARNING WARNING WARNING WARNING !!!" << std::endl;
			std::cout << "This will overwrite already saved frame data!" << std::endl;
			std::cout << "!!! WARNING WARNING WARNING WARNING WARNING !!!" << std::endl;
		}
		else if(q == 1048608) {
			save_to_file = true;
		}
		else {
			std::cout << "Unknown key: " << q << std::endl;
			std::cout << "Usage:" << std::endl;
			std::cout << "\tEsc\tQuit program" << std::endl;
			std::cout << "\tSpace\tSave one frame" << std::endl;
			std::cout << "\tR\tstart/stop recording" << std::endl;
			std::cout << "\t+\tIncrease frame skip" << std::endl;
			std::cout << "\t-\tDecrease frame skip" << std::endl;
			std::cout << "\tQ\tRest frame counter (WILL OVERWRITE RECORDED FRAMES)" << std::endl;
		}
	}
	if(save_to_file) {
		// save to file
		unsigned int frame = frame_count++;
		std::string fn = (p_save_pattern % frame).str();
		slimage::Save(color, fn + "_color.png");
		slimage::Save(depth, fn + "_depth.pgm");
		std::cout << "Saved frame " << frame << " to '" << fn << "'" << std::endl;
	}
}

int main(int argv, char** argc)
{
	boost::shared_ptr<Romeo::Kinect::KinectGrabber> kinect_grabber(new Romeo::Kinect::KinectGrabber());

	std::cout << "Initializing kinect..." << std::endl;
	kinect_grabber->OpenConfig("/home/david/Programs/RGBD/OpenNI/Platform/Linux-x86/Redist/Samples/Config/SamplesConfig.xml");

	kinect_grabber->on_depth_and_color_.connect(boost::bind(&OnImages, _1, _2));

	while(running) {
		kinect_grabber->GrabAndNotify();
	}

	return 0;
}
