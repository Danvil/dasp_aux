function seed_points_plot
    img=imread('/media/smallfile/iasdocs/src/conferences/12-ICPR-DASP/images/seeds/seed_points_0.png');
    imshow(img)
    seeds = [];
    for y=20:440
        for x=40:590
            if img(y,x,1) == 255
                seeds = vertcat(seeds, [x 480-y]);
            end
        end
    end
    length(seeds)
    figure(1)
    clf(1)
    hold on
    plot(seeds(:,1), seeds(:,2), 'ok','MarkerSize',4,'MarkerFaceColor',[0 0 0])
    axis([40 590 40 460])
    set(gca, 'Visible','off')
    %set('XTick',[])
    %set('YTick',[])
    
    savefig('/home/david/Desktop/seeds_0', 'pdf')

end
