function plot_USE()
    figure(1)
    clf(1)
    hold on
    pbaspect([1 1 1])
    ylabel('Undersegmentation error')
    xlabel('Number of superpixels')
%    title('undersegmentation error')
    %axis([0 2000 0 1])
    create_plot_undersegmentation_error_impl(1:11)
    savefig('/home/david/Desktop/dasp_use', 'pdf')
    %for i=1:11;
    %    create_plot_undersegmentation_error_impl(i)
    %end
end

function create_plot_undersegmentation_error_impl(ids)
    use_turbo = load_error(ids, 'turbo');
    use_dasp = load_error(ids, 'dasp');
    use_slic = load_error(ids, 'slic');
    plot(use_turbo(:,1), use_turbo(:,2), '-ok','linewidth',3)
    plot(use_dasp(:,1), use_dasp(:,2), '-or','linewidth',3)
    plot(use_slic(:,1), use_slic(:,2), '-ob','linewidth',3)
    legend('Turbopixels','DASP','SLIC','location','NorthEast')
end

function use = load_error(ids, name)
    global gDaspDatabasePath
    for k=ids;
        fn = strcat(gDaspDatabasePath, '/results/', sprintf('%03d',k), '_', name, '_use.csv');
        data = csvread(fn);
        if exist('use') == 1
            use = use + data;
        else
            use = data;
        end
    end
    use = (1/size(ids,2)) * use;
    use(:,2) = use(:,2)./use(:,3);
end
