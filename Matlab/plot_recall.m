function plot_recall()
    figure(1)
    clf(1)
    hold on
    axis([0 2000 0 1])
    pbaspect([1 1 1])
    ylabel('Recall')
    xlabel('Number of superpixels')
%    title('boundary recall')
    create_recall_plot_impl(1:11, {'turbo','dasp','slic'}, ['k' 'r' 'b'])
    legend('Turbopixels','DASP','SLIC','location','SouthEast')
    savefig('/home/david/Desktop/dasp_recall', 'pdf')
%     for i=1:11;
%         create_recall_plot_impl(i, {'dasp'}, 'k')
%     end
%     for i=1:11;
%        labels{2*i-1} = sprintf('dasp %d',i);
%        labels{2*i} = sprintf('dasp %d',i);
%     end
%     legend(labels,'location','SouthEast')   
end

function create_recall_plot_impl(ids,algos,colors)
    for i=1:size(algos,2);
        r = load_recall(ids, algos{i});
        plot(r(:,1), r(:,3), strcat('-o',colors(i)), 'linewidth',3)
    end
%   complete(ids, 'turbo', 1,2);
%   complete(ids, 'dasp', 3,4);
end

function recall = load_recall(ids, name)
    global gDaspDatabasePath
    for k=ids;
        fn = strcat(gDaspDatabasePath, '/results/', sprintf('%03d',k), '_', name, '_br.csv');
        data = csvread(fn);
        if exist('recall') == 1
            recall = recall + data;
        else
            recall = data;
        end
    end
    recall = (1/size(ids,2)) * recall;
end

% function complete(ids, name, f1, f2)
%     recall = load_recall(ids, name);
%     figure(f1)
%     hold on
%     axis([0 2000 0 1])
%     plot ( recall(:,1), recall(:,2:5) )
%     plot ( recall(:,1), recall(:,2:5), 'o' )
%     ylabel('recall')
%     xlabel('count')
%     title(strcat(name, ' distance'))
%     legend('1','2','3','4','location','SouthEast')
%     figure(f2)
%     hold on
%     axis([0 2000 0 1])
%     plot ( recall(:,1), recall(:,6:9) )
%     plot ( recall(:,1), recall(:,6:9), 'o' )
%     ylabel('recall')
%     xlabel('count')
%     title(strcat(name, ' sigma'))
%     legend('1','2','3','4','location','SouthEast')
% end
