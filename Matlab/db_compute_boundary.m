%% creates boundary image from segmentation
function db_compute_boundary(ids)
    global gDaspDatabasePath
    for id=ids;
        fn_in = strcat(gDaspDatabasePath,'/labels/',sprintf('%03d',id),'.png');
        fn_out = strcat(gDaspDatabasePath,'/labels/',sprintf('%03d',id),'_bnds.png');
        imwrite(compute_boundary(imread(fn_in)), fn_out)
    end
end
