function plot_runtime
    plot_runtime_line
end

function plot_runtime_bar
    q = load('turbo');
    timesTURBO = q(7,2);
    q = load('dasp');
    timesDASP = q(7,2);
    timesgPb = 217.81;
    timessDASP = 0.8;
    y = [timesTURBO timesDASP timesgPb timessDASP]
    figure(1)
    clf(1)
    ylabel('runtime [s]')
    xlabel('Number of superpixels')
    %bar(y)
    %BreakPlot(sampleTimes,y,20,200,'Line');
    BarPlotBreak(y, 30, 150, 'Line', 0.75);
    set(gca,'XTickLabel',{'Turbopixels' 'DASP' 'gPb', 'sDASP'})
    %legend('turbo','dasp','gPb','location','NorthWest')
end

function plot_runtime_line
    timesTURBO = load('turbo')
    timesDASP = load('dasp')
    timesgPb = 217.81 * [1 1 1 1 1 1 1 1 1];
    %timesSLIC = [1.0 0.9 0.7 0.5 0.4 0.3 0.3 0.3 0.3];
    figure(1)
    clf(1)
%    axis([0 2000 -4 +4])
    ylabel('runtime [s]')
    xlabel('Number of superpixels')
%    title('runtime')
    semilogy(timesTURBO(:,1), timesTURBO(:,2), '-ok', 'linewidth',3)
    hold on
    pbaspect([1 1 1])
    semilogy(timesTURBO(:,1), timesDASP(:,2), '-or', 'linewidth',3)
    semilogy(timesTURBO(:,1), timesgPb, '-g', 'linewidth',3)
    %plot(counts, log(timesSLIC), '-r', 'linewidth',3)
    legend('turbo','dasp','gPb','location','East')
    savefig('/home/david/Desktop/dasp_runtime', 'pdf')
end

function data = load(algo)
    global gDaspDatabasePath
    fn = strcat(gDaspDatabasePath, '/results/', algo, '_timings.csv');
    data = csvread(fn);
end
