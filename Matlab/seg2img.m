%% converts an segmentation to an image
function [img] = seg2img(seg)
    colors = [255 0 0; 0 255 0; 0 0 255; 255 255 0; 0 255 255; 255 0 255; 0 0 0; 255 128 0; 255 255 255; 128 0 0; 0 128 0; 0 0 128; 128 128 0; 0 128 128; 128 0 128 ];
    img = zeros([size(seg,1),size(seg,2),3]);
    for i=1:size(seg,1);
        for j=1:size(seg,2);
%            if seg(i,j) > 6
%                fprintf('%d => %d %d %d\n', seg(i,j), colors(seg(i,j),1), colors(seg(i,j),2), colors(seg(i,j),3))
%            end
            k = seg(i,j);
            id = mod(k,size(colors,1));
            if id == 0
                id = size(colors,1);
            end
            img(i,j,1:3) = colors(id,1:3) / 255.0;
        end
    end
end

function [seg] = img2seg(img)
    colors = [255 0 0; 0 255 0; 0 0 255; 255 255 0; 0 255 255; 255 0 255; 0 0 0; 255 128 0; 255 255 255; 128 0 0; 0 128 0; 0 0 128; 128 128 0; 0 128 128; 128 0 128 ];
   seg = zeros([size(img,1),size(img,2),2]);
    for i=1:size(seg,1);
        for j=1:size(seg,2);
            for k=1:size(colors,1);
                if img(i,j,1:3) == colors(k,1:3)
                    seg(i,j) = k
                end
            end
        end
    end
end
