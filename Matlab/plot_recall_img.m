function plot_recall_img()
    ids = 1:11;
    data = zeros(size(ids,2),3);
    take=9;
    for i=ids;
        r1 = load_recall(i, 'turbo');
        r2 = load_recall(i, 'dasp');
        data(i,:) = [i r1(take,3) r2(take,3)];
    end
    figure(1)
    clf(1)
    hold on
    axis([0 12 0 1])
    bar(data(:,2:3))
    legend('turbo','dasp','location','SouthEast')
end

function recall = load_recall(ids, name)
    global gDaspDatabasePath
    for k=ids;
        fn = strcat(gDaspDatabasePath, '/results/', sprintf('%03d',k), '_', name, '_br.csv');
        data = csvread(fn);
        if exist('recall') == 1
            recall = recall + data;
        else
            recall = data;
        end
    end
    recall = (1/size(ids,2)) * recall;
end

