function data = create_plot_clusters()
    %cnt = [100;200;300;400;500;750;1000;1500;2000];
    cnt = 500;
    global gDaspDatabasePath
    data = {};
    for i=1:size(cnt);
        fn = strcat(gDaspDatabasePath,'/results/004_dasp_',sprintf('%d',cnt(i)),'_clusters.csv');
        t = csvread(fn);
        data{i} = t;
    end
    
    max = cnt / 2000 * 300;
    
    subplot(2,3,1)
    hold on
    axis([0 0.1 0 max])
    hist(1.96*sqrt(data{1}(:,1)), 0:0.002:0.1)
    subplot(2,3,2)
    hold on
    axis([0 0.1 0 max])
    hist(1.96*sqrt(data{1}(:,2)), 0:0.002:0.1)
    subplot(2,3,3)
    hold on
    axis([0 0.1 0 max])
    hist(1.96*sqrt(data{1}(:,3)), 0:0.002:0.1)    
    
    subplot(2,3,4)
    hold on
    axis([0 0.05 0 max])
    hist(data{1}(:,4), 0:0.001:0.05)
    subplot(2,3,5)
    hold on
    axis([0 0.05 0 max])
    hist(data{1}(:,5), 0:0.001:0.05)
    
end
