%% creates turbo pixel oversegmentation for an image in the database
function db_turbo(ids)
    cnts = [100 200 300 400 500 750 1000 1500 2000];
    global gDaspDatabasePath
    global gDaspResultImagePath
    t = zeros(length(cnts),2);
    t(:,1) = cnts;
    for i=1:length(ids)
        id = ids(i);
        for c=1:length(cnts)
            cnt = cnts(c);
            fprintf('Computing %d turbopixels for image %d\n', cnt, id)
            fn_in = strcat(gDaspDatabasePath, '/images/', sprintf('%03d',id), '_color.png');
            fn_out = strcat(gDaspResultImagePath, '/', sprintf('%03d',id), '_turbo_', sprintf('%d',cnt), '.png');
            t(c,2) = t(c,2) + turbo(fn_in, fn_out, cnt);
        end
        tt = t;
        tt(:,2) = tt(:,2) / i;
        csvwrite(strcat(gDaspDatabasePath, '/results/turbo_timings.csv'), tt);
    end
end
