function plot_precision_recall()
    global gDaspDatabasePath
    gpb=csvread(strcat(gDaspDatabasePath,'/results/gPb.csv'));
%    daspspec200=csvread(strcat(gDaspDatabasePath,'/results/dasp_spectral_200.csv'));
    daspspec1000=csvread(strcat(gDaspDatabasePath,'/results/dasp_spectral_1000.csv'));
%    daspspec2000=csvread(strcat(gDaspDatabasePath,'/results/dasp_spectral_2000.csv'));
%    slicspec200=csvread(strcat(gDaspDatabasePath,'/results/slic_spectral_200.csv'));
    slicspec1000=csvread(strcat(gDaspDatabasePath,'/results/slic_spectral_1000.csv'));
%    slicspec2000=csvread(strcat(gDaspDatabasePath,'/results/slic_spectral_2000.csv'));

    figure(1)
    clf(1)
    hold on
    axis([0 1 0 1])
    daspect([1 1 1])
    pbaspect([1 1 1])
    ylabel('Precision')
    xlabel('Recall')
    %title('Boundary Recall on Segments')
    
    for q=0.1:0.1:0.9;
        plot_f1_iso(q)
    end

    plot(slicspec1000(:,3),slicspec1000(:,2),'b','linewidth',3)
    plot(gpb(:,3),gpb(:,2),'k','linewidth',3)
%    plot(daspspec200(:,3),daspspec200(:,2),'r','linewidth',3,'Color',[0.5 0 0])
    plot(daspspec1000(:,3),daspspec1000(:,2),'r','linewidth',3)
%    plot(daspspec2000(:,3),daspspec2000(:,2),'-r','linewidth',3,'Color',[1 0.5 0.5])
%    plot(slicspec200(:,3),slicspec200(:,2),'b','linewidth',3,'Color',[0 0 0.5])
%    plot(slicspec2000(:,3),slicspec2000(:,2),'-b','linewidth',3,'Color',[0.5 0.5 1])
%    legend('gPb','dasp','slic','location','SouthWest')

    plot_max_f1(slicspec1000, [0 0 1])
    plot_max_f1(gpb, [0 0 0])
    plot_max_f1(daspspec1000, [1 0 0])

    %annotation('ellipse',...
    %    [0.66044142614601 0.735807860262008 0.15 0.2],...
    %    'FaceColor',[1 0 0],'Color',[1 0 0]);
 
%    legend('gPb','dasp 200','dasp 1000','slic 200','slic 1000','location','SouthWest')   
    legend('[F=0.59] sSLIC','[F=0.70] gPb-owt-ucm','[F=0.76] sDASP','location','SouthWest')   

    savefig('/home/david/Desktop/dasp_segments', 'pdf')
end

function plot_f1_iso(q)
    r = (q/(2-q)):0.01:1;
    p = q*r./(2*r-q);
    plot(r,p,'Color',[0.7 0.7 0.7],'HandleVisibility','off')
end

function plot_max_f1(u, col)
    f1 = (2*u(:,2).*u(:,3)./(u(:,2)+u(:,3)));
    i = find(f1==max(f1));
    plot(u(i,3), u(i,2), 'o','Color',col,'MarkerSize',8,'MarkerFaceColor',col)
end