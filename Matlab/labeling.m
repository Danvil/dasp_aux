% interactive segmentation gui example
function labeling(fn_in, fpath_base)
    %clear all;close all;clc;

    addpath('BSR/grouping');
    addpath('BSR/grouping/lib');
    addpath('BSR/grouping/interactive');

    imgFile = fn_in;
    outFile = strcat(fpath_base, '/gPb.mat');
    ucmImgFile = strcat(fpath_base, '/ucm.bmp');
    ucmMatFile = strcat(fpath_base, '/ucm2.mat');
    seedsFile = strcat(fpath_base, '/seeds.csv');
    segFile = strcat(fpath_base, '/seg.csv');
    segImgFile = strcat(fpath_base, '/seg.png');

    %% load example data
    % load image
    im = double(imread(imgFile))/255;

    % load gPb
    load(ucmMatFile);
    [txb,tyb]=size(ucm2);

    % load seeds
    if exist(seedsFile)
        seeds = csvread(seedsFile);
        %s = imresize(imread(seedsFile),[txb,tyb],'nearest');
        %seeds = zeros(size(s,1), size(s,2));
        %seeds((s(:,:,1)==0 & s(:,:,2)==255 & s(:,:,3)==0)) = 1;
        %seeds((s(:,:,1)==255 & s(:,:,2)==0 & s(:,:,3)==0)) = 2;
        %seeds = seeds(2:2:end,2:2:end);
    %    seeds = imread(seedsFile);
    else
        seeds = {};
    end

    % load object annotations (none exist yet)
    obj_names = {};

    %% run interactive segmentation gui
    % return updated seeds and object names
    % also return last button clicked either 'prev' or 'next'
    [seeds obj_names seg action] = interactive_segmentation(im, ucm2, seeds, obj_names);

    % save manual segmentation seeds
    csvwrite(seedsFile,seeds)
    % save segments
    csvwrite(segFile,seg)
    % save colored segments
    imwrite(seg2img(seg), segImgFile);
end
