%% computes the boundary of a segmentation
function bnd = compute_boundary(seg)
    bnd = [];
    if size(seg,3) == 1
        bnd = compute_boundary_1(seg);
    end
    if size(seg,3) == 3
        bnd = compute_boundary_3(seg);
    end
end

function bnd = compute_boundary_3(seg)
    bnd = false([size(seg,1),size(seg,2)]);
    for i=2:size(seg,1)-1;
        for j=2:size(seg,2)-1;
            u = seg(i,j,1:3);
            bnd(i,j) = (sum(u == seg(i,j-1,1:3)) + sum(u == seg(i,j+1,1:3))  + sum(u == seg(i-1,j,1:3)) + sum(u == seg(i+1,j,1:3))) ~= 4*3;
        end
    end

end

function bnd = compute_boundary_1(seg)
    bnd = false([size(seg,1),size(seg,2)]);
    for i=2:size(seg,1)-1;
        for j=2:size(seg,2)-1;
            u = seg(i,j);
            bnd(i,j) = ~(u == seg(i,j-1) && u == seg(i,j+1) && u == seg(i-1,j) && u == seg(i+1,j));
        end
    end

end
