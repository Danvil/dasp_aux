function nyudb_show(nyudb, i)
    figure(1)
    image(nyudb.images(:,:,:,i))
    figure(2)
    image(depth2col(nyudb.depths(:,:,i)))
    figure(3)
    image(seg2img(nyudb.labels(:,:,i)))
    figure(4)
    image(compute_boundary(nyudb.labels(:,:,i)))
end

function col = depth2col(dep)
    col = zeros(size(dep,1),size(dep,2),3);
    for i=1:size(dep,1);
        for j=1:size(dep,2);
            g = dep(i,j) / 8;
            col(i,j,:) = [g g g];
        end
    end
end
