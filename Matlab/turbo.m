%% creates turbo pixel oversegmentation for an image
function dt = turbo(fn_in, fn_out, cnt)
    addpath('TurboPixels');
    addpath('TurboPixels/lsmlib');
    img = im2double(imread(fn_in));
    tic
    [phi,boundary,disp_img] = superpixels(img, cnt);
    dt = toc;
    imagesc(disp_img);
    imwrite(boundary,fn_out)
end
