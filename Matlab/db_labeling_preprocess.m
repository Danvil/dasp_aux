function db_labeling_preprocess(id)
    global gDaspDatabasePath
    tic
    labeling_preprocess(...
        strcat(gDaspDatabasePath,'/images/',sprintf('%03d',id),'_color.png'),...
        '/tmp')
        %strcat(gDaspDatabasePath,'/labels/',sprintf('%03d',id)))
    t = toc;
    fprintf('\nFinished in %f seconds\n', t)
end