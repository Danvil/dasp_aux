function nyudb_create(path, nyudb, ids)
    ids = sort(ids);
    for k=1:size(ids,2);
        i = ids(k);
        istr = sprintf('%04d',k);
        imwrite(nyudb.images(:,:,:,i), strcat(path,'/',istr,'_color.png'))
        img_depth = uint16(1000*nyudb.depths(:,:,i));
        imwrite(img_depth, strcat(path,'/',istr,'_depth.pgm'))
        imwrite(compute_boundary(nyudb.labels(:,:,i)), strcat(path,'/',istr,'_bnds.png'))
    end

end
