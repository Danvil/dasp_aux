function plot_edge_weights()
    for i=1:16;
        edgeweights(:,i) = importdata(strcat('/tmp/edge_weights_',sprintf('%03d',i-1),'.txt'));
    end
    edgeweights_sum = importdata('/tmp/edge_weights_sum.txt');
    figure(1)
    clf
    hold on
    plot(log(edgeweights),'.')
    plot(log(edgeweights_sum),'r')
end
